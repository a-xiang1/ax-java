package page;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-03
 * Time: 16:12
 */

//记住多态的实现条件
    //必须在继承体系下
    //子类必须对父类的方法进行重写
    //通过父类的引用调用重写的方法
class Animal {
    String name;
    int age;
    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }
    public void eat(){
        System.out.println(name + "吃饭");
    }
    public void show(){
        System.out.println("韦坤");
    }
}
 class Cat extends Animal{ //条件1 属于继承关系
    public Cat(String name, int age){//继承父类的成员并进行调用
        super(name, age);//super对父类的成员进行初始化
    }
    @Override
    public void eat(){//条件二 对父类的方法进行重写
        System.out.println(name+"吃鱼~~~");
    }
}
class Dog extends Animal {
    public Dog(String name, int age){
        super(name, age);
    }
   // @Override
    public void eat( ){
        System.out.println(name+"吃骨头~~~");
    }
    public void shw(){
        System.out.println("韦坤123");
    }
}
public class AX {
    public static void eat(Animal a){
        a.eat();
    }
    public static void main(String[] args) {

        //Dog dog = new Dog("小王",1);

        //条件三通过父类的引用调用重写的方法
        // eat(cat);
        //eat(dog);
        Dog dog = new Dog("小王",1);
        Animal animal = new Cat("可可",2);
        animal.eat();
        animal.show();


       /* if(animal instanceof Dog){
            dog = (Dog)animal;//类似于强制转换类型
           dog.shw();
        }*/



    }
}
