package page;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-03
 * Time: 15:18
 */

class Base{
    final int a = 10;
    //a = 20;//背final修饰了那么就不能修改
    final public void show(){
        System.out.println("父类");
    }

}
class Kun extends Base{

   /* @Override
    public void show() {//被final修饰的方法不能进行重写
        super.show();
    }*/
}
public class AxJava {
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");

    }
}
