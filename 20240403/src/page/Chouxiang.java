package page;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-03
 * Time: 19:35
 */
//抽象类
//使用abstract的方法称为抽象方法
//使用abstract的类称为抽象类
//抽象类是不可以进行实例化的
//抽象类当中 可以和普通类一样定义成员变量 和成员方法
//当一个普通的类继承了这个抽象类，那么需要重写这个抽象类当中的所有抽象方法。
//抽象类就是为了继承
//abstract 和final是天敌 他们不能共存
//被private和 static修饰的也不可以

abstract class Shape{
    public abstract void draw();

}
class Bas extends Shape{
    public  void draw(){//需要重写不然报错
        System.out.println("韦坤k");
    }
}
class Ba extends Shape{
    public  void draw(){//需要重写不然报错
        System.out.println("韦k");
    }

}
public class Chouxiang {

    public static void raw(Shape shape){//这里要静态不道为啥
        shape.draw();

    }
    public static void main(String[] args) {
            //抽象不能实例化，但是可以用向上转型
        Shape ax = new Bas();
        Shape aa = new Ba();
        raw(ax);
        raw(aa);
    }
}
