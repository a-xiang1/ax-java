/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-09
 * Time: 10:08
 */
public class axstudentjava {
    public static void main(String[] args) {
        String ch = "韦坤要四战" ;
        System.out.println(ch);

        /*//除数
        System.out.println(5/2);//2，int类型
        System.out.println(5.0/2);//2.5
        System.out.println(5/0);//这里是会报错，运算错误
        System.out.println(5.0/0); //这里是等于无穷

        //求余数
        //求余数的符合结果取决于被余数的符号。
        System.out.println(9 % 2); //1
        System.out.println(9 % -2); //1
        System.out.println(-9 % 2); //-1
        System.out.println(-9 % -2); //-1

        //java可以用小数进行求余数
        System.out.println(11.5 % 2);//1.5*/

        //增量运算符
        /*double d = 11.5;
        int c = 20 ;*/
        //这两个写法都会报错，因为double是八个字节，范围大的不能转换为范围小的。
       /* int a  = c + d;
         c  = c + d; */
        //但是下面这个写法可以成立
        //因为把增量运算符能自动帮你进行类型转换。
      /*  c += d;*/
        //System.out.println(c);
        //快速打印c，可以这么写c.sout
        /*System.out.println(c);*/

       /* int a = 10 ;
        int b = a ++ ;//先赋值10给b，然后++
        int b2 = ++ a;
        System.out.println(b);
        System.out.println(b2);

        //关系运算符
        //结果只会输出true或者false
        System.out.println(a == b);
        System.out.println(a != b);
        System.out.println(a < b);
        System.out.println(a > b);
        System.out.println(a <= b);
        System.out.println(a >= b);*/

        //逻辑运算符短路求值
        //在这里&&的话如果前面为false那么后面的无需计算，直接出结果，所以在这里5/0是不会报错的。
        //System.out.println(10 > 20 && 5/0 == 0);
        //如果是单&那么两个都会判断一次，所以在这里就好判断5/0报错
        //System.out.println(10 > 20 & 5/0 == 0);

        //在这里||的话如果前面为true那么后面的无需计算，直接出结果，所以在这里5/0是不会报错的。
        //System.out.println(10 < 20 || 5 / 0 == 0);
        //同样
        //System.out.println(10 < 20 | 5 / 0 == 0);

        //按位与，只要对应位上都是1，结果才为1，其他都是0；
        //按位或，只要对应位上有1，结果都是1.
        /*int a = -1;
        int b = ~ a;//-1补码全是1，按位取反得到8个0。（符号位也要取反）。
        System.out.println(b);*/

        //左移和右移
        int a = -1;
        System.out.println(a>>>1);//无符号右移
        System.out.println(a>>1);

        int b = 11;
        System.out.println(b >> 1);
        System.out.println(b >>> 1);
        //右移代表除法，左移代表乘法。

        //三目运算符
        //优先级不用记，自己加括号就好了
        boolean flg = true == true ? true == false ? true : false : true;
        //用括号区分就容易看出来了
        boolean flg = (true == true) ? (true == false ? true : false ): true;
        System.out.println(flg);



    }

}
