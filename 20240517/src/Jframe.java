/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-17
 * Time: 12:56
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;



public class Jframe  extends JFrame{
    private JPanel topJPanel;// 定义顶层面板
    private JFrame ckFrame;// 定义窗口
    private JTextField webenField;// 定义单行文本输入框
    private JRadioButton radioButton[];// 定义单选按钮数组
    private JCheckBox duoxuan[];// 定义多选按钮数组 ，
    private JComboBox<String> xialaBox;//下拉列表
    private JButton tijiaoButton; //提交按钮进行读取数据

    public JFrame getck() {
        ckFrame = new JFrame(); //复杂数据类型需要重新new，而基础数据类型如int无需
        ckFrame.setTitle("篮球场");

        ckFrame.add(getPanel());

        ckFrame.setSize(300,200);
        ckFrame.setLocation(200,200);
        ckFrame.setVisible(true);
        ckFrame.setDefaultCloseOperation(3);
        return ckFrame;

    }

    public JPanel getPanel() {
        topJPanel = new JPanel();
        webenField  = new JTextField(15);
        topJPanel.add(webenField);

        //分组,不分组会咋样
        ButtonGroup bGroup = new ButtonGroup();
        radioButton = new JRadioButton[4];

        for (int i = 0; i < radioButton.length; i++) {
            radioButton[i] = new JRadioButton("A" + i);
            bGroup.add(radioButton[i]);
            topJPanel.add(radioButton[i]);
        }


        duoxuan = new JCheckBox[5];
        //添加多选按钮
        for (int i = 0; i < duoxuan.length; i++) {
            duoxuan[i] = new JCheckBox("BB" + i);
            topJPanel.add(duoxuan[i]);
        }


        xialaBox = new JComboBox<String>();
        xialaBox.addItem("打篮球");
        xialaBox.addItem("鸿发");
        xialaBox.addItem("坤坤");
        topJPanel.add(xialaBox);

        tijiaoButton = new JButton("提交");
        topJPanel.add(tijiaoButton);
        tijiaoButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                String st1 ="";
                st1 = webenField.getText();
                String st2 = "";

                for (int i = 0; i < radioButton.length; i++) {
                    if (radioButton[i].isSelected()) {//是否被选中
                        st2 += radioButton[i].getText(); // 选中就拼接起来


                    }
                }
                String st3 = "";
                for (int i = 0; i < duoxuan.length; i++) {
                    if(duoxuan[i].isSelected()) {
                        st3 = st3 + "-"  + duoxuan[i].getText();  //分开拼接
                    }
                }
                //处理获取下拉列表
                String st4 = "";
                st4 = (String) xialaBox.getSelectedItem();


                //弹出内容
                JOptionPane.showMessageDialog(null, st1+"-" +  st2 +"-"  + st3 +"-" + st4);

            }
        });


        return topJPanel;
    }





}
