package jsq;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-15
 * Time: 15:13
 */
@SuppressWarnings("serial")
public class JSQs extends JFrame implements ActionListener {
    private JFrame  jsq; //  定义计算器窗口
    private JPanel dingPanel;//定义计算器顶层面板
    private JButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b0;// 定义数字0-9按键
    private JButton jia,jian,cheng,chu,dian,dengyu;// 定义加减乘除按钮，小数点，等于号按钮
    private JTextField jieguo;// 定义计算后的结果显示 文本框
    private JLabel yun1,yun2,fuhao1,result,fuhao2;
    private String[] num;
    private boolean anxia =false; // 设置是否按下过运算符，按下过true ，没有按下过false
    public void chuangkou() { // 创建窗口
        jsq = new JFrame();  // 创建窗口对象
        jsq.setTitle("简单计算器");// 为窗口设置标题
        jsq.add(getPanel());// 为窗口 添加 内容面板（计算器界面）
        jsq.setSize(250, 250);// 设置窗口大小，250*250px
        jsq.setLocation(300, 200);// 设置窗口弹出初始显示器位置
        jsq.setVisible(true);// 设置窗口可见
        jsq.setDefaultCloseOperation(EXIT_ON_CLOSE);// 设置窗口 右上角的最小大关闭事件
    }

    private JPanel getPanel() {
        dingPanel = new JPanel();
        dingPanel.setLayout(new BorderLayout());

//		jieguo = new JTextField(15);
        JPanel top=new  JPanel();
        yun1 = new JLabel();
        yun2 = new JLabel();
        fuhao1 = new JLabel();
        fuhao2 = new JLabel();
        result = new JLabel();
        top.add(new JLabel("计算式："));
        top.add(yun1);top.add(fuhao1);top.add(yun2);
        top.add(fuhao2);top.add(result);
        dingPanel.add(top,BorderLayout.NORTH);

        JPanel neijJPanel = new JPanel();
        neijJPanel.setLayout(new GridLayout(5, 4, 5, 5));
        b7 = new JButton("7");
        b8 = new JButton("8");
        b9 = new JButton("9");
        jia = new JButton("+");
        neijJPanel.add(b7);neijJPanel.add(b8);
        neijJPanel.add(b9);neijJPanel.add(jia);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        jia.addActionListener(this);

        b4 = new JButton("4");
        b5 = new JButton("5");
        b6 = new JButton("6");
        jian = new JButton("-");
        neijJPanel.add(b4);neijJPanel.add(b5);
        neijJPanel.add(b6);neijJPanel.add(jian);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        jian.addActionListener(this);

        b1 = new JButton("1");
        b2 = new JButton("2");
        b3 = new JButton("3");
        cheng = new JButton("*");
        neijJPanel.add(b1);neijJPanel.add(b2);
        neijJPanel.add(b3);neijJPanel.add(cheng);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        cheng.addActionListener(this);

        b0 = new JButton("0");
        dian = new JButton(".");
        dengyu = new JButton("=");
        chu = new JButton("/");
        neijJPanel.add(b0);neijJPanel.add(dian);
        neijJPanel.add(dengyu);neijJPanel.add(chu);
        b0.addActionListener(this);
        dian.addActionListener(this);
        dengyu.addActionListener(this);
        chu.addActionListener(this);

        JButton qingkong= new JButton("空");
        neijJPanel.add(qingkong);
        qingkong.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                yun1.setText("");
                yun2.setText("");
                fuhao1.setText("");
                fuhao2.setText("");
                result.setText("");
                anxia =false;
            }
        });

        dingPanel.add(neijJPanel,BorderLayout.CENTER);
        return dingPanel;
    }

   /* public JPanel getnumPanel() {
        JPanel nei = new JPanel();
        nei.setLayout(new GridLayout(4, 4, 5, 5));
        num =new String[] {"0","1","2","3","4","5","6","7","8","9","+","-","*","/",".","="};
        for (int i = 0; i < num.length; i++) {
            JButton tem = new JButton(num[i]);
            nei.add(tem);
        }
        return nei;

    }*/

    @Override
    public void actionPerformed(ActionEvent e) {
        if (dengyu == e.getSource()) { // 输入等于号，直接计算
            jisuan();
        }else if((e.getSource() == jia) || (e.getSource() == jian) ||
                (e.getSource() == cheng) || (e.getSource() == chu)
        ) { // 按下 +-*/其中的一个运算符
            if (anxia) { // 之前按下过 +-*/
                jisuan();
                yun1.setText(result.getText());
                yun2.setText("");
                fuhao1.setText( ((JButton) e.getSource() ).getText() );
                result.setText("");
                fuhao2.setText("");

            }else{ // 没有按下过
                anxia =true;
                fuhao1.setText( ((JButton) e.getSource() ).getText() );
            }


        }
        else {// 按下0-9 ，.  11个按钮
            if (anxia) { // 代表按下过 +-*/
                yun2.setText(yun2.getText() +( (JButton) e.getSource() ).getText() );
            }else {// 代表没按下过 +-*/
                yun1.setText(yun1.getText() + ( (JButton) e.getSource() ).getText());
            }

        }

//		jieguo.setText(jieguo.getText() +
//				( (JButton) e.getSource() ).getText()  );


    }

    public void jisuan() {
        Float num1 ,num2,resFloat = 0f;
        String yunsuanfu;
        num1 = Float.parseFloat( yun1.getText() );
        num2 = Float.parseFloat( yun2.getText() );
        yunsuanfu = fuhao1.getText();
        switch (yunsuanfu) {
            case "+":
                resFloat = num1 + num2;
                break;
            case "-":
                resFloat = num1 - num2;
                break;
            case "*":
                resFloat = num1 * num2;
                break;
            case "/":
                resFloat = num1 / num2;
                break;
            default:
                break;
        }
        result.setText(resFloat.toString());
        fuhao2.setText("=");
    }


}
