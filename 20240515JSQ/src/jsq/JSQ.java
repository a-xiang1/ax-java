package jsq;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-15
 * Time: 12:36
 */




import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;






@SuppressWarnings("serial")
public class JSQ extends JFrame implements ActionListener {

    //private static final int EXIT_ON_CLOSE = 0;
    //事件要用到全局变量
    private JFrame jsqFrame;////窗口
    //顶层面板
    private JPanel dingcJPanel;
    private JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b0;//定义数字
    private JButton jia, jian, cheng, chu, dian, dengyuButton;//计算机符合
    private JTextField jieguoField;//
    private boolean anxia = false;//是否按下过+-*/
    private JPanel neijJPanel;
    //运算
    private JLabel yuns1, yuns2, fuhao1, result, fuhao2;

    //创建窗口
    public void chuangkinit() {
        jsqFrame = new JFrame();//窗口对象
        jsqFrame.setTitle("计算器");//窗口标题
        jsqFrame.setSize(300, 300);//窗口大小
        jsqFrame.add(getPanel());
        jsqFrame.setLocation(300, 300);//弹出位置
        jsqFrame.setVisible(true);//窗口可见
        jsqFrame.setDefaultCloseOperation(3);//设置关闭结束
        //jsqFrame.setDefaultCloseOperation(3);//设置关闭结束
    }


    //计算面板
    private JPanel getPanel() {
        dingcJPanel = new JPanel();
        //在顶部设置边框布局
        dingcJPanel.setLayout(new BorderLayout());
        dingcJPanel.add(new JTextField(15));

        //设置运算
        JPanel top=new  JPanel();
        yuns1 = new JLabel();
        yuns2 = new JLabel();
        fuhao1 = new JLabel();
        fuhao2 = new JLabel();
        result = new JLabel();

        //top.add(new JTextField("计算式："));
        top.add(new JLabel("计算式："));//表达式放在文本框中
        //这里的摆放顺序要对齐
        top.add(yuns1);
        top.add(fuhao1);
        top.add(yuns2);
        top.add(fuhao2);
        top.add(result);
        dingcJPanel.add(top, BorderLayout.NORTH);


        //布局结果设置文本框
        //jieguoField = new JTextField(15);
        //dingcJPanel.add(jieguoField,BorderLayout.NORTH); //放置在顶部

        neijJPanel = new JPanel();
        neijJPanel.setLayout(new GridLayout(5, 4, 5, 5));//网格布局
        //getanniu();
        String[] strings = {"7", "8", "9", "+", "4", "5", "6", "-", "1", "2", "3", "*", "0", ".", "=", "/"};

       /* JPanel anniuJPanel = new JPanel();
        anniuJPanel.setLayout(new GridLayout(4, 4, 6, 6));*/
       /* for (int i = 0; i < strings.length; i++) {


            JButton temButton = new JButton(strings[i]);//放的是字符串
            neijJPanel.add(temButton);
            temButton.addActionListener(this);
        }*/

        //放第一行
			/*b7 = new JButton("7");
			b8 = new JButton("8");
			b9 = new JButton("9");
			jia = new JButton("+");

			neijJPanel.add(b7);
			neijJPanel.add(b8);
			neijJPanel.add(b9);
			neijJPanel.add(jia);



			//放第二行
			b4 = new JButton("4");
			b5 = new JButton("5");
			b6 = new JButton("6");
			jian = new JButton("-");

			neijJPanel.add(b4);
			neijJPanel.add(b5);
			neijJPanel.add(b6);
			neijJPanel.add(jian);

			//第三行

			b1 = new JButton("1");
			b2 = new JButton("2");
			b3 = new JButton("3");
			cheng = new JButton("*");

			neijJPanel.add(b1);
			neijJPanel.add(b2);
			neijJPanel.add(b3);
			neijJPanel.add(cheng);


			//第四行
			b0 = new JButton("0");
			dian = new JButton(".");
			dengyuButton = new JButton("=");
			chu = new JButton("/");

			neijJPanel.add(b0);
			neijJPanel.add(dian);
			neijJPanel.add(dengyuButton);
			neijJPanel.add(chu);
			*/
        b7 = new JButton("7");
        b8 = new JButton("8");
        b9 = new JButton("9");
        jia = new JButton("+");
        neijJPanel.add(b7);neijJPanel.add(b8);
        neijJPanel.add(b9);neijJPanel.add(jia);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        jia.addActionListener(this);

        b4 = new JButton("4");
        b5 = new JButton("5");
        b6 = new JButton("6");
        jian = new JButton("-");
        neijJPanel.add(b4);neijJPanel.add(b5);
        neijJPanel.add(b6);neijJPanel.add(jian);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        jian.addActionListener(this);

        b1 = new JButton("1");
        b2 = new JButton("2");
        b3 = new JButton("3");
        cheng = new JButton("*");
        neijJPanel.add(b1);neijJPanel.add(b2);
        neijJPanel.add(b3);neijJPanel.add(cheng);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        cheng.addActionListener(this);

        b0 = new JButton("0");
        dian = new JButton(".");
        dengyuButton = new JButton("=");
        chu = new JButton("/");
        neijJPanel.add(b0);neijJPanel.add(dian);
        neijJPanel.add( dengyuButton);neijJPanel.add(chu);
        b0.addActionListener(this);
        dian.addActionListener(this);
        dengyuButton.addActionListener(this);
        chu.addActionListener(this);


        //给+-*/添加事件监控

        //添加清空按钮
        JButton qkButton = new JButton("空");
        neijJPanel.add(qkButton);
        qkButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                yuns1.setText("");
                yuns2.setText("");
                fuhao1.setText("");
                fuhao2.setText("");
                result.setText("");
                anxia = false;


            }
        });

        //dingcJPanel.add(neijJPanel, BorderLayout.CENTER);//这是啥
        dingcJPanel.add(neijJPanel, BorderLayout.CENTER);
        return dingcJPanel;
    }


    public void getanniu() {
        String[] strings = {"7", "8", "9", "+", "4", "5", "6", "-", "1", "2", "3", "*", "0", ".", "=", "/"};

       /* JPanel anniuJPanel = new JPanel();
        anniuJPanel.setLayout(new GridLayout(4, 4, 6, 6));*/
        for (int i = 0; i < strings.length; i++) {


            JButton temButton = new JButton(strings[i]);//放的是字符串
            neijJPanel.add(temButton);
            temButton.addActionListener(this);


        }


    }

    //a + b = c
    //怎么判断985 是abc中的哪一个
    //不管表达式多长，最终还是回到a+b =c，


    //监听
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        //JButton temButton = e.getText();

        //
        if (dengyuButton == e.getSource()) {//输入=号直接计算
            //进行计算
            jisuan();

        } else if ((e.getSource() == jia) ||
                (e.getSource() == jian) ||
                (e.getSource() == cheng) ||
                (e.getSource() == chu))
        { if (anxia) {//之前按下过
                jisuan();
                yuns1.setText(result.getText());//把之前的两个计算结果给yuns1
                yuns2.setText("");
                fuhao1.setText(((JButton) e.getSource()).getText());
                result.setText("");
                fuhao2.setText("");


            } else {
                anxia = true;//设置按下
                fuhao1.setText(((JButton) e.getSource()).getText());
            }


        } else {//按下0-9
            if (anxia)//按下过那么就不拼接了
            {
                yuns2.setText(yuns2.getText() + ((JButton) e.getSource()).getText());

            } else {//没按的放在第一个运算
                yuns1.setText(yuns1.getText() + ((JButton) e.getSource()).getText());
            }
        }
        // jieguoField.setText(jieguoField.getText() + ((JButton)e.getSource()).getText());


        /*
         * String string = " "; string = jieguoField.getText();//获取当前文本框的内容字符串
         */            //e.getSource() 获取事件源

        //Object object =((JButton)e.getSource()).getText();
        //进行拼接

        //不知道是什么类型，知道是按钮
        //jieguoField.setText(jieguoField.getText() + object);
        //System.out.println("点鸡");
    }

    public void jisuan() {
        Float num1, num2, reFloats = 0f;//获取ab
        String yunsuanfuString;

        num1 = Float.parseFloat(yuns1.getText());//获取yuns1的文本，然后转换成浮点型
        num2 = Float.parseFloat(yuns2.getText());

        yunsuanfuString = fuhao1.getText();//字符串不用转

        //计算
        switch (yunsuanfuString) {
            case "+":
                reFloats = num1 + num2;
                break;

            case "-":
                reFloats = num1 - num2;
                break;

            case "*":
                reFloats = num1 * num2;
                break;

            case "/":
                reFloats = num1 / num2;
                break;
            default:
                break;

        }
        result.setText(reFloats.toString());
        fuhao2.setText("=");

    }

}




