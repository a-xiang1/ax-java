/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 12:18
 */

public class String01{
public static void main(String[] args) {
    /*public boolean equals(Object anObject) {
        // 1. 先检测this 和 anObject 是否为同一个对象比较，如果是返回true
        if (this == anObject) {
            return true;
        }

        // 2. 检测anObject是否为String类型的对象，如果是继续比较，否则返回false
        if (anObject instanceof String) {
            // 将anObject向下转型为String类型对象
            String anotherString = (String)anObject;
            int n = value.length;

            // 3. this和anObject两个字符串的长度是否相同，是继续比较，否则返回false
            if (n == anotherString.value.length) {
                char v1[] = value;
                char v2[] = anotherString.value;
                int i = 0;

                // 4. 按照字典序，从前往后逐个字符进行比较
                while (n-- != 0) {
                    if (v1[i] != v2[i])
                        return false;
                    i++;
                }
                return true;
            }
        }
        return false;
    }*/

       /* // s1和s2引用的是不同对象  s1和s3引用的是同一对象
        String s1 = new String("hello");
        String s2 = new String("world");
        String s3 = s1;

        System.out.println(s1.length());   // 获取字符串长度---输出5
        System.out.println(s1.isEmpty());  // 如果字符串长度为0，返回true，否则返回false

        // 打印"hello"字符串(String对象)的长度
        System.out.println("hello".length());*/

        int a = 10;
        int b = 20;
        int c = 10;

        // 对于基本类型变量，==比较两个变量中存储的值是否相同
        System.out.println(a == b);    // false
        System.out.println(a == c);    // true

        // 对于引用类型变量，==比较两个引用变量引用的是否为同一个对象
        //就是比较引用中的地址
        String s1 = new String("hello");
        String s2 = new String("hello");
        String s3 = new String("world");
        char ch = 'h';
        String s4 = s1;
        System.out.println(s1 == s2);   // false
        System.out.println(s2 == s3);   // false
        System.out.println(s1 == s4);   // true

        System.out.println(s1.equals(s2));
    }
    
}
