import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 10:59
 */
public class Writer {
    public static void main(String[] args) {
        //字符创建文件字符输出流对象
        FileWriter out = null;
        try {
            out = new FileWriter("C:\\java\\javam\\ax-java\\20240425\\kun.txt");
            //开始写
            char[] ch = new char[]{'坤','坤','打','篮','球'};
            out.write(ch);
            out.write(ch,1,3);

            //能直接写入一个字符串
            out.write("\n");
            out.write("\n我爱打篮球\n");



            //刷新
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if (out != null){
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
