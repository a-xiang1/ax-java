import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 10:41
 */
public class Reader {
    public static void main(String[] args) {
        FileReader reader = null;
        try {
            //字符输入流
            reader = new FileReader("C:\\java\\javam\\ax-java\\20240425\\kun.txt");
            //读
            /*char[] ch = new char[4];//一次读一个字符
            int readCont = 0;
            while(((readCont = reader.read(ch)) != -1)){
                System.out.print(new String(ch,0,readCont));
            }*/

            //一个一个读
            char[] ch = new char[10];
            reader.read(ch);
            for (char s : ch){
                System.out.println(s);
            }





        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {//关闭
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
