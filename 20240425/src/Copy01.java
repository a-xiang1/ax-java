import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 10:13
 */
//使用输入和输出实现一边读一边写，最后实现复制
//可以拷贝任意文件类型
public class Copy01 {
    public static void main(String[] args) {

        FileInputStream fis = null;

        FileOutputStream fos = null;

        try {
            //创建输入流
            fis = new FileInputStream("C:\\java\\javam\\ax-java\\20240425\\ 坤k.txt");
            //创建输出流
            fos = new FileOutputStream("C:\\ 坤k.txt");

            //复制
            //先读
            byte[] bytes = new byte[1024 * 1024];//每次读1mb大小
            int readCount = 0;//读多少写多少
            while(((readCount = fis.read(bytes)) != -1 )){
                fos.write(bytes,0,readCount);
            }

            fos.flush();//刷新
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

            //分开关闭，防止有一个关闭失败而影响另一个关闭
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
