import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 11:08
 */
//字符流的复制
public class Copy02 {
    public static void main(String[] args) {
        FileReader reader = null;
        FileWriter out  = null;

        try {
            reader = new FileReader("C:\\java\\javam\\ax-java\\20240425\\kun.txt");
            out = new FileWriter("C:\\java\\kun.txt");
            //开始读
            char[] ch = new char[4];
            int readCount = 0;
            while(((readCount = reader.read(ch)) != -1)){//读
                //写
                out.write(ch,0,readCount);
            }

            out.flush();//刷新
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null){
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        System.out.println(new String("ax"));
    }
}
