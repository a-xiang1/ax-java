import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-25
 * Time: 9:40
 */
//
public class FileOutPut {
    public static void main(String[] args) {
        //字节输出，写
        FileOutputStream fos = null;
        try {
            //没有文件会自动创建,但有个缺陷，就是每次都会对文件进行清空然后再写
            /*fos = new FileOutputStream("C:\\java\\javam\\ax-java\\20240425\\ 坤k.txt");
            byte[] bytes = new byte[]{97,98,65,99};
            fos.write(bytes);//全部写入
            fos.write(bytes,0,2);//写入一部分

            */

            //改掉清空的问题
            fos = new FileOutputStream("C:\\java\\javam\\ax-java\\20240425\\ 坤k.txt",true);//true表示追加
            byte[] bytes = new byte[]{97,98,65,99};
            fos.write(bytes);

            //
            String s = "坤坤爱打篮球";
            byte[] bs = s.getBytes();
            fos.write(bs);
            fos.write('\n');//换行
            fos.write(bs);
            fos.write(bs);
            fos.write('\n');//换行
            fos.write(bs);






            //写完记得刷新
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } finally{//关闭流
            if (fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
