import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-19
 * Time: 12:45
 */
public class Gather {

    public static void main(String[] arges) {
        System.out.println("韦坤四战四级");
        /*
         * Set<String>s1 = new HashSet<>(); Set<String>s2 = new HashSet<>();
         * s1.add("a"); s1.add("a");//set会自动去重,无序存储 s1.add("b"); s1.add("c");
         * s1.add("d");
         *
         * //s1.clear(); s2.add("A"); s2.add("B"); s2.add("C"); s2.add("D");
         * //set无法用普通for循环来遍历，只能用增强版for和迭代器处理 for(String a : s1) { System.out.print(" "
         * + a); }
         *
         * System.out.println("---------------");
         *
         * for(String a : s2) { System.out.print(" " + a); }
         *
         * System.out.println(); System.out.println(s1.contains("a"));//判断是否包含某元素
         * System.out.println(s2.contains("A"));
         *
         * System.out.println(s1.isEmpty());
         *
         * Set<String>s1 = new HashSet<>(); Set<String>s2 = new HashSet<>();
         * s1.add("a"); s1.add("a");//set会自动去重,无序存储 s1.add("b"); s1.add("c");
         * s1.add("d");
         */

        //s1.clear();
        Set<String> s1 = new HashSet<>();
        Set<String> s2 = new HashSet<>();
        Set<String> s3 = new HashSet<>();

        s1.add("a");
        s1.add("b");//set会自动去重,无序存储 s1.add("b"); s1.add("c");
        s1.add("c");
        s1.add("d");
        s1.add("e");

        s2.add("b");
        s2.add("e");
        s2.add("f");
        s2.add("g");

        s3.add("d");
        s3.add("e");

        for (String s : s1) {
            System.out.print(" " + s);
        }

        System.out.println(" ");
        System.out.println("并集后--------");
        //s1.addAll(s2);
        for (String s : s1) {
            System.out.print(" " + s);
        }

        System.out.println(" ");
        System.out.println("--------交集-------- ");

        for (String s : s1) {
            System.out.print(" " + s);
        }

        System.out.println(" ");
        System.out.println("交集后--------");
        //s1.retainAll(s2);
        for (String s : s1) {
            System.out.print(" " + s);
        }


        System.out.println(" ");
        System.out.println("--------差集-------- ");

        for (String s : s1) {
            System.out.print(" " + s);
        }

        System.out.println(" ");
        System.out.println("差集后--------");
        //s1.removeAll(s2);
        for (String s : s1) {
            System.out.print(" " + s);
        }


        System.out.println(" ");
        System.out.println("--------超集-------- ");

        for (String s : s1) {
            System.out.print(" " + s);
        }

        System.out.println(" ");
        System.out.println("超集后--------");
        //s1.containsAll(s2);
        System.out.println(s1.containsAll(s2));//s2不是s1的子集
        System.out.println(s1.containsAll(s3));//s3是s1的子集


        //迭代器
        Iterator it = s1.iterator();
        while (it.hasNext()) {
            System.out.print("  " + it.next());

        }

        Map<Integer, String> im = new HashMap<Integer, String>();
        im.put(1, "a");//
        im.put(2, "b");
        im.put(3, "c");
        im.put(1, "a");//去重，有就替换没有就新增*-----------------------------------------
        System.out.println(" ");
        //Map遍历
        Set<Integer> skey = im.keySet();
        for (Integer i : skey) {
            System.out.println(" key = " + i + " value " + im.get(i));
        }
        System.out.println(im.containsKey(5));
        System.out.println(im.containsValue("a"));

        Map<Integer, String> im2 = new HashMap<Integer, String>();
        im.put(10, "a");//
        im.put(11, "b");
        im.put(12, "c");

        im.putAll(im2);
        Set<Integer> skey2 = im.keySet();
        for (Integer i : skey2) {
            System.out.println(" key = " + i + " value " + im.get(i));
        }
        System.out.println(" 1");
        Collection<String> sv = im.values();

        for (String ss : sv) {
            System.out.print(ss + "   ");
        }
        System.out.println();
        System.out.println(" 1");
        Set<Map.Entry<Integer, String>> sm = im.entrySet();
        for (Map.Entry e : sm) {
            System.out.println(" key = " + e.getKey() + " value " + e.getValue());

        }

        //无序，set存的元素是无序的，没有下标就是
        Set<String> str = new TreeSet<>();
        str.add("a");
        str.add("f");
        str.add("d");
        str.add("b");
        for (String s : str){
            System.out.print(s + "  ");
        }


    }
}
