package setjava;

import com.sun.source.util.Trees;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-20
 * Time: 14:41
 */
public class Mapp {
    public static void main(String[] args) {
        System.out.println("韦坤四战");
        //HashMap集合类栗子
        //创建一个电话薄
        HashMap<String ,String > tes_list = new HashMap<>();
        //给集合添加元素
        //key键是唯一的，value可以重复
        tes_list.put("张三","18076656328");
        tes_list.put("李四","15007877041");
        tes_list.put("王五","18075258413");
        tes_list.put("老六","18075448433");
       //获取集合的所有元素
        Set<Map.Entry<String ,String >> te_list = tes_list.entrySet();
        for (Map.Entry t : te_list){
            System.out.println("姓名："  + t.getKey() + "   电话："+ t.getValue());
        }
        System.out.println("老六的电话号码为： " + tes_list.get("老六"));//查询key对应的电话号码

        //HashMap输出是乱序的，如果想要按顺序输出改为LinkedHashMap
        /*Map<String, String> map = new LinkedHashMap<String, String>();
        for (int i=0; i<5; i++) {
            map.put("key"+i, "value" + i);
        }
        for (Map.Entry<String, String> item : map.entrySet()) {
            System.out.println(item.getKey() + "==" + item.getValue());
        }*/

        //改用TreeMap集合类
        //TreeMap是可以实现自然的排序

        TreeMap<String ,String > te_lists = new TreeMap<>();
        te_lists.put("张3","18076656328");
        te_lists.put("李4","15007877041");
        te_lists.put("王5","18075258413");
        te_lists.put("老6","18075448433");
        Set<Map.Entry<String ,String>> lit = te_lists.entrySet();
        Iterator iter = lit.iterator();
        while(iter.hasNext()){

            System.out.println("  " + iter.next());
        }
        System.out.println("-----");





    }
}
