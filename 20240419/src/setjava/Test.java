package setjava;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-19
 * Time: 15:57
 */

//实现Map集合
/*
1.map和Collection没有继承关系
2.Map集合以key和value的方式存储数据： 键值对
  key和value都是引用数据类型
  key和value都是引用数据类型
  key起到主导的地位，value是key的一个附属品
 3.Map接口常用到的方法
 put(key,value)向Map集合中添加键值对
 get(Object key)通过key获取value
 void clear()清空Map集合
 //实现接口implements
 Set集合中元素类型是 Map.Entry<K,V>
 Map.Entry和String 一样，都是一种类型的名字，只不过：Map.Entry是静态内部类，是Map中的静态内部类
*/

public class Test {
    //声明一个静态内部类
    private static class InnerClass{
        //静态方法
        public static void m(){
            System.out.println("静态内部类，直接调用");
        }
        //静态内部实例方法，需要实例化才能调用
        public void m2(){
            System.out.println("实例方法");
        }
    }
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");
        Test.InnerClass.m();
        Test.InnerClass im = new Test.InnerClass();
        im.m2();

        //set集合中存储的对象是 Test.InnerClass类型
        Set<Test.InnerClass> set = new HashSet<>();

        //同理
        //Set<Map.Entry<K,V>>
        //创建Map集合对象
        Map<Integer,String> map  = new HashMap<>();
        //put向Map集合中添加键值对
         map.put(1,"韦坤");
         map.put(2,"爱");
         map.put(3,"打");
         map.put(4,"坤坤");
        map.put(45,"坤坤爆");
         //通过key获取value
        String value = map.get(1);
        System.out.println(value);

        //获取键值对的数量
        System.out.println("键对值数量：" + map.size());

        //勇敢key删除key 以及对应的value
        //map.remove(3);
        System.out.println("键对值数量：" + map.size());

        //判断是否包含某个key
        System.out.println(map.containsKey(45));
        //判断是否包含某个value
        System.out.println(map.containsValue("坤坤"));

        //获取所有的value
        Collection<String> values = map.values();
        for (String s : values){
            System.out.print(s + " ");
        }
        System.out.println();
        //获取所有的键值对集合视图
        Set<Map.Entry<Integer,String>> list = map.entrySet();
        for (Map.Entry t : list){ //Map.Entry就是一个静态内部类
            System.out.println(t.getKey()+"   " +t.getValue());
        }

        System.out.println(map.get(1));//通过key查询key对应的value


        //清空map集合
        map.clear();
        System.out.println(map.size());




    }
}
