package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 15:58
 */
//书这方面，就需要书的名字、书的作者、价格、书的类型是小说还是其他、是否被借阅；
public class Book {
    //都是设置私有成员
    private String name;//书名
    private String author;//书作者
    private int price;//书价格
    private String type;//书类型
    private boolean isborrowed;//是否被借阅
    //突然想到可以设置一个库存的

    //构造方法初始化，isborrwed默认flase不初始化
    public Book(String name, String author, int price, String type) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIsborrowed() {
        return isborrowed;
    }

    public void setIsborrowed(boolean isborrowed) {
        this.isborrowed = isborrowed;
    }


    //把他们转为中文
    @Override
    public String toString() {
        return "书名 ='" + name + '\'' +
                ", 作者 ='" + author + '\'' +
                ", 价格 =" + price +
                ", 类型 ='" + type + '\'' +"  "+
                 ((isborrowed == true) ?  "已被借出" : "未被借出");

    }
}
