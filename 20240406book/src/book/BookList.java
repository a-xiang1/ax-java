package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:02
 */
//书架，存放各类书
public class BookList {
    private  Book[] books;//创建书对象
    private int usedSize;//记录当前书架上实际存放的书数量
    private static final int DEFAULT = 10;
    public BookList() {
        this.books = new Book[DEFAULT];//空间10可以存放10本书
        //初始给3本书
        this.books[0] =new Book("三国演义","罗贯中",15,"小说");
        this.books[1] =new Book("西游记","吴承恩",20,"小说");
        this.books[2] =new Book("红楼梦","曹雪芹",12,"小说");
        usedSize = 3;
    }

    public int getUsedSize() {//返回目前书的数量
        return usedSize;
    }

    public void setUsedSize(int usedSize) {//在这里记录书的数量
        this.usedSize = usedSize;
    }
    //返回书所在位置
    public Book getBook(int pos){
        return books[pos];
    }
    //没搞懂这个是作甚的
    public void setBooks(int pos,Book book){
        books[pos] = book;//好像是在这个位置放一本书
    }
    public Book[] getBooks(){//返回书架的大小
        return books;
    }



}
