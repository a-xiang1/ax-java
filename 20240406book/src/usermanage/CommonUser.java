package usermanage;

import operate.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 18:19
 */
//普通用户
public class CommonUser extends User{

    //重写父类方法
    public CommonUser(String name) {
        super(name);
        this.ioperations = new IOPerate[]{//在这里给那些操作方法给空间，当选择好进来时候就初始化好了
                new ExitOperate(),//退出系统，对应好菜单的选项
                new FindOperate(),
                new BorrowOperate(),
                new ReturnOperate(),
                new ShowOperate()
        };
        System.out.println("普通用户");
    }

    @Override
    public int menu() {
        System.out.println("-----------普通用户-----------");
        System.out.println("-----------------------------");
        System.out.println("----------1. 查找图书---------");
        System.out.println("----------2. 借阅图书---------");
        System.out.println("----------3. 归还图书---------");
        System.out.println("----------4. 显示图书---------");//我觉得普通用户需要显示图书目录，更好查找书籍借阅
        System.out.println("----------0. 退出系统---------");//还能查是否被借阅一目即可
        System.out.println("-----------------------------");
        System.out.println("请输入你的操作： ");
        Scanner scanner = new Scanner(System.in);
        int choose2 = scanner.nextInt();
        return choose2;//返回你的选择

    }

}
