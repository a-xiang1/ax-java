package usermanage;

import book.BookList;
import operate.IOPerate;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 18:16
 */
//用户类又包括了普通用户和管理用户作为子类继承这个父类的name
public abstract class User {
    protected String name;//限定在定义的类的子类里面使用，也就是只能普通用户和管理员使用
    protected IOPerate[] ioperations; //定义一个接口数组，限定子类使用，因为接口绑定着那些操作类
    public User(String name) {
        this.name = name;
    }
    //菜单
    //这里为了向上转型的时候，父类能调用到子类的menu而写。
    public abstract int menu();//这里不需要实现，所以作为抽象类即可

    //在这里写一个能调用到对应的操作，接收用户选择的操作数，然后给书
    public void doOperation(int choose , BookList bookList){
        ioperations[choose].word(bookList);
    }

}
