package usermanage;

import operate.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 18:20
 */
//管理用户
public class IntendantUser extends User {

    public IntendantUser(String name) {
        super(name);
        this.ioperations = new IOPerate[]{
                new ExitOperate(),
                new FindOperate(),
                new AddOperate(),
                new DeleteOperate(),
                new ShowOperate()
        };
        System.out.println("管理员");
    }

    @Override
    public int menu() {
        System.out.println("-----------管理员-------------");
        System.out.println("-----------------------------");
        System.out.println("----------1. 查找图书---------");
        System.out.println("----------2. 新增图书---------");
        System.out.println("----------3. 删除图书---------");
        System.out.println("----------4. 显示图书---------");
        System.out.println("----------0. 退出系统---------");
        System.out.println("-----------------------------");
        System.out.println("请输入你的操作： ");
        Scanner scanner = new Scanner(System.in);
        int choose2 = scanner.nextInt();
        return choose2;
    }
}
