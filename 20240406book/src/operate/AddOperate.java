package operate;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:32
 */
//增加操作
public class AddOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("增加图书");
        //怎么增加
        //那肯定先输入吧
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入书名： ");
        String name = scanner.nextLine();

        System.out.println("请输入作者： ");
        String author = scanner.nextLine();

        System.out.println("请输入价格： ");
        int  price = scanner.nextInt();
        String kk = scanner.nextLine();//这个用来接收/n的

        System.out.println("请输入书类型： ");
        String type = scanner.nextLine();


//        原因是 nextLine() 方法不能放在 nextInt( 代码段的后面，
//        其实，他不是跳过你了，而是他已经有内容了，内容就是 \n。因为 nextInt()
//        接收一个整型字符，不会读取 \n，nextline() 读入一行文本，会读入 \n 字符

        //可以在 nextInt() 和 nextLine() 中间加一个 nextLine() 语句来接收这个 \n,用个东西接收掉
        //这个不知道为啥，不给输入直接跳过，针对String，如果是输入int倒不会
        //因为你实际的输入是包含一个换行的，你那块如果写nextInt的话，后面再加一个nextLine把刚才键盘输入的那个换行吞掉



        Book book = new Book(name,author,price,type);

        //输入完别直接录入先检查是否有这本书或者书架是否已经满了
        int count = bookList.getUsedSize();
        for (int i = 0; i < count ; i ++){
            Book  book1= bookList.getBook(i);//从书架了从0开始获取每一本书然后进行判断是否相同
            if(book1.getName().equals(name)){//按照姓名来比较
                System.out.println("这本书已经存在 ");
            }

        }
        //这个循环走完接着判断是否满了
        //怎么判断是否满了呢，就是目前的实际书量跟书架总量比较即可
        //这个不太懂，为啥就能调用length方法呢,我知道了因为返回的是数组，数组才会有length这个方法
        if(count == bookList.getBooks().length){
            System.out.println("书架已经满了，无法增加");
        }else{
            bookList.setBooks(count ,book);//把数组下标和一本书传过去，然后添加到对应的位置，默认末尾
            bookList.setUsedSize(count+1);
            //System.out.println("新增完成");
        }



    }
}