package operate;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:28
 */
//查找操作
public class FindOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("查找图书");
        //查找
        //用姓名配合eq来查找就行了
        System.out.println("请输入要查找的图书书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        //通过遍历来查找
        int count = bookList.getUsedSize();
        for (int i = 0; i < count ;i ++){
            Book book= bookList.getBook(i);//从书架了从0开始获取每一本书然后进行判断是否相同
            if (book.getName().equals(name)){//如果相等
                System.out.println("找到了这本书，信息如下 ：");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有这本书");

    }
}
