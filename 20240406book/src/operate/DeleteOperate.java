package operate;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:30
 */
//删除操作
public class DeleteOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("删除图书");
        //如何删除图书
        //先输入要删除的书名然后判断是否有这本书
        //删除其实就是后面的往前覆盖就行了，将最后一本书置为空

        System.out.println("请输入要删除的图书书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int count = bookList.getUsedSize();
        int pos = -1;//定义好下标
        int i= 0;
        for ( i = 0; i < count ;i ++){
            Book book1 = bookList.getBook(i);
            if (book1.getName().equals(name)){
                System.out.println("有这本书可以进行删除");
                pos=i;//下标
                break;//找到了就返回
            }

        }
        //判断一下是否找到
        if(i == count){
            System.out.println("没有这本书");
            return;
        }
        int j = 0;
        for (j = pos ; j < count-1;j ++ ){
            //[j] = [j+1]后往前移动
            //先获取一本书
            Book book = bookList.getBook(j+1);
            bookList.setBooks(j,book);
        }
        //让最后一位弄为空
        bookList.setBooks(j,null);
        bookList.setUsedSize(count-1);

    }
}
