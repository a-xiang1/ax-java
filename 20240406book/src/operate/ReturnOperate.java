package operate;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:34
 */
//归还图书
public class ReturnOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("归还图书");
        //先说名字，然后给true给改回来
        System.out.println("请输入要归还的图书书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int count = bookList.getUsedSize();

        //然后看看有没有这本书
        int i= 0;
        for ( i = 0; i < count ;i ++){
            Book book1 = bookList.getBook(i);
            if (book1.getName().equals(name)){
                if(book1.isIsborrowed()) //如果是false就是未被借阅就不用归还
                {
                    System.out.println("有这本书可以进行归还");
                    //pos=i;//下标
                    //找到了那就打印一下
                    System.out.println(book1);
                    //归还完成那就标记一下
                    book1.setIsborrowed(false);//变成true
                    break;//找到了就返回

                }
                else{
                    System.out.println("该书未被借阅，无需归还");
                    return;
                }


            }

        }
        //判断一下是否找到
        if(i == count){
            System.out.println("没有这本书");
            return;
        }

    }
}