package operate;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:29
 */
//借阅操作
public class BorrowOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("借阅图书");
        //先说名字
        System.out.println("请输入要借阅的图书书名： ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int count = bookList.getUsedSize();

        //然后看看有没有这本书
        //然后接着判断该书是否被借阅了
        int pos = -1;//定义好下标
        int i= 0;
        for ( i = 0; i < count ;i ++){
            Book book1 = bookList.getBook(i);
            if (book1.getName().equals(name)){
                if(book1.isIsborrowed())
                {
                    System.out.println("这本书已经被借阅");
                    return;


                }//如果是false就是未被借阅
                else {
                    System.out.println("有这本书可以进行借阅");
                    //找到了那就打印一下
                    System.out.println(book1);
                    //借阅完成那就标记一下
                    //pos = i;
                    book1.setIsborrowed(true);//变成true
                    return;
                }



            }

        }
        //判断一下是否找到
        if(i == count){
            System.out.println("没有这本书");
            return;
        }




    }
}
