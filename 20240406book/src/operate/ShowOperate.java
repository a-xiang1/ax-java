package operate;

import book.Book;
import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:35
 */
//显示图书
public class ShowOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("图书列表");
        //遍历图书即可
        int count = bookList.getUsedSize();
        for(int i = 0;i < count ; i ++){
            Book book = bookList.getBook(i);//每次定义一本临时变量书，然后打印出来
            System.out.println(book);
        }
    }
}
