package operate;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 16:31
 */
//退出系统
public class ExitOperate implements IOPerate{
    @Override
    public void word(BookList bookList) {
        System.out.println("退出系统");
        System.exit(0);
    }
}
