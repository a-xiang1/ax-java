import book.Book;
import book.BookList;
import usermanage.CommonUser;
import usermanage.IntendantUser;
import usermanage.User;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 18:22
 */
//主类实现调用
public class Main {
    //注册登录
    public static User login(){
        System.out.println("请输入你的姓名： ");
        Scanner scaner = new Scanner(System.in);//创建输入类
        String name = scaner.nextLine();//lin输入一串字符串

        System.out.println("请输入你的身份，1：管理员 2：普通用户  ");
        int choose = scaner.nextInt(); //输入一个整型
        //在这里卡一下，如果不是1和二咋办
        while (true){
            if (choose == 1 || choose ==2){
                break;
            }
            System.out.println("输入错误请重新输入 : ");
             choose = scaner.nextInt();
        }

        if(choose == 1){
            //管理员
            //把姓名传进去进行构造方法的完成，然后返回到用户那里，实现向上转型
            return new IntendantUser(name);
        }else {
            //普通用户
            return new CommonUser(name);

        }

    }

    public static void main(String[] args) {

        //初始化书
        BookList booklist = new BookList();
        User user = login();
        while(true){
            //注册登录系统
            //返回来的user类
            //不管是普通用户还是管理员，这是不知道的，属于动态绑定，我只需使用父类进行调用即可。
            int choose = user.menu();//取到用户输入的操作
            System.out.println("choos:  "+ choose);
            //选择好之后开始进行对应的操作
            user.doOperation(choose,booklist);
        }
        //最终实现目标吧
        //就是能把数据进行存储，运行一次输入的数据，再次运行还在。
        //c语言的话可以用文件操作来实现，不过java就不会
        //java的话要实现持久化的存储
        //1.存到文件里面，2.存到数据库当中
        //后期可以做成网页交互



        //现在我想实现每个书都有自己的库存，比如我进货了这本书50本，那么库存就是50；
        //如果有10人借去了10本，那么就显示已被借阅10本剩余40本可以借

    }
}
