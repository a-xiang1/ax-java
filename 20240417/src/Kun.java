import java.util.AbstractList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-17
 * Time: 16:25
 */

class Student{
    String name;
    int age;
    public Student(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }
    public void show() {
        System.out.println(name);
        System.out.println(age);
    }
    @Override
    public String toString() {
        return "Student [name=" + name + ", age=" + age + "]";
    }

}

public class Kun {
    public static void main(String[] args) {
//        Basket<String> bs = new Basket<>();//<>传什么类型进行都行，替代T

        List<Integer>li = new AbstractList<Integer>() {
            @Override
            public Integer get(int index) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };
        //List<Integer>li =new ArrayList<Integer>();
        li.add(10);//在后尾加有序的
        li.add(20);
        li.add(1, 15);
        //size为集合的长度
        for(int i =0; i < li.size(); i ++) {
            System.out.print(li.get(i) + "  ");
        }
        System.out.println();
        for(Integer j : li) {

            System.out.print(j + "  ");
        }
        System.out.println( "  ");
        System.out.println( "  ");
        //数组能快速的获取数据和读取数据
        //链表能快速的插入和删除数据
        List<Student>s1 =new LinkedList<>();


        s1.add(new Student("韦坤",20));//只用一次直接new

        Student s = new Student("kunk",21);
        s1.add(s);
        s1.add(new Student("aa",15));

        for(Student t : s1) {
            System.out.println(t);
        }
        System.out.println( "  ");
        System.out.println( "  ");
        s1.remove(s);
        s1.remove(0);//传下标删除

        for(Student t : s1) {
            System.out.println(t);
        }

        System.out.println( "  ");
        System.out.println( "  ");

        //迭代器输出
        //定义迭代器的 对象
        Iterator iter = s1.iterator();

        while(iter.hasNext()) {

               System.out.println(iter.next());
        }


        System.out.println( "  ");
        System.out.println( "  ");

        LinkedList<Student> s5 = new LinkedList<Student>();
        s5.addFirst(new Student("99",45));
        s5.addLast(new Student("kkk",50));
        for(Student t : s5) {
            System.out.println(t);
        }
        System.out.println( "  ");
        System.out.println( "  ");

        System.out.println( s5.getFirst());

        System.out.println(s5.peek());//获取第一个
        s5.push(new Student("500",50));//入栈入的是头部
        System.out.println( "  ");
        for(Student t : s5) {
            System.out.println(t);
        }
        System.out.println( "  ");
        System.out.println( "  ");

        System.out.println(s5.pop());
        System.out.println( "  ");//出栈出头部


    }
}

//class Basket<T>{//T只能是类和接口
//    int n,total;
//    private Object[] arr;
//
//    public void add(T t) {
//
//    }
//    public T indexof(int i ) {
//        return (T)arr[i];
//    }
//}



