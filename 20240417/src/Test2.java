/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-17
 * Time: 17:36
 */
//泛型参数的限定
class TestG<T extends Number>{ //泛型的上界，就是这个T类型不能是其他类只能是Number的子类

}

//我要使用泛型类求数组中的最大值
//在这里继承一个Comparable就是用来约束那些只有实现了Comparable接口的类才可以使用这个泛型
//T就一定要实现了Comparable接口才不会报错
class Tesg<T extends Comparable<T>>{
    public T finMax(T[] array){
        T max = array[0];
        for (int i = 1; i < array.length; i ++){
            if (max.compareTo(arr[i]) < 0){//返回值小于0说明max小于arr[i]
                max = array[i];

            }
        }
        return max;
    }

}
public class Test2 {
    public static void main(String[] args) {
        TestG<Number> test = new TestG<>();
        TestG<Integer> tesa = new TestG<>();
        //TestG<String> te = new TestG<String>();//String不是Number子类

        Tesg<Integer> tes = new Tesg<>();
        Integer[] arr = new Integer[]{1,2,5,7,1,6,4};
        tes.finMax(arr);

    }
}
