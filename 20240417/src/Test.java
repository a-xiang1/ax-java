import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-17
 * Time: 17:10
 */
//自定义异常类
class MyEx extends Exception{//自定义类，必须基于Exception派生
    String message;//提示异常信息

    public MyEx(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}

public class Test {
    public static void main(String[] args) {
        try{

            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入用户的姓名、年龄");
            String name = scanner.nextLine();
            int age = scanner.nextInt();
            if (age < 0){//如果年龄为负数则抛出异常
                throw new MyEx("年龄不能为负数");//传参构造方法
            }
            System.out.println("该用户的基本信息是：");
            System.out.println("姓名： " + name +"   "+ "年龄" + age);

        } catch (MyEx e) {//捕获MyEx这个类型的异常
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("检查完毕");
        }


    }
}
