import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-09
 * Time: 13:05
 */
public class ZuJian extends JFrame {


        private JFrame ck ;

        public void init() {

            JPanel dingc = initfor();
            JScrollPane dingcScrollPane = new JScrollPane(dingc);
            initFrame(dingcScrollPane);
            ck.setDefaultCloseOperation(3);
            ck.setVisible(true);

        }

    private JPanel initfor() {
        ck = new JFrame();
        JPanel dingc  = new JPanel();
        ck.setTitle("注册页面");
        JPanel one = new JPanel();
        one.add(new JLabel("姓名"));
        one.add(new JTextField(15));

        JPanel two = new JPanel();
        two.add(new JLabel("性别"));

        //单选按钮
        JRadioButton nan = new JRadioButton("男",true);//默认选男
        JRadioButton nv = new JRadioButton("女");
        JRadioButton qt = new JRadioButton("其他");

        //分组,实现只能同时选中一个
        ButtonGroup fz = new ButtonGroup();
        fz.add(nan);
        fz.add(nv);
        fz.add(qt);

        //将按钮加到窗格里面
        two.add(nan);
        two.add(nv);
        two.add(qt);


        //密码标签
        JPanel three = new JPanel();
        three.add(new JLabel("密码"));
        three.add(new JPasswordField(15));//密码标签会以**代替


        //爱好标签
        JPanel four = new JPanel();
        four.add(new JLabel("爱好"));
        four.add(new JCheckBox("坤歌",true));//复选按钮
        four.add(new JCheckBox("打篮球"));
        four.add(new JCheckBox("坤坤爆"));
        four.add(new JCheckBox("csgo"));

        //下拉列表
        JPanel five = new JPanel();
        five.add(new JLabel("列表"));
        //创建好列表类，再放到列表里面
        JComboBox<String> cmBox = new JComboBox<>();
        cmBox.addItem("坤坤爆");
        cmBox.addItem("java");
        cmBox.addItem("坤坤篮球");
        five.add(cmBox);


        //自我介绍
        JPanel six = new JPanel();
        six.add(new JLabel("自我介绍"));
        //six.add(new JTextArea(6,12));
        //JTextArea area = new JTextArea();//创建文本区无参数情况
        JTextArea area = new JTextArea(6,12);
        six.add(new JScrollPane(area));//给窗口添加带滚动条的文本区

        //注册按钮
        JPanel zhuce = new JPanel();
        zhuce.add(new JButton("注册"));


        dingc.add(one);
        dingc.add(two);
        dingc.add(three);
        dingc.add(four);
        dingc.add(five);
        dingc.add(six);
        dingc.add(zhuce);
        //dingc.add()
        dingc.setLayout(new BoxLayout(dingc, BoxLayout.Y_AXIS));
        ck.add(new JScrollPane(dingc));//在要进行滑动滚轮的前面加就可以了
        return dingc;
    }

    private void initFrame(JScrollPane dingcScrollPane) {
        ck.add(new JScrollPane(dingcScrollPane));

        ck.setSize(300,300);
        ck.setLocation(300, 300);
    }

    public void bor(){
        ck = new JFrame();
        ck.setTitle("注册页面");
        JPanel dingc = new JPanel();

        dingc.setLayout(new BorderLayout()); //设置布局管理器
        //dings.setLayout(new GridLayout());//网格布局管理器

        //JButton按钮类
        JButton but1 = new JButton("按钮1");
        JButton but2 = new JButton("按钮2");
        JButton but3 = new JButton("按钮3");
        JButton but4 = new JButton("按钮4");
        JButton but5 = new JButton("按钮5");
        JButton but6 = new JButton("按钮6");

        //dings.add(but1.Border)
        dingc.add(but1,BorderLayout.EAST);
        dingc.add(but2,BorderLayout.WEST);//这是逗号
        dingc.add(but3,BorderLayout.SOUTH);
        dingc.add(but4,BorderLayout.NORTH);
       /* dingc.add(but5,BorderLayout.CENTER);
        dingc.add(but6,BorderLayout.CENTER);*/


        //要实现两个按钮共用第五个按钮区域
        JPanel cet = new JPanel();
        cet.add(but5);
        cet.add(but6);
        dingc.add(cet,BorderLayout.CENTER);


        ck.add(dingc);
        ck.add(new JScrollPane( dingc ) );
        ck.setSize(300,300);
        ck.setLocation(400, 300);
        ck.setVisible(true);
        ck.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}

