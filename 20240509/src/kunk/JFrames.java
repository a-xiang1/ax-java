package kunk;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-09
 * Time: 13:18
 */
public class JFrames extends JFrame {

    private JFrame ck;
    public void init() {
        ck = new JFrame();
        ck.setTitle("注册页面");
        JPanel dingc = new JPanel();

        JPanel one = new JPanel();
        one.add(new JLabel("姓名"));
        one.add(new JTextField(15)); // 单行文本输入框

        JPanel two = new JPanel();
        two.add(new JLabel("性别"));
        JRadioButton nan = new JRadioButton("男");
        JRadioButton nv = new JRadioButton("女",true);
        JRadioButton qt = new JRadioButton("其他");
        ButtonGroup br = new ButtonGroup();
        br.add(nan);br.add(nv);br.add(qt);
        two.add(nan);two.add(nv);two.add(qt);

        JPanel three = new JPanel();
        three.add(new JLabel("密码"));
        three.add(new JPasswordField(15));

        JPanel four = new JPanel();
        four.add(new JLabel("爱好"));
        four.add(new JCheckBox("音乐"));
        four.add(new JCheckBox("体育"));
        four.add(new JCheckBox("读书"));
        four.add(new JCheckBox("游戏",true));

        JPanel five = new JPanel();
        five.add(new JLabel("列表"));
        JComboBox<String> cm = new JComboBox<>();
        cm.addItem("java");
        cm.addItem("C++");
        cm.addItem("Python");
        cm.addItem("PHP");
        five.add(cm);

        JPanel six = new JPanel();
        six.add(new JLabel("自我介绍"));
        six.add(new JScrollPane( new JTextArea(6,30) ) );

        JPanel seven = new JPanel();
        seven.add(new JButton("提交"));
        seven.add(new JButton("重置"));

        dingc.add(one);
        dingc.add(two);
        dingc.add(three);
        dingc.add(four);
        dingc.add(five);
        dingc.add(six);
        dingc.add(seven);
        dingc.setLayout(new BoxLayout(dingc, BoxLayout.Y_AXIS));

        ck.add(new JScrollPane( dingc ) );

        ck.setSize(300,300);
        ck.setLocation(300, 300);
        ck.setVisible(true);

        ck.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


    public void bor() {
        ck = new JFrame();
        ck.setTitle("注册页面");
        JPanel dingc = new JPanel();

        dingc.setLayout(new BorderLayout());
        JButton but1 = new JButton("按钮1");
        JButton but2 = new JButton("按钮2");
        JButton but3 = new JButton("按钮3");
        JButton but4 = new JButton("按钮4");
        JButton but5 = new JButton("按钮5");
        JButton but6 = new JButton("按钮6");

        dingc.add(but1,BorderLayout.EAST);
        dingc.add(but2,BorderLayout.WEST);
        dingc.add(but3,BorderLayout.SOUTH);
        dingc.add(but4,BorderLayout.NORTH);


        JPanel cet = new JPanel();
        cet.add(but5);
        cet.add(but6);
        dingc.add(cet,BorderLayout.CENTER);

        ck.add(dingc);
        ck.add(new JScrollPane( dingc ) );
        ck.setSize(300,300);
        ck.setLocation(400, 300);
        ck.setVisible(true);
        ck.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }



}
