import org.w3c.dom.ls.LSOutput;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-06
 * Time: 11:29
 */
//内部类
class Base{
    public int date1 = 10;

    class Per{
        int date1 = 111;
        public int date2 = 2;
        public static final  int date3 = 3;//static不能在内部类定义，如果非要定义，加上final
        private int date4 = 4;
        public void test(){
            System.out.println(this.date1);
            //要想访问外部类的同名成员变量类名.this.成员
            System.out.println(Base.this.date1);
            System.out.println(date2);
            System.out.println(date3);
            System.out.println(date4);
        }
        public void test2(){//在类里面可以直接实例化
            Per per2 = new Per();
            per2.test();
        }

    }

    //静态内部类
    static class InnerClass{
        public int b = 2;
        int date1 = 20;
        public void test(){
            Base bas = new Base();//访问外部类
            System.out.println(b);
            System.out.println(date1);
            System.out.println(bas.date1);
        }
    }


}

interface Jko{
    void test();
}
public class NeiBu {
    public static void main(String[] args) {
        //如何实例化内部类，先实例化外部类
        Base base = new Base();
        Base.Per per = base.new Per(); //这样就实例化内部类了
        per.test();
        System.out.println("--------------------------");
        //实例化静态类
        Base.InnerClass innerclass = new Base.InnerClass();
        innerclass.test();

        //匿名内部类的使用
       Jko jk = new Jko(){
            @Override
            public void test() {
                System.out.println("重写接口方法");
            }
        };
       jk.test();


        new Jko(){
            @Override
            public void test() {
                System.out.println("重写接口方法");
            }
        }.test();



    }

}
