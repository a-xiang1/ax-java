package TEST;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-11
 * Time: 12:18
 */
public class MyJFrame03  extends JFrame  implements KeyListener {
    public MyJFrame03()  {
        //键盘监听
        //创建窗口对象
        JFrame jFrame = new JFrame();
        //设置界面的宽高
        jFrame.setSize(600,600);
        jFrame.setTitle("鼠标演示");//标题
        jFrame.setAlwaysOnTop(true);//设置窗口界面置顶
        jFrame.setLocationRelativeTo(null);//设置界面剧中
        jFrame.setDefaultCloseOperation(3);//关闭模式
        jFrame.setLayout(null);// 取消默认放置按钮啥的

        //给整个窗体添加键盘监听
        //调用者this：指代本类对象即为界面对象 jFrame，
        //ddKeyListener给整个界面添加监听
        //参数this 当事件触发后，会执行本类KeyListener接口中实现的方法

        //this.addKeyListener(this);
        jFrame.addKeyListener(this);



        jFrame.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {//这个局限性多不用

    }

    //按下不松
    /*
    如果按下键不松开一直调用按下不松的函数
    键盘那么多按键如何区分
     */
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("按下不松");
    }
    //松开按键
    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("松开按键");
        int code = e.getKeyCode();
        System.out.println(code);
        //对一些按键进行监听
        if (code == 65){
            System.out.println("A");
        }
        else if (code == 66){
            System.out.println("B");
        }
    }
}
