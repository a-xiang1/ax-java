import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-16
 * Time: 10:16
 */
public class AxJava {

    //数组的引用
    public static void arr(){
        int[] ax = new int[3];
        ax[1] = 10;
        ax[2] = 20;
        ax[0] = 30;
        /*for(int i = 0;i < ax.length ;i ++)
        {
            System.out.println(ax[i] + " ");
        }*/

        int[] ax2 = new int[]{1,2,3};
        ax = ax2;//此时ax数组引用指向了ax2这个引用所指向的对象
        //那么之前ax的引用对象被系统回收，因为没有被指向。
        ax[1] = 20;
        ax[2] = 30;
        ax2[0] = 50;//修改的是ax2数组的内容。
        for(int i = 0;i < ax2.length ;i ++)
        {
            System.out.println(ax2[i] + " ");
        }
    }

    //数组的传参
    public static void fun1(int[] arr1){
       for (int i = 0 ;i < arr1.length ; i ++){
           arr1[i] = i;
       }

    }
    public static void fun2(int[] arr2){
        arr2 = new int[]{11,22,33,44};//这个的话是本来是指向arr，后面新new一个数组，指向新的引用对象
    }
    public static void fun3(int[] arr3){
        arr3[1] = 55;//arr3是指向arr的引用对象，可以修改内容。
    }

    //数组作为返回值
    public static int[] fun4(){
        int[] ret = {11,22,33};
        return ret;
    }

    //模拟实现Arrays.toString
    public static String arrays(int[] arr){
        String ret = "[";
        for(int i = 0 ;i < arr.length ; i ++){
            ret += arr[i] ;//就是把数组拼接起来。
            if(i !=arr.length -1){
                ret += ", ";
            }
        }
        //拼接玩数组内容，然后加上]
        ret += "]";
        return ret;
    }

    //实现通过传参，改变将数组内容都变大一倍
    public static void fun5(int[] arr3){
        for (int i = 0; i < arr3.length ;i ++){
            arr3[i] *= 2;
        }

    }
    public static int[] fun6(int[] arr3){
       int[] ret = new int[arr3.length];
       for(int i = 0 ; i < arr3.length ; i++){
           ret[i] = 2 * arr3[i];
       }
       return ret;

    }

    //实现奇数放前面，偶数放后面
    public static void fun8(int[] arr){
        int i = 0;
        int j = arr.length - 1;
        while(i < j){
            //当第一轮结束后，第二轮，这个时候i和j已经变了，那么下面的while条件都要重新判定。
            while(i < j && arr[i] %2 != 0){
                i ++;
            }
            //过一遍寻找偶数
            while(i < j && arr[j] % 2 == 0){
                j --;
            }
            //过一遍寻找奇数
             int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;

        }

    }
    public static void fun7(){
        int[] arr = {1,2,3,4,5};
        fun8(arr);
        for (int x : arr){
            System.out.print(x + " ");
        }

    }

    //实现二分查找
    public static int binarySearch(int[] arr, int key ){
        int i = 0;
        int j = arr.length - 1;
        int mid;
        while(i <= j){
            mid = (i + j) /2;
            if( arr[mid] < key){
                i = mid + 1;
            }
            else if(arr[mid] == key){
                return mid ;//寻找成功返回下标
            }
            else{
                j = mid - 1;
            }
        }
        return -1 ;
    }

    //给定一个数组和一个整数目标值，要在这个数组中找出和为目标值target的两个整数
    //然后返回他们的数组下标，注意，数组中同一个元素的答案只能出现一次，就是只能是一组
    //比如求6，只能有一组的和是6，不能有其他的
    public static void fun9(int[] arr, int key){
        int i = 0;
        int j = 0;
        while(i < arr.length-1){

            while(j < arr.length){
                if( arr [i] + arr[j] == key){
                    System.out.print(i +"  "+ j);
                    System.out.println();
                }
                j++;
            }
            i++;
            j = i;
        }


    }

    public static void main(String[] args) {
        //数组的引用
        /*arr();*/

        //数组的传参
        //int[] arr = {1,2,3,4};
        //fun1(arr);//这是通过循环依次把数组里的数据进行遍历修改
       //fun2(arr);//这个的话是本来是指向arr，后面新new一个数组，指向新的引用对象，所以最后数组的内容并没有修改
        //fun3(arr);
       /* for (int x : arr){
            System.out.print(x + " ");
        }*/

        //数组作为返回值使用
       /*int[] ret = fun4();
       for(int i : ret){
           System.out.println(i);
       }*/

        //强大的Arrays,可以快速打印数组，可以给数组快速排序
       /* int[] arr2 = {100,55,101,60};*/
        //System.out.println(Arrays.toString(arr2));//快速打印数组
        //Arrays.sort(arr2);//这个进行排序然后输出。
        //Arrays.sort(arr2,0,2);//还能对这个数组指定排序几个，以及排序其他类型的数组。
        //System.out.println(Arrays.toString(arr2));
        //System.out.println(Arrays.sort(arr2));

        //用方法模拟实现Arrays.toString
        /*String tostring = arrays(arr2); //模拟实现成功
        System.out.println(tostring);
        System.out.println(Arrays.toString(arr2));
        */

        //实现通过传参，改变将数组内容都变大一倍
        /*int[] arr3 = {1,2,3,4};
        fun5(arr3);//这是不需要返回值的写法
        int[] ret =  fun6(arr3);//这是需要返回值。
        for(int x : arr3){
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.println(Arrays.toString(ret));*/

        //实现奇数放前面，偶数放后面
       /* fun7();//成功，算法着呢不错*/

        //实现二分查找
        /*int[] arr = {2,4,6,8,9};
        System.out.println(binarySearch(arr,5));
        System.out.println(binarySearch(arr,9));
        int a = Arrays.binarySearch(arr,9);//使用arrays来查找
        System.out.println(a);*/

        //给定一个数组和一个整数目标值，要在这个数组中找出和为目标值target的两个整数
        //然后返回他们的数组下标，注意，数组中同一个元素的答案只能出现一次，就是只能是一组
        //比如求6，只能有一组的和是6，不能有其他的
        /*int[] arr = {1,5,3};
        fun9(arr,4);*/

        //一个数组中只出现一次的数
        /*int[] arr = {2,5,5,2,1};
        int ret = 0;
        //全部异或一遍即可。
        for (int i = 0 ; i < arr.length; i++){
            ret ^= arr[i];
        }
        System.out.println(ret);*/

        //在一个数组中是否存在三位连续的奇数
        int[] arr = {2,1,3,5,6};
        int i = 0;
        int count = 0;
        while(i < arr.length){

            if(arr[i] % 2 != 0) {//奇数的条件
                count++;
            }
            //怎么判断连续呢
            else{//不是奇数的话那么就是偶数，这时候让count重置为0
                count = 0;
            }
            if(count == 3){
                System.out.println("存在三位连续的奇数");
            }
            i ++;
        }

    }

}
