package jjj;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuBarcc {

	private  JMenuBar menu;

	
	public MenuBarcc() {
		super();
		this.menu = new JMenuBar();
	}


	public JMenuBar getmenus() {
		JMenu m1 = new JMenu("开始");
		JMenu m2 = new JMenu("插入");
		JMenu m3 = new JMenu("设计");
		JMenu m4 = new JMenu("结束");
		
		JMenu m01 = new JMenu("剪切");
		m1.add(m01);//相当于嵌套在m1里面
		
		
		JMenuItem m001 = new JMenuItem("复制");
		JMenuItem m002 = new JMenuItem("剪切");
		m01.add(m001);
		//加一个分割线
		m01.addSeparator();
		m01.add(m002);
		
		menu.add(m1);
		menu.add(m2);
		menu.add(m3);
		menu.add(m4);
		
		//放入菜单
		//menu.setJMenuBar(menu);
		
		
		//menu.setSize(200,300);//设置窗口大小
		//menu.setResizable(false);//让窗口无法进行放大
		//menu.setVisible(true);//设置窗口可见
		//设置窗口显示坐标
		//jFrame.setLocation(800,250);
		//menu.setLocationRelativeTo(null);
		
		return menu;
}
}
