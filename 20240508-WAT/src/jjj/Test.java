package jjj;

import java.awt.Frame;
import java.awt.HeadlessException;

import javax.swing.JFrame;


public class Test extends Frame {
	private JFrame jFrame;//窗口

	public Test(String name) throws HeadlessException {
		super();
		this.jFrame = new JFrame(name);
	}
	
	public void init() {
		//jFrame.setTitle("窗口命名");
		jFrame.setSize(200,300);//设置窗口大小
		
		jFrame.setJMenuBar(new MenuBarcc().getmenus());
		jFrame.setResizable(false);//让窗口无法进行放大
		jFrame.setVisible(true);//设置窗口可见
		//设置窗口显示坐标
		//jFrame.setLocation(800,250);
		jFrame.setLocationRelativeTo(null);
		
	}
}
