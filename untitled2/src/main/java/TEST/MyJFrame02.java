package TEST;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-11
 * Time: 11:40
 */
public class MyJFrame02 extends JFrame implements MouseListener {
    JButton jtb1  = new JButton("点我");

    public MyJFrame02() {
        //创建窗口对象
        JFrame jFrame = new JFrame();
        //设置界面的宽高
        jFrame.setSize(600,600);
        jFrame.setTitle("鼠标演示");//标题
        jFrame.setAlwaysOnTop(true);//设置窗口界面置顶
        jFrame.setLocationRelativeTo(null);//设置界面剧中
        jFrame.setDefaultCloseOperation(3);//关闭模式
        jFrame.setLayout(null);// 取消默认放置按钮啥的
        //设置位置和宽高

        jtb1.setBounds(0,0,100,50);
        //给按钮增加鼠标
        jtb1.addMouseListener(this);


        //放到界面中
       jFrame.add(jtb1);




       jFrame.setVisible(true);


        //设置位置和宽高




    }

    //单击
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("单击");
    }
    //按下不松
    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("按下不松");
    }
    //松开
    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("松开");
    }

    //划入
    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("划入");
    }
    //划出
    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("划出");
    }
}
