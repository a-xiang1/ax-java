package TEST;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-09
 * Time: 20:09
 */
public class Test01 {
    public static void main(String[] args) {
        /*
        需求
        把一个一维数组中数组 0- 9打乱顺序
        然后按照3个一组方式添加到二维数组中
         */

        //定义一位数组
        int[] arr = new int[]{0,1,2,3,4,5,6,7,8};//0虽然没有，但是他会是空的，拼图需要空的位置

        //2.打乱数组中的数据顺序
        //遍历数组，得到每一个元素，拿着每一个元素跟随机索引上的数组进行交换
        Random random = new Random();
        for (int i  = 0; i < arr.length ; i ++ ){
            //获得随机索引
            int index = random.nextInt(arr.length);//限制随机数的范围,虽然会重复但是没啥影响，打乱就行
            //随机交换
            int temp = arr[i];
            arr[i] = arr[index];
            arr[index] = temp;

        }
        for (int i = 0 ; i< arr.length ;i ++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();

        //3.分组放到二维数组里面
        int[][] arrs = new int[3][3];
        for (int i = 0 ; i < arr.length ; i ++){
            arrs[i / 3][ i % 3] = arr[i];//每组3个
        }
        for (int i = 0 ; i < arrs.length; i ++){
            for (int j = 0 ; j < arrs[i].length ; j ++){
                System.out.print(arrs[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

    }
}
