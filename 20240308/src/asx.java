import org.w3c.dom.css.CSSUnknownRule;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-08
 * Time: 13:19
 */
public class asx {
   /* public static void main(String[] args) {
        System.out.println("hell 韦坤");
        //System.out.println();//sout+tab得到println
        byte a = 100;//byte 只有一个字节，2的7次方，最大只能是127
        //输出Long类型的最大值和最小值
        System.out.println(Long.MAX_VALUE);//ctrl+d能快速粘贴上一行的内存
        System.out.println(Long.MIN_VALUE);

        float f = 12.5f;//这里需要加f不然默认为double
        System.out.println(f);
        //固定输出多收位
        System.out.printf("%f",f);

        //char在java这里是2个字节，可以放一个汉字，一个汉字需要两个字节大小。
        char b = '祥';
        System.out.println(b);

        //强制转换，结果自己负责，就是可能丢失数据
        // 就比如说当你int型4个字节的数据强制转换成long 8个字节数据类型
        int w = 10 ,q = 20 ;
        long c = w + q ;
        System.out.println(c);

        //整型转换
        //就是范围小的会提升为范围大的进行计算。
        byte e = 10 ;
        byte t = 20 ;
        //这样写会报错，因为吧计算机是按照四个字节来读取数据的
        //假如byte和short低于4个字节，那么就会提升为int进行计算。
        //但在这里y是byte型的，所以会报错。
        //byte y = e + t;

        //正确2个解法
        //int  y = e + t;
        byte y = (byte)(e + t) ;
        System.out.println(y);*/

    public static void main(String[] args) {

        //java字符串类型
       /* String a = "abcd" ;
        String b = "erty" ;
        String c = a + b ;//字符串可以相加
        System.out.println(c); */

        //还有一种拼接
       /* int a = 10;
        int b = 20;
        System.out.println("a = " + a + " b = " +b); //这个+号在这里并不是相加那种计算，而是单纯的拼接
        System.out.println("a + b = " + a + b); //这里以字符串一样拼接起来。因为用到“"双引号，当做字符串处理。
        System.out.println(a + b + "a + b "  );//从左到右计算的，先加然后再拼接字符串的。
        System.out.println("a + b = " +(a + b));//这个才是计算相加。*/

       /* //string字符串类型转换为int类型
        String str = "123" ;
        int a = Integer.parseInt(str);//但是前提 字符串必须是合法的数字字符串
        System.out.println(a+1);*/

      /*  //string字符串类型转换为int类型
        int a = 10 ;
        String str = a + "";
        System.out.println(str);

        //法二
        String STR =String.valueOf(a);
        System.out.println(STR);*/

        //就是范围小的能赋值给范围大的，反过来会报错。
        int a = 100;
        long b = 10L;
        b = a;   // a和b都是整形，a的范围小，b的范围大，当将a赋值给b时，编译器会自动将a提升为long类型，然后赋值
        //a = b;   // 编译报错，long的范围比int范围大，会有数据丢失，不安全






    }




}

