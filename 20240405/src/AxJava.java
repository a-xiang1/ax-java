import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-05
 * Time: 10:18
 */

class BA implements Cloneable{//设置支持拷贝
    public int a = 10;

    @Override
    protected Object clone() throws CloneNotSupportedException {//这里也要重新克隆方法，不然无法使用
        return super.clone();
    }
}
// 克隆接口Clonable

class Animal implements Cloneable {
    public String name;
    public int age;

    public BA ba = new BA();

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override//浅拷贝
/*//    protected Object clone() throws CloneNotSupportedException {
//        //throws CloneNotSupportedException 可能会异常 ，要解决异常，直接加throws CloneNotSupportedException
//        return super.clone();
//    }*/
    protected Object clone() throws CloneNotSupportedException {
        //throws CloneNotSupportedException 可能会异常 ，要解决异常，直接加throws CloneNotSupportedException
        Animal tmp = (Animal) super.clone();//用临时创建的对象来接收
        tmp.ba = (BA) this.ba.clone();
        return tmp;

    }

    //重写equals
    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Animal)) return false;
//        Animal animal = (Animal) o;
//        return age == animal.age && Objects.equals(name, animal.name) && Objects.equals(ba, animal.ba);
        //上面这个写法也是一样好像
        //this指的是an2，传过来的o参数是an3
        Animal tmp = (Animal) o;
        return this.name.equals(tmp.name) && this.age == tmp.age;

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

    public class AxJava {
        public static void main(String[] args) throws CloneNotSupportedException {
            System.out.println("韦坤四战四级");
            Animal animal = new Animal("舍长", 21);
            //克隆上面这个对象,要强制转换类型
            Animal animal2 = (Animal) animal.clone();//但是找不到这方法，要类对接克隆接口关系
            System.out.println(animal);
            System.out.println(animal2);

            //测试一下改变一个值,这样写只会改变当前的，克隆的没有变
            animal.age = 10;
            animal.name = "ax";
            System.out.println("-----------------------");
            System.out.println(animal);
            System.out.println(animal2);

            //测一下浅拷贝
//        System.out.println("-----------------------");
//        System.out.println(animal.ba.a);
//        System.out.println(animal2.ba.a);
//        System.out.println("-----------------------");
//        animal.ba.a = 20;
//        System.out.println(animal.ba.a);
//        System.out.println(animal2.ba.a);

            //测一下深拷贝
            System.out.println("-----------------------");
            animal.ba.a = 30;
            System.out.println(animal.ba.a);
            System.out.println(animal2.ba.a);

            Animal an2 = new Animal("张三",15);
            Animal an3 = new Animal("张三",15);
            System.out.println(an2.equals(an3));//进行比较是否一样，一样返回true
            //这时即使一样都返回flase ，说明equals方法已经不适用，要我们自己重写他的功能

            //hashCode定位在哪位置
            System.out.println(an2.hashCode());
            System.out.println(an3.hashCode());//因为重写了hashcode，所以输出一样


        }
    }


