import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-10
 * Time: 12:52
 */
public class Jsq extends JFrame {
    //事件要用到全局变量
    private JFrame jsqFrame ;////窗口
    //顶层面板
    private JPanel dingcJPanel;
    private JButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b0;//定义数字
    private JButton jia,jian,cheng,chu,dian,dengyuButton;//计算机符合
    private JTextField jieguoField;//

    //创建窗口
    public void chuangkinit() {
        jsqFrame = new JFrame();//窗口对象
        jsqFrame.setTitle("计算器");//窗口标题
        jsqFrame.setSize(300,300);//窗口大小
        jsqFrame.add(getPanel());
        jsqFrame.setLocation(300,300);//弹出位置
        jsqFrame.setVisible(true);//窗口可见
        jsqFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);//设置关闭结束
        //jsqFrame.setDefaultCloseOperation(3);//设置关闭结束
    }
    //计算面板
    private JPanel getPanel() {
        dingcJPanel = new JPanel();
        //在顶部设置边框布局
        dingcJPanel.setLayout(new BorderLayout());

        //布局结果设置文本框
        //怎么设置这个文本框大一点呢
        jieguoField = new JTextField(50);
        dingcJPanel.add(jieguoField,BorderLayout.NORTH); //放置在顶部


		/*JPanel neijJPanel = new JPanel();
		neijJPanel.setLayout(new GridLayout(4,4,5,5));
		//放第一行
		b7 = new JButton("7");
		b8 = new JButton("8");
		b9 = new JButton("9");
		jia = new JButton("+");

		neijJPanel.add(b7);
		neijJPanel.add(b8);
		neijJPanel.add(b9);
		neijJPanel.add(jia);



		//放第二行
		b4 = new JButton("4");
		b5 = new JButton("5");
		b6 = new JButton("6");
		jian = new JButton("-");

		neijJPanel.add(b4);
		neijJPanel.add(b5);
		neijJPanel.add(b6);
		neijJPanel.add(jian);

		//第三行

		b1 = new JButton("1");
		b2 = new JButton("2");
		b3 = new JButton("3");
		cheng = new JButton("*");

		neijJPanel.add(b1);
		neijJPanel.add(b2);
		neijJPanel.add(b3);
		neijJPanel.add(cheng);


		//第四行
		b0 = new JButton("0");
		dian = new JButton(".");
		dengyuButton = new JButton("=");
		chu = new JButton("/");

		neijJPanel.add(b0);
		neijJPanel.add(dian);
		neijJPanel.add(dengyuButton);
		neijJPanel.add(chu);
		*/


        dingcJPanel.add(getanniu(),BorderLayout.CENTER);//这是啥


        return dingcJPanel;
    }


    public JPanel getanniu() {
        String[] strings = {"7","8","9","+","4","5","6","-","1","2","3","*","0",".","=","/"};

        JPanel anniuJPanel = new JPanel();
        anniuJPanel.setLayout(new GridLayout(4,4,6,6));
        for (int i = 0; i < strings.length; i++) {

            //模仿
			/*
			 * b0 = new JButton("0");
			neijJPanel.add(b0);
			 */
            JButton temButton = new JButton(strings[i]);//放的是字符串
            anniuJPanel.add(temButton);


        }
        return anniuJPanel;

    }


}
