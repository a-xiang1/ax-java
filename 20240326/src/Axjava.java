/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-26
 * Time: 17:35
 */
public class Axjava {
    String s;
    public static  int add(int[] array){
        int ret = 0;
        for (int x: array){
            ret += x;
        }
        return ret;
    }
    public static void main(String[] args) {
        Axjava ax = new Axjava();
        System.out.println(ax.s);
        //匿名对象
        System.out.println(new Axjava().s);//缺点只能使用一次
        System.out.println(add(new int[]{1,2,3,4,5}));


    }
}
