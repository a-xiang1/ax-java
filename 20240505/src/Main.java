import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-05
 * Time: 16:34
 */
public class Main {
    public static void main(String[] args) {
        //栈的使用
        Stack<Integer> stack = new Stack<>();

        //入栈
        stack.push(1);
        stack.push(5);
        stack.push(88);
        stack.push(6);

        //取出栈顶元素
        Integer ret = stack.pop();
        System.out.println(ret);

        //不取出栈顶元素只是查看一下
        System.out.println(stack.peek());
    }
}
