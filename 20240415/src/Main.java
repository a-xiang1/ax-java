import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-15
 * Time: 9:39
 */
public class Main {

    public static void men(int[] A, int m, int[] B, int n){
        int i = m-1;
        int k = m +n -1;
        int j = n-1;
        while(j >= 0 && i >= 0){
            if(A[i] < B[j])//拿出两个数组末尾的进行比较，谁大就放到A数组的末尾位置
            {
//                A[k] = B[j];
//                j--;
//                k--;
                //合成一句
                A[k--] = B[j--];
            }else{
                A[k--] = A[i--];
            }
        }

        //跳出上面的循环，说明j或者i有一方已经全部转移完成
        while(j >= 0){
            A[k--] = B[j--];
        }
        while(i >= 0){
            A[k--] = A[i--];
        }

        for(int x: A){
            System.out.print(" ");
            System.out.print(x);
        }
    }
    public static void func(){
        try(Scanner scanner = new Scanner(System.in)){
            int[] arr =null;//空指针异常
            System.out.println(arr.length);

            System.out.println(10/0);//算术异常

        }catch(NullPointerException e){// 捕获时候捕获的是空指针异常--真正的异常无法被捕获到
            System.out.println("空指针异常");

        }
        //try中可能会抛出多个不同的异常对象，则必须用多个catch来捕获----即多种异常，多次捕获
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("这是个数组下标越界异常");
            e.printStackTrace();//这个是把异常信息打印出来
        }
        finally{
            System.out.println("一定会执行finally");//不管上面如何运行，这个都会执行
        }
    }

    public static void main(String[] args) {
        System.out.println("ss");
        //两个升序数组A,B，然后要把B数组合并到A数组中，要求是升序的
        int[] A = new int[6];
        A[0] = 2;
        A[1] = 3;
        A[2] = 6;

        int[] B = new int[3];
        B[0] = 2;
        B[1] = 14;
        B[2] = 19;
        int m= A.length;
        int n = B.length;

        men(A,3,B,3);
        func();
    }
}
