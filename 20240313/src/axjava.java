/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-13
 * Time: 9:19
 */
public class axjava {

    //通过递归输入123，然后依次输出1 2 3
    //要理解起点条件跟终止条件
    public static void fun(int n){
        if(n < 10){//要输出第一位，那就是个位
            System.out.println(n);
            return ;//返回fun函数
        }
        //如果不是个位，那么依次进行除法，消去一位。
        fun(n / 10);
        System.out.println(n % 10);//这里输出要进行对10求余，才能得到
    }

    //使用递归求1+...+10和
    public static int maxh(int n){
        //其实你要找到起点跟终点才行，比如吧1+...+10，那么就是逆序相加，最后一个加的是1，就把+1作为结束的条件
        if(n == 1){

            System.out.println(n);
            return 1;

        }
        System.out.println(n);
        return n + maxh(n - 1);
    }
   /* public static int maxh(int n){
        //其实你要找到起点跟终点才行，比如吧1+...+10，那么就是逆序相加，最后一个加的是1，就把+1作为结束的条件
        if(n == 10){

            System.out.println(n);
            return 10;

        }
        System.out.println(n);
        return n + maxh(n + 1);
    }*/

    //实现输入一个非负整数，返回他的数字之和，例如输入1729，返回1+7+2+9

    public static int fun2(int n){
        if(n < 10){
            return n;
        }
        return  n % 10 + fun2(n /10);
    }

    //使用递归写斐波那契
    //如果数据很大，效率很慢。
    public static int recursion(int a ){

        if(a == 1 || a == 2){
            return 1;
        }
        return recursion(a-1) + recursion(a-2);//第三项开始，等于前面两项之和
    }

    //使用非递归求斐波那契
    //循环迭代处理。
    public static int fun3(int n){
        if(n == 1 || n == 2){
            return 1;
        }
         int f1 = 1;
         int f2 = 1;
         int f3 = 0;
         for(int i  = 3 ; i <= n ; i++){
             f3 = f1 + f2; //交换值
             f1 = f2;
             f2 = f3;
         }
         return f3;
    }
    //实现汉诺塔
    public static void move(char pos1,char pos2){
        System.out.print(" "+ pos1 + "->" + pos2);

    }
    public static void hanio(int n ,char po1,char po2 ,char po3){
        if(n == 0){
            return ;
        }
        else if(n == 1){
            move(po1,po3);
        }
        else{
            //首先，从a柱上借助c挪到b上
            //然后，在b上借助a挪到c
            hanio(n-1,po1,po3,po2);
            move(po1,po3);
            hanio(n-1,po2,po1,po3);

        }

    }


    public static void main(String[] args) {
        //递归输出
        // fun(123);
        //System.out.println(maxh(10));
        //System.out.println(maxh(1));
        /*int x = fun2(1729);
        System.out.println(x);*/
        //recursion//递归意思
        //recursion(10);
        //System.out.println(recursion(10));
        System.out.println(fun3(20));
        System.out.println();
        hanio(4,'A','B','C');
        System.out.println();
       /* hanio(4,'A','B','C');
        System.out.println();
        hanio(4,'A','B','C');
        System.out.println();
*/




    }
}
