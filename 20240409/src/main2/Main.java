package main2;

import java.util.Arrays;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-09
 * Time: 10:27
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");

        //区分""和null
        String str = "";//指向的对象为空
        String str2 = null;//没有指向任何对象
        System.out.println(str.length());
        //System.out.println(str2.length());//空指针异常


        String s1 = "abd";
        String s2 = "Abc";
        //比较两个字符串是否相等
        //使用equals
        System.out.println(s1.equals(s2));

        //比较两个字符串谁大谁小
        //使用comareto
        /*s1>s2 返回正数
        s1=s2 返回0
        s1<s2 返回负数
        * */
        System.out.println(s1.compareTo(s2));
        //忽略大小写比较
        System.out.println(s1.compareToIgnoreCase(s2));

        //查找字符串位置，没有找到返回-1，找到返回其下标
        String str3 = "adbcadef";
        System.out.println(str3.indexOf('c'));//按字符查找下标
        System.out.println(str3.indexOf("cad"));//按字符串查找下标
        System.out.println(str3.indexOf('a',2));//从下标为2位置开始找字符
        System.out.println(str3.indexOf("ad",2));//从下标为2位置开始找字符串
        //倒序查找
        //虽然是从后开始找，但是下标还是从前开始数
        System.out.println(str3.lastIndexOf('d'));
        //倒序查找字符串，如果有两个一样的只会找到第一个
        System.out.println(str3.lastIndexOf("ad"));
        //这样写的话就是从第三个下标位置往前找ad，所以优先找到第一个ad位置为0
        System.out.println(str3.lastIndexOf("ad",3));
        //这样才得到后面那个ad位置
        System.out.println(str3.lastIndexOf("ad",5));

        //数字转换成字符串
        String s = String.valueOf(1.554566);//数字转成字符串
        System.out.println(s+"就解决");
        String s4 = String.valueOf(true);
        System.out.println(s4);

        //字符串转成数字
        int data = Integer.parseInt("1985");
        System.out.println(data);
        System.out.println(Integer.parseInt("1985"));

        //小写转换成大写
        String s5 = "hello monery";
        String s6 = s5.toUpperCase();
        //转变成大写并不是在原本的基础上进行转变，转变成大写后是一个新的对象，所以原本的没有变
        System.out.println(s5);
        System.out.println(s6);
        System.out.println(s5.toUpperCase());
        //大写转成小写
        System.out.println(s6.toLowerCase());

        //字符串转成字符数组
        String s7 = "hi hi";
        char[] arr =s7.toCharArray();
        System.out.println(Arrays.toString(arr));
        //字符数组转成字符串
        char[] arr2 = {'a','b','5'};
        String s8 = new String(arr2);
        System.out.println(s8);

        //格式化
        String s9 = String.format("%d-%d-%d",2019,9,4);//梦回c语言的printf
        System.out.println(s9);

        //字符串的替换
        String str4 = "ababcdcdooosabhasdasfdjgsdj";
        //将给定要替换的字符串部分，全部替换成给定的字符串
        System.out.println(str4.replace("ab","哈哈"));
        System.out.println(str4.replace('a','6'));
        //这个只替换第一个字符串
        System.out.println(str4.replaceFirst("ab","55"));
        //这个跟replace好像一样，也是全部替换，replaceAll优先”基于规则表达式的替换
        System.out.println(str4.replaceAll("ab","哈哈"));

        //字符串的拆分
        String str5 = "ab cd eg";
        String[] ret = str5.split(" ",3);
        //这里另一个分组参数可以不填，不填他就自动识别可以分为几组
        //String[] ret = str5.split(" ");
        for (int i = 0; i < ret.length;i ++){
            System.out.println(ret[i]);
        }
        //特殊情况。如果遇到分隔符为'.'*'|'+'这类情况，都需要在前面加上转义字符
        String str6 = "19.5.62";
        String[] ret2 = str6.split("\\.");
        for (int i = 0; i < ret2.length;i ++){
            System.out.println(ret2[i]);
        }
        //如果是"\\"那么要写成"\\\\"
        String str7 = "192\\5\\62";
        String[] ret3 = str7.split("\\\\");
        for (int i = 0; i < ret3.length;i ++){
            System.out.println(ret3[i]);
        }

        //如果要有多个分割符用"|"来连接
        String str8 = "192=56&2";
        String[] ret4 = str8.split("=|&");
        for (int i = 0; i < ret4.length;i ++){
            System.out.println(ret4[i]);
        }

        //字符截取
        String str9 = "abcgs";
        System.out.println(str9.substring(2));//从第二个下标位置往后截取
        System.out.println(str9.substring(0,3));//[0,3)，右边是开区间无法取到

        //去掉字符串的左右两边空格 中间的空格无法去掉
        String str10 = "  ab cgs  ";
        System.out.println(str10.trim());
















    }
}
