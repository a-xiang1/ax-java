package com.ax.ui;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-13
 * Time: 17:59
 */


import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.*;
import java.util.Random;




/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-08
 * Time: 16:42
 */

//1.创建游戏的主界面
    /*
    逻辑
    JavaBean类描述界面的
    属性（宽、高） 以及行为、成员
    上下移动的代码逻辑
    统计步数的代码逻辑
    一键通关
    查看最终效果
    恶搞好友
     */
    /*
    疑点
    1.怎么打乱图片的顺序
    用到随机生成数嘛，然后循环，但要数字唯一挺不容易的
    不用唯一，打乱就行
     解法：把一个一维数组中数组 0- 9打乱顺序
      然后按照3个一组方式添加到二维数组中
      2.可以实现用鼠标来玩
     */

//接口KeyListener给界面添加事件,键盘监听“上下左右”
//接口ActionListener给界面添加鼠标监听
public class GameJFrame extends JFrame implements KeyListener, ActionListener {


    int[][] arrs = new int[3][3];//作为全局更好的使用，打乱需要，然后初始化数据也要，后面判断是否拼图成功也要
    //记录0即空白图片的索引
    int xx = 0;
    int yy = 0;

    //胜利数组
    int[][] win = new int[][]{
            {1,2,3},
            {4,5,6},
            {7,8,0}
    };

    //优化路径，变换容易改，为后面的更换图片使用
    String path = "image\\animal1\\";

    //记录步数
    int count = 0;

    //作为成员，方便鼠标事件
    //创建选项下面的条目对象
    JMenuItem replayItem = new JMenuItem("重新游戏");
    JMenuItem reLoginItem = new JMenuItem("重新登录");
    JMenuItem clooseItem = new JMenuItem("关闭游戏");

    JMenuItem accountItem = new JMenuItem("作者微信");
    JMenuItem accountItem02 = new JMenuItem("打伤作者");

    //添加更换图片
    JMenuItem  gh = new JMenuItem("美女");
    JMenuItem  gh2 = new JMenuItem("动物");
    JMenuItem  gh3 = new JMenuItem("运动");

    //



    //用构造方法每次进行创建窗口
    public GameJFrame() {
        super("拼图小游戏");
        //窗口初始化
        inistJMenuBar();
        //菜单初始化
        initJMenuBar();

        //初始化数据（打乱顺序）
        initData();

        initImage();

        //放到最后
        this.setVisible(true);//让其显示窗口
    }

    private void initData() {
        //打乱顺序
        int[] arr = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8};//0虽然没有，但是他会是空的，拼图需要空的位置
        //2.打乱数组中的数据顺序
        //遍历数组，得到每一个元素，拿着每一个元素跟随机索引上的数组进行交换
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            //获得随机索引
            int index = random.nextInt(arr.length);//限制随机数的范围,虽然会重复但是没啥影响，打乱就行
            //随机交换
            int temp = arr[i];
            arr[i] = arr[index];
            arr[index] = temp;

        }

        System.out.println();

        //3.分组放到二维数组里面

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0){//如果遇到0 记录一下他在二维数组里面的索引
                xx = i / 3;
                yy = i % 3;

            }//不用else，防止漏了0
            arrs[i / 3][i % 3] = arr[i];//每组3个
        }



    }

    private void initImage() {

        //清空原本已经出现的图片
        this.getContentPane().removeAll();

        //判断是否胜利
        if(victory()){
            //胜利把空白坐标置为2,2
            xx = 2;
            yy = 2;
            //显示胜利图标
            JLabel win = new JLabel(new ImageIcon(path+"win.png"));
            win.setBounds(720,150,264,122);
            //放到界面
            this.getContentPane().add(win);
            showJDialog("恭喜大佬成功通关");
        }

        //可以写一个计时器

        //显示步数
        //？？？？怎么设置字体大小呢，字好小
        JLabel countshop = new JLabel("步数: " + count);
        //设置大小位置
        countshop.setBounds(70,350,200,100);
        this.getContentPane().add(countshop);

        //说明一下快捷键
        JLabel kd3 = new JLabel(" 快捷玩法  " );
        //设置大小位置
        kd3.setBounds(10,40,200,100);
        this.getContentPane().add(kd3);

        //说明一下快捷键
        JLabel kd = new JLabel(" 按住a不松可查看完整图片  " );
        //设置大小位置
        kd.setBounds(10,60,200,100);
        this.getContentPane().add(kd);

        JLabel kd2 = new JLabel(" 按w可一键通关 ");
        //设置大小位置
        kd2.setBounds(10,80,200,100);
        this.getContentPane().add(kd2);





        //创建ImageIcon的对象
        //ImageIcon icon = new ImageIcon("C:\\java\\javam\\ax-java\\20240508\\image\\animal1\\" + "01"+".png");

        //创建一个JLabel的对象（管理容器）
        //JLabel jlabel1 = new JLabel(icon);

        //给添加的图片进行位置管理，第一张放在左上角,设置了还是不行，因为布局容器默认居中
        //jlabel1.setBounds(0,0,320,195);

       /* int x = 135;
        int y = 135;*/

        int x = 300;
        int y = 190;

        /*
       路径分为2种
       绝对路径：一定是从盘符开始的 c:
       相对路径：不是从盘符开始
       相对路径相对当前项目而言
       在当前项目路径基础上找该文件
         */

        JLabel jlabel = null;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {

                // ImageIcon icon = new ImageIcon("C:\\java\\javam\\ax-java\\20240508\\image\\animal1\\" + "0" + arrs[i][j] + ".png");
                //相对路径，取项目前路径即可
                int tem = arrs[i][j];
                //ImageIcon icon = new ImageIcon("image\\animal1\\" + "0" + tem  + ".png");//
                ImageIcon icon = new ImageIcon(path + "0" + tem  + ".png");
                jlabel = new JLabel(icon);
                //调整位移
                //jlabel.setBounds(x * j , y * i, x , y);
                jlabel.setBounds(x * j+ 400 , y * i+ 140, x , y);
                //给图片增加边框
                /*
                0表示图片凸显
                1表示图片凹进去
                 */
                jlabel.setBorder(new BevelBorder(0));
                //jlabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
                //jlabel.setBorder(new BevelBorder(1));
                //把管理容器添加到界面中
                this.getContentPane().add(jlabel);


            }


        }

        //将管理容器放到界面中去
        //this.getContentPane().add(jlabel);
        //this.add(jlabel);

        //添加背景图片放在后面
        //先加载的图片在上方
        //创建背景添加
        JLabel back = new JLabel(new ImageIcon(path+"bj03.png"));
        //放位置
        back.setBounds(215,10,1303,790); //宽度和高度按照图片大小来写*/
        this.getContentPane().add(back);



        //刷新图片加载
        this.getContentPane().repaint();
    }


    private void initJMenuBar() {
        //创建菜单
        JMenuBar jmenuBar = new JMenuBar();

        //创建菜单上面的两个选项的对象（功能 关于作者）
        JMenu functionJMenu = new JMenu("功能");
        JMenu aboutJMenu = new JMenu("关于作者");
        JMenu  print  = new JMenu("更换图片");



        //JMenuItem m = new JMenuItem("关闭");

        //给菜单条目对象都绑定点击事件
        replayItem.addActionListener(this);//this就是执行该接口的方法
        reLoginItem.addActionListener(this);
        clooseItem.addActionListener(this);
        accountItem.addActionListener(this);
        accountItem02.addActionListener(this);
        //给更换图片的增添事件
        gh.addActionListener(this);
        gh2.addActionListener(this);
        gh3.addActionListener(this);


        //放到功能下面菜单栏
        functionJMenu.add(replayItem);
        functionJMenu.add(reLoginItem);
        functionJMenu.add(clooseItem);

        //将更换图片的下一节放到更换里面
        print.add(gh);
        print.add(gh2);
        print.add(gh3);

        //放更换图片
        functionJMenu.add(print);





        //放到关于作者下面菜单栏
        aboutJMenu.add(accountItem);
        aboutJMenu.add(accountItem02);



        //replayItem.add(m);

        //将菜单里面的两个选项放到菜单中
        jmenuBar.add(functionJMenu);
        jmenuBar.add(aboutJMenu);

        //放完之后将界面设置
        this.setJMenuBar(jmenuBar);
    }

    public void inistJMenuBar() {
        this.setSize(1650, 980);//设置窗口的大小
        //设置窗口置顶
        this.setAlwaysOnTop(true);
        //设置界面居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        //两个写法都行
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        // this.setDefaultCloseOperation(3);

        //取消默认的居中位置，只有取消了才会安装xy皱的形式添加组件
        this.setLayout(null);

        //给整个界面添加键盘监听事件
        this.addKeyListener(this);

    }

    //键盘监听
    @Override
    public void keyTyped(KeyEvent e) {

    }


    @Override
    public void keyPressed(KeyEvent e) {
        //如果已经胜利不能再进行操作
        if (victory()){
            return ;
        }

        //按住a不松显示完整图片
        int code = e.getKeyCode();
        if (code == 65){
            //先清空前面的图片
            this.getContentPane().removeAll();
            //加载图片进来
            JLabel all = new JLabel(new ImageIcon(path+"alls.png"));
            //设置图片大小位置
            all.setBounds(550,200,635,401);
            //将图片加载到界面中
            this.getContentPane().add(all);

            //加载背景图片
            JLabel back = new JLabel(new ImageIcon(path+"bj03.png"));
            //放位置
            back.setBounds(215,10,1303,790); //宽度和高度按照图片大小来写*/
            this.getContentPane().add(back);


            //刷新图片加载
            this.getContentPane().repaint();


        }

    }


    //键盘松开
    @Override
    public void keyReleased(KeyEvent e) {
        //如果已经胜利不能再进行操作
        if (victory()){
            return ;
        }



        //对上下左右按键进行监听
        int code = e.getKeyCode();
        if (code == 37){

            if (yy == 2){//最左边时候不能移动了
                return;
            }
            System.out.println("左");
            arrs[xx][yy] = arrs[xx ][yy+ 1];
            arrs[xx ][yy +1] = 0;//然后置0，让空白方块值始终为0
            yy ++;//空白的位置发生改变
            //每次移动步数都加1
            count++;
            //调用方法按照最新的数组加载图片
            initImage();

        }else if (code == 38){

            if (xx == 2){
                return;
            }
            System.out.println("上");
            /*
            把空白方块下面的数字往上移动
            x，y表示空白方块
            x + 1 ，y不变 表示空白下面的数字索引
             */
            //把空白方块下方的数字赋值给空白方块
            arrs[xx][yy] = arrs[xx + 1][yy];
            arrs[xx + 1][yy] = 0;//然后置0，让空白方块值始终为0
            xx ++;//空白的位置发生改变
            //每次移动步数都加1
            count++;
            //调用方法按照最新的数组加载图片
            initImage();

            //但这样写当空白在低层时候，下方没有图片了，还按上移动会异常。

        }else if (code == 39){

            if (yy == 0){
                return;
            }
            System.out.println("右");
            arrs[xx][yy] = arrs[xx ][yy- 1];
            arrs[xx ][yy - 1] = 0;//然后置0，让空白方块值始终为0
            yy --;//空白的位置发生改变
            //每次移动步数都加1
            count++;
            //调用方法按照最新的数组加载图片
            initImage();

        }else if (code == 40){

            if (xx == 0){
                return;
            }
            System.out.println("下");
            //把空白方块下方的数字赋值给空白方块
            arrs[xx][yy] = arrs[xx - 1][yy];
            arrs[xx - 1][yy] = 0;//然后置0，让空白方块值始终为0
            xx --;//空白的位置发生改变
            //每次移动步数都加1
            count++;
            //调用方法按照最新的数组加载图片
            initImage();
        }
        //松开后变回去
        else if (code == 65){
            initImage();
        }
        //作弊器实现
        else if (code == 87){
            //直接让其顺序变回来
            arrs = new int[][]{
                    {1,2,3},
                    {4,5,6},
                    {7,8,0}
            };
            //再进行拼接
            initImage();


        }


    }


    //判断数组数据是否跟win数组中相同
    //如果全部相同返回true，否则返回false
    public boolean victory(){
        //依次对比
        for (int i = 0; i < arrs.length; i++) {
            for (int i1 = 0; i1 < arrs[i].length; i1++) {
                if (arrs[i] [i1] != win[i][i1]){
                    //只要有一个不同就返回
                    return false;
                }
            }

        }
        //循环结束说明比较完成
        return true;
    }




    //鼠标点击或者空格触发
    @Override
    public void actionPerformed(ActionEvent e) {
        //获取当前被点击的条目对象
        Object object  = e.getSource();

        if (object == replayItem){
            //重新游戏
            System.out.println("重新游戏");
            //2.打乱
            initData();
            //计数器清0
            count = 0;
            //3.加载图片
            initImage();

        }else if (object == reLoginItem){
            System.out.println("重新登录");
            //关闭游戏界面
            //打开登陆界面
            this.setVisible(false);
            new LonginJFrame();//登录界面


        }else if (object == clooseItem){
            System.out.println("关闭游戏");
            System.exit(0);
        }else if (object == accountItem){
            System.out.println("作者微信");
            /*JFrame jFrame = new JFrame("作者微信");
            //设置界面的宽高
            jFrame.setSize(635,866);
            jFrame.setAlwaysOnTop(true);//设置窗口界面置顶
            jFrame.setLocationRelativeTo(null);//设置界面剧中

            //放入微信图片
            JLabel wx = new JLabel(new ImageIcon(path+"微信.jpg"));
            //设置大小
            wx.setBounds(0,0,634,866);
            //放到界面中
            jFrame.add(wx);
            jFrame.setLayout(null);// 取消默认放置按钮啥的
            jFrame.setVisible(true);*/


            //创建弹框对象
            JDialog jDialog = new JDialog();
            JLabel jLabel = new JLabel(new ImageIcon(path+"微信.jpg"));
            jLabel.setBounds(0,0,634,866);

            //图片添加到弹框中
            jDialog.getContentPane().add(jLabel);
            jDialog.setSize(634,866);//弹框大小
            //弹框置顶
            jDialog.setAlwaysOnTop(true);
            //居中
            jDialog.setLocationRelativeTo(null);
            //弹框不关闭无法操作
            jDialog.setModal(true);
            //弹框显示出来哦
            jDialog.setVisible(true);


        }else if (object == accountItem02){
            //作者收款码
            JFrame jFrame = new JFrame("打伤作者");
            //设置界面的宽高
            jFrame.setSize(745,1014);
            jFrame.setAlwaysOnTop(true);//设置窗口界面置顶
            jFrame.setLocationRelativeTo(null);//设置界面剧中

            //放入微信图片
            JLabel skm = new JLabel(new ImageIcon(path+"收款码.jpg"));
            //设置大小
            skm.setBounds(0,0,745,1014);
            //放到界面中
            jFrame.add(skm);
            jFrame.setLayout(null);// 取消默认放置按钮啥的
            jFrame.setVisible(true);
        }//给更换图片增加鼠标点击
        else if (object == gh){
            System.out.println("美女");
            //=更新步数
            count = 0;

            //设置个随机随机抽取图片
            Random random = new Random();
            int ran = random.nextInt(2);//[)
            path = "image\\美女\\"+ran + "\\";//美女文件夹
            initData();
            initImage();



            //空白方块出了问题,原因就是没有02图片，把02当做空白图片了，搞乱了


        } else if (object == gh2){
            System.out.println("动物");
            //=更新步数
            count = 0;
            //设置个随机随机抽取图片
            Random random = new Random();
            int ran = random.nextInt(2);//[)
            path = "image\\动物\\"+ran + "\\";//美女文件夹
            initData();
            initImage();


        }else if (object == gh3){
            System.out.println("运动");
            //=更新步数
            count = 0;

            //设置个随机随机抽取图片
            Random random = new Random();
            int ran = random.nextInt(2);//[)
            path = "image\\运动\\"+ran + "\\";//美女文件夹
            initData();
            initImage();

        }

        //重新开始
        //绑定点击事件，重新打乱二维数组数字，加载乱的图片，计时器清0


    }

    //胜利给个提示
    public void showJDialog(String content){
        //创建弹框对象
        JDialog jDialog = new JDialog();
        //弹框大小
        jDialog.setSize(200,150);
        //弹框置顶
        jDialog.setAlwaysOnTop(true);
        //弹框居中
        jDialog.setLocationRelativeTo(null);
        //弹框不关无法操作
        jDialog.setModal(true);


        //this.setLayout(new FlowLayout());//设置窗口布局
        //创建Jlabel对象管理文字添加到弹框中去
        JLabel warning = new JLabel(content);
        warning.setBounds(20,10,200,150);
        jDialog.getContentPane().add(warning);

        //弹框显示
        jDialog.setVisible(true);

    }


}
