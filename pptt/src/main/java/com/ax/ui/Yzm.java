package com.ax.ui; /**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-15
 * Time: 17:23
 */


import java.awt.Color; // 引入 Color 类，提供系统颜色和关于颜色空间的类。
import java.awt.Font; // 引入 Font 类，支持在AWT的所有渲染环境下的通用文字布局，并为所有给定字符和脚本提供字体处理功能。
import java.awt.Graphics; // 引入 Graphics 类，允许应用程序以协同的方式绘制到屏幕上或者其他任何地方。
import java.awt.image.BufferedImage; // 引入 BufferedImage 类，描述一些整数像素的具有可访问的数据缓冲区的图像。
import java.io.File; // 引入 File 类，它提供了与文件和路径名相关的操作，它具有创建、删除、读取文件等功能。
import java.io.IOException; // 引入 IOException 类，处理输入、输出操作失败或中断的情况（类似于文件未找到等）。
import java.util.Random; // 引入 Random 类，用于生成伪随机数。
import javax.imageio.ImageIO; // 引入 ImageIO 类，用于读取和写入图像格式，如 JPEG、PNG等。

public class Yzm {

    String path = "image\\animal1\\";
    static String[] strs={"a", "b", "c", "d", "e", "f", "g", "h",
            "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z","0", "1", "2", "3", "4", "5", "6",
            "7", "8", "9","A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
            "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static String yzm = ""; //这个要变成静态，不然无法在其他类里面进行调用或者传值

    public  void Main()  {
        int height=60;//高
        int width=100;//宽
        int imageType = BufferedImage.TYPE_INT_RGB;//画板的组成，rgb是red，green，blue，红绿蓝组成
        BufferedImage image = new BufferedImage(width,height,imageType);//实例化画板对象，三个参数

        Graphics g = image.getGraphics();//实例化了一支笔
        g.setColor(Color.white);//笔的颜色为白色
        g.fillRect(0, 0, width, height);//然后将画板填充为白色，x，y为矩形左上角的坐标
        /*设置画板 */

        Random r=new Random();//实例化一个随机的对象
        int x=0,y=40;//这个是x，y界限
        g.setColor(Color.blue);//让验证码的颜色为蓝色
        g.setFont(new Font("宋体",Font.PLAIN,40));//设置字体，字体为宋体，并且大小为100
        for(int i=0;i<4;i++){//用for循环生成4个随机验证码
            int index=r.nextInt(62);//strs数组总共62个数，所以下标界限是0-61
            //System.out.println(strs[index]);
            //记录验证码，方便进行比较
            yzm += strs[index];//进行拼接
            System.out.println(yzm);

            String str = strs[index];//随机取一个字符
            g.drawString(str,x , y);//画字符在画板上
            x+=25;//这个移位，不然我第一次在这个位置画A，第二次画B的时候还在这个位置不就覆盖掉了吗，所以要移动位置


        }
        /*画出四位数的验证码 */

        /*g.setColor(Color.green);//让画笔的颜色为绿色
        for(int i=0;i<10;i++){//生成十条干扰线
            int a=r.nextInt(10);
            int b=r.nextInt(400);
            int c=r.nextInt(10)+380;
            int d=r.nextInt(400);
            g.drawLine(a,b,c,d);//ab是干扰线的左边的横纵坐标，cd是右边的
        }*/

        // 尝试写入图像
        try {

            ImageIO.write(image, "jpg", new File(path+"666.png")); //此处应给出具体的文件名
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("当前正确验证码 " + yzm);
    }
}