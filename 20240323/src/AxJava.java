/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-24
 * Time: 11:24
 */
//创建类
  class Student{
        public String name;
        public int age;

    //初始化用构造方法
    //构造方法名字要跟类名一模一样
    //构造方法只调用一次
    //这构造方法好像要放在上面，放在下面报错
    public Student(){
        this("ax",20);//直接调用，但只能在构造方法里面使用
        System.out.println("123456");
    }
    //还能进行参数的传
    //构造方法一开始没写会默认有一个空的构造方法，如果你写了构造方法，那么默认的没有了
    /*public Student(String name ,int age){

        System.out.println("ax");
        this.name = name;
        this.age= age;
        System.out.println(" Student(String,int)");
    }*/
//快捷创建构造方法，alt+inser
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }



    public void print(){//this就是谁调用这个函数，那么this就代表谁
            System.out.println("姓名 = "+this.name);
        }
    public void show(){
        System.out.println("年龄 = "+this.age);





    }
}



public class AxJava {
    public static void main(String[] args) {
        //System.out.println("考完二级回归了");
        /*Student student = new Student();//类的实例化
        student.age = 18;
        student.name = "韦坤四战";
        student.print();
        student.show();*/

        //Student student2 = new Student("ax",19);//在这里调用构造方法的
        Student student = new Student();




    }
}
