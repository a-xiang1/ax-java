package axjava;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-24
 * Time: 16:34
 */
public class Private {
    /*public String name; //public公有的，外部可以调用
    public int age;*/

    private String name; //private私有的，只能内部调用，但可以公开方法进行调用
    private int age;


    //先创建构造方法初始化
    //创建两个，一个是没参数另一个有
    public Private(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println(name);
        System.out.println(age);
    }

    public Private() {
        this("丫丫",20);

    }
}
