import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 11:04
 */
//序列化对象
//参与序列化和反序列化的对象必须实现Serializable接口
//Serializable接口是标志性接口（就是空的啥也没有）
//标志性接口是给java虚拟机参考的，java虚拟机看到会自动生成一个序列化版本号。
//序列号，区分类，每个类都有自己的序列号
//缺点就是一旦序列化之后就不能修改
 //因此一般序列化对象时，我们要手动给一个固定序列化

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("我爱打篮球");

        //创建java对象
        Student student = new Student(12,"kunk");

        //进行序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\java\\javam\\ax-java\\20240504\\kunk"));

        //序列化对象
        oos.writeObject(student);

        //反序列
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\java\\javam\\ax-java\\20240504\\kunk"));

        Object obj = ois.readObject();
        //反序列回来是一个学生对象
        System.out.println(obj);


        oos.flush();
        oos.close();
        ois.close();
    }
}
