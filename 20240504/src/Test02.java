import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 13:01
 */
public class Test02 {
    public static void main(String[] args) throws IOException {
        FileReader reader = new FileReader("add");

        //新建map集合
        Properties pro = new Properties();

        //调用集合对象将文件数组加载到集合中
        pro.load(reader);

        //通过key获取value
        String use = pro.getProperty("ad");
        System.out.println(use);
    }
}
