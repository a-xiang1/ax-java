import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 12:01
 */
public class Test01 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //实现序列化多个对象
        List<Student> list = new ArrayList<>();

        list.add(new Student(2,"aa"));
        list.add(new Student(5,"bb"));
        list.add(new Student(7,"cc"));

        //序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\java\\javam\\ax-java\\20240504\\kun"));

        //序列化一个集合
        oos.writeObject(list);

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\java\\javam\\ax-java\\20240504\\kun"));

        List<Student> user = (List<Student>)ois.readObject();
        for (Student us : user){
            System.out.println(us);
        }
        /*Object object = ois.readObject();
        System.out.println(object);*/

        oos.flush();
        oos.close();
        ois.close();
    }
}
