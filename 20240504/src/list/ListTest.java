package list;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 19:54
 */
public class ListTest {
    public static void main(String[] args) {
        //DoublyLiST list = new DoublyLiST()
       DoublyLiST list = new DoublyLiST();
       //因为节点的创建是静态类
        list.addFirst(2);
        list.addFirst(3);
        list.addLast(1);
        list.display();
        System.out.println(list.size());
        list.addIndex(1,20);
        list.display();
        list.addIndex(2,20);
        list.display();
        //list.remove(1);
        list.display();
        list.removeAllkey(20);
        list.display();
        list.clear();
        list.display();


    }
}
