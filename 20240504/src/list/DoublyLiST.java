package list;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 19:52
 */
//双向链表
public class DoublyLiST  implements ListInterface{


    //定义结点内容
    static  class ListNode{
        public int val;
        public ListNode next;
        public ListNode prev;//第二个指针域

        //其他两个默认为空
        public ListNode(int val) {
            this.val = val;

        }
    }

    //定义一个指针指向链表末尾
    public ListNode head;
    public  ListNode last;




    @Override
    public void addFirst(int data) {
        ListNode node = new ListNode(data);//创建节点
        if (head == null){
             head = node;
             last = node;
        }else{
            node.next = head;
            head.prev = node;
            head = node;
        }
    }

    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);//创建节点
        if (head == null){
            head = node;
            last = node;
        }else{
            last.next = node;
            node.prev = last;
            last = node;
        }
    }

    @Override
    public void addIndex(int i, int data)  {
        //任意插入
        int len = size();
        //System.out.println(len);
        ListNode node = new ListNode(data);//创建节点
       ListNode cur = head;
        //区分头部和尾部插入
        if (len == 0){
            addFirst(data);
        }if (len == 12){
            addLast(data);//尾插
        }

        //找到i位置
        while(i > 0){
            cur = cur.next;
            i--;
        }
        //已经找到i并且cur在i位置
        cur.prev.next = node;
        node.next = cur;
        node.prev = cur.prev;
        cur.prev = node;


    }

    @Override
    public boolean contains(int key) {
        ListNode cur = head;

        while(cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    @Override
    public void remove(int key) {

        //先找到要删除的位置
        ListNode cur = head;

        int len = size();
        while (cur != null){

            if (cur.val == key){
                //删除头部
                if (cur == head){//怎么知道是头cur==head时候就是了

                    head = cur.next;
                    //当只有一个头部的时候，下一个那么就是空的
                    if (head == null){
                        last = null;
                    }else {
                        head.prev = null;
                    }
                }else{
                    cur.prev.next = cur.next;
                    //删除尾部
                    if (cur.next ==null){
                        last = last.prev;
                    }
                    else {
                        //删除中间

                        cur.next.prev = cur.prev;

                    }

                }

                return;

            }
            cur = cur.next;
        }
    }

    @Override
    public void removeAllkey(int key) {
        //先找到要删除的位置
        ListNode cur = head;

        int len = size();
        while (cur != null){

            if (cur.val == key){
                //删除头部
                if (cur == head){//怎么知道是头cur==head时候就是了

                    head = cur.next;
                    //当只有一个头部的时候，下一个那么就是空的
                    if (head == null){
                        last = null;
                    }else {
                        head.prev = null;
                    }
                }else{
                    cur.prev.next = cur.next;
                    //删除尾部
                    if (cur.next ==null){
                        last = last.prev;
                    }
                    else {
                        //删除中间

                        cur.next.prev = cur.prev;

                    }

                }

            }
            cur = cur.next;
        }
    }

    @Override
    public int size() {
        ListNode cur = head;
        int count = 0;
        while(cur != null){

            count++;
            cur = cur.next;
        }
        return count;
    }

    @Override
    public void clear() {
        //清空可以暴力
       /* head = null;
        last = null;*/
        ListNode cur = head;
        while(cur != null){
            ListNode tmp = cur.next;
            cur .prev = null;
            cur.next = null;
            cur = tmp;
        }
        head = null;
        last = null;
    }

    @Override
    public void display() {
        ListNode cur = head;
        while(cur != null){
            System.out.print(cur.val+"  ");
            cur = cur.next;
        }
        System.out.println();
    }
}
