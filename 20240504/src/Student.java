import java.io.Serial;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-04
 * Time: 11:31
 */
public class Student implements Serializable {
    @Serial
    private static final long serialVersionUID = -4112372218918256818L;
    public int a;
    //如果不想让某个成员参与序列化加上关键字transient
    public transient String chars;//加上

    //固定序列号
    //private  static  final long serialversionUID = 1144650149L;

    public Student(int a, String chars) {
        this.a = a;
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Student{" +
                "a=" + a +
                ", chars='" + chars + '\'' +
                '}'+"\n";
    }
}
