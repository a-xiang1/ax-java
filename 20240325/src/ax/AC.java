package ax;

import bao.AX;
import kotlin.reflect.jvm.internal.impl.descriptors.Visibilities;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-25
 * Time: 9:55
 */
public class AC {
    public int a;
    //类变量。不属于对象。
    public static String name = "韦坤";//static静态的，不属于成员变量，是类里面共用的只有一个，用类名直接调用。
    static{//静态方法区，当静态变量被运行时候才运行，并且值运行一次
        System.out.println("sdfs");

    }
    {//构造方法 区，当类实例化时候会运行
        System.out.println("sdfsdjhfhdghdh");
    }
}
