package list;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-18
 * Time: 10:22
 */
//接口放的是大概要实现的方法
public interface IList {
    //在数组后面增加元素
    public void add(int data);

    //在pos位置新增元素
    public void add(int pos,int data);

    //判断某个元素是否在数组中
    public boolean contains(int toFind);

    //获取pos位置的元素
    public int indexOf(int pos);

    //获取pos位置的元素
    public int get(int pos);
    //给pos位置元素改为value
    public void set(int pos,int value);

    //删除第一次出现的关键字key
    public void remove(int toRemove);

    //删除pos位置元素
    public void posremove(int pos );

    //获取顺序表长度
    public int  size();

    //清空顺序表
    public void clear();

    //打印顺序表
    public void display();

    //判断数组空间是否存满
    public boolean isFull();

    //判断数组是否为空
    public boolean isEmpty();

}
