package list;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-18
 * Time: 10:21
 */
public class Test {
    public static void main(String[] args) {
        System.out.println("学习顺序表");
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(1,20);
        myArrayList.set(1,8888);
        myArrayList.display();
       // myArrayList.add(8,20);
        myArrayList.remove(1);

        myArrayList.display();
    }

}
