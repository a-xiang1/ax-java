package list;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-18
 * Time: 10:23
 */
public class MyArrayList implements IList {

    public int[] elem;//定义了数组，但没有给空间
    private static final int DEEAULT_SIZE = 2;
    public int usedSize;//记录现在顺序表里面的有效个数

    public MyArrayList() {//在构造方法这里进行扩容
       this.elem = new int[DEEAULT_SIZE];
    }

    public MyArrayList(int capacity) {//这个就可以自行给扩容大小
        this.elem = new int[capacity];
    }

    //在数组后面增加元素
    @Override
    public void add(int data) {
        //进行扩容检查
        checkCapacity();
        this.elem[usedSize] = data;//不能直接这步
        // 数据结构是一门很严谨的学科，在此之前要考虑到要判断数组是否满了
        this.usedSize++;

    }


    //在指定位置插入元素
    @Override
    public void add(int pos, int data) {
        //先判断位置是否合法
        try{
            checkPosAdd(pos);
        }catch(PosUnusual e){
            e.printStackTrace();//打印异常
            return;//return结束，不影响后面进行
        }
        //然后进行扩容检查,可能会碰到这个问题，如果上面位置不符合，然后扩容后符合了
        checkCapacity();

        //从最后位置开始向后移动
        //当i< pos就结束
        //存放元素到pos位置
        //usedsize++
        for (int i = usedSize - 1; i >= pos ; i --){
            elem[i+1] = elem[i];
        }
        elem[pos] = data;
        usedSize++;
    }

    //判断是否包含某个元素
    @Override
    public boolean contains(int toFind) {
        //先判断顺序表是否为空
        if (isEmpty()){
            return false;
        }
        for (int i = 0 ; i < usedSize ; i ++ ){
            if (toFind == elem[i]){//这里如果是引用对像那么就用eq
                return true;
            }
        }
        return false;
    }
//获取某个元素的位置
    @Override
    public int indexOf(int toFind) {
        //先判断顺序表是否为空
        if (isEmpty()){
            return -1;
        }
        for (int i = 0 ; i < usedSize ; i ++ ){
            if (toFind == elem[i]){//这里如果是引用对像那么就用eq
                return i;
            }
        }
        return -1;

    }

    @Override
    public int get(int pos) {
        checkPosGet(pos);
        if (isEmpty()){
            throw new PosUnusual("获取下标元素异常 下标位置不合法");
        }
        return  elem[pos];
    }

    //给定下标更新数据
    @Override
    public void set(int pos, int value) {
    checkPosGet(pos);
    elem[pos] = value;

    }

    //删除
    //先找到位置下标，然后后往前覆盖，size--
    @Override
    public void remove(int toRemove) {
        int index = indexOf(toRemove);//返回对应的下标
        if (index == -1 ){
            //=-1说明不存在
            System.out.println("没有这个数字无法删除 ！");
            return ;
        }
        for (int i = index ; i < usedSize - 1 ; i ++){
            elem[i] = elem[i+1];
        }
        usedSize--;

    }

    @Override
    public void posremove(int pos) {
        //先判断下标是否合法
        checkPosGet(pos);
        if (isEmpty()){//再判断是否为空
            throw new PosUnusual("顺序表为空，无法删除");
        }
        for (int i = pos ; i < usedSize - 1 ; i ++){
            elem[i] = elem[i+1];
        }
        usedSize--;

    }

    @Override
    public int size() {
        return usedSize;
    }
//如何清空呢
    @Override
    public void clear() {


    }

    @Override
    public void display() {
        for (int i = 0; i < this.usedSize; i ++){
            System.out.print(this.elem[i] + "  ");
        }
        System.out.println();
    }

    @Override
    public boolean isFull() {

        return usedSize == elem.length;
    }



    //-------------------
    //封装方法

    //封装一个进行扩容的方法
    private void checkCapacity(){
        //先判断是否满了
        if (isFull()){//返回true说明满了，所以要进行扩容
            //如何扩容，使用包方法进行复制两倍大小，然后进行拷贝到elem
            elem = Arrays.copyOf(elem,elem.length*2);
        }
    }

    //又进行对位置是否合法的判断进行封装、
    //方法名后面加上PosUnusual提醒可能会出现异常
    private void checkPosAdd(int pos) throws PosUnusual {
        //pos的范围只能是[0,usedSize]
        if(pos < 0 || pos > usedSize){
            throw  new PosUnusual("位置不合法：  " + pos);
        }

    }

    //判断顺序表是否为空进行封装
    public boolean isEmpty(){
        return usedSize ==0 ;
    }
    //封装获取下标时位置是否合法
    private void checkPosGet(int pos) throws PosUnusual {
        //pos的范围只能是[0,usedSize]
        if(pos < 0 || pos >=usedSize){
            throw  new PosUnusual("位置不合法：  " + pos);
        }
    }


}
