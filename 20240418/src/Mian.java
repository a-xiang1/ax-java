/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-18
 * Time: 9:57
 */
//写一个泛型方法
class Alg{
    public  <T extends Comparable<T> > T findMax(T[] array){
        T max = array[0];
        for (int i = 1; i < array.length; i ++){
            if (max.compareTo(array[i]) < 0){
                max = array[i];
            }
        }

        return max;
    }
}


//擦除机制，擦成Object
public class Mian {
    public static void main(String[] args) {
         Alg alg = new Alg();
         Integer[] arr = {1,5,7,2,9,8,5};
        //System.out.println(Alg.findMax(arr));静态static不用实例化对象可以直接用
        int ret = alg.<Integer>findMax(arr);//这里可以加上<Integer>说明传过去的类型
        // 如果没有就会自己根据参数推导
        System.out.println(alg.findMax(arr));
    }
}
