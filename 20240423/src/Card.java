/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-23
 * Time: 10:51
 */
public class Card {
    //扑克牌的属性，花色、数字
    private  String colour; //花色
    private int number;

    public Card(String colour, int number) {
        this.colour = colour;
        this.number = number;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "花色 ："+ colour + " " + number;
    }
}
