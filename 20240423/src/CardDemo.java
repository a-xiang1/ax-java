import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-23
 * Time: 10:57
 */
public class CardDemo {
    //
    private final String[] suits = {"♦","♣","♥","♠"};
    //买一副扑克牌
    public List<Card> buyCard() {
        List<Card> cardList = new ArrayList<>();//创建一个数组存放类型为Card
        //给扑克牌上色
        //限制牌的数字大小总共13
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                //通过card的构造方法进行初始化
                Card card = new Card(suits[i], j);
                cardList.add(card);
            }
        }

        return cardList;
    }


    //洗牌思路
    //生成随机数（1-51的随机数）
    //定义一个i从数组的最后开始往前交换数据
    public void shuffle (List<Card> cardList){
        Random random = new Random();
        for (int i =cardList.size()-1; i > 0; i --){
            int index = random.nextInt(i);
            swap(cardList,i,index);
        }
    }
    //交换
    public void swap(List<Card> cardList,int a,int b){
        Card tmp = cardList.get(a);//临时变量tmp存放a
        cardList.set(a,cardList.get(b));
        cardList.set(b,tmp);
        /*
        tmp = a;
        a = b ;
        b = tmp;
         */
    }

    //
    //人轮流取牌五次
    public void draw(List<Card> cardList){
            //那么就有三人
        List<Card> hand1 = new ArrayList<>();
        List<Card> hand2 = new ArrayList<>();
        List<Card> hand3 = new ArrayList<>();

        //他们之间建立关系，合成一个二维数组
        List<List<Card>> hands = new ArrayList<>();
        hands.add(hand1);
        hands.add(hand2);
        hands.add(hand3);

        //3人开始轮流翻
        for (int i = 0; i < 5 ; i++){
            //i代表一轮，j代表人
            for (int j = 0 ; j < 3 ; j ++){
                Card card = cardList.remove(0);//remove如果是给下标的话是删除，然后返回值
                hands.get(j).add(card);
            }
        }
        System.out.println("第一个揭牌 ： ");
        System.out.println(hand1);

        System.out.println("第二个揭牌 ： ");
        System.out.println(hand2);

        System.out.println("第三个揭牌 ： ");
        System.out.println(hand3);
        System.out.println("剩余的牌： ");
        CardTest.dayin(cardList);

    }


}

