package LinKeds;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-23
 * Time: 15:55
 */
public class Linkedlist implements LinKedface {

    //定义节点内部类
    static class ListNode{
        public  int val;
        //指针域，用来存储下一个节点的地址类型是ListNode
        public ListNode next;

        public ListNode(int val){//提供构造方法，用于节点的插入查找
            this.val = val;
        }
    }
    public ListNode head;//用来引用链表对象

    //弄一个始终指向尾节点的
    public ListNode tail;



    //头插
    @Override
    public void addFirst(int data) {
        //先实例一个节点
        //改变该节点的next，对接头结点的地址
        //然后改变head地址
        ListNode node = new ListNode(data);//把data丢进去就实例化节点了
        if (this.head == null){//分为如果一开始啥也没有
            this.head = node;

        }else{
            node.next = this.head;
            this.head = node;

        }

        //不过其实只要这两句也米问题
        /*node.next = this.head;
        this.head = node;*/


    }


    //需要注意的是
    /*
    cur.next != null;和 cur != null;是不一样的，想错了
    1.如果想让cur停在最后一个节点的位置，cur.next != null
    2.如果想把整个链表的吗，每个节点都遍历完，那么就是cur != null
     */
    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);
        if (this.head == null) {
            this.head = node;
        } else {
            //找到位置，我觉得是遍历找
            ListNode cur = this.head;
            //此时cur指向最后一个节点
            while (cur.next != null) {
                cur = cur.next;
                }
                cur.next = node;

            }

    }

    @Override
    public void  addIndex(int i,int data) throws Unusual {


        //限制一下i的范围,不能为负数
        if (i < 0 || i > size()){
            //自定义异常
                throw new Unusual("下标不合法");
        }
        if (i ==0 ){
            addFirst(data);
        }
        if (i == size()){
            addLast(data);
        }

        ListNode cur = searchPrev(i);
        ListNode node = new ListNode(data);
        node.next = cur.next;//先把后面的接好
        cur.next = node;


    }

    public ListNode searchPrev(int i ){
        ListNode cur = this.head;
        int count = 0;
        while ( count < i-1 ){
            cur = cur.next;
            count++;
        }
        return cur;
    }

    @Override
    public boolean contains(int key) {
        ListNode cur = this.head;
        while(cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }


    @Override
    public void remove(int key) {
        if (this.head == null){
            return ;
        }
        //删除头部问题
        if (this.head.val == key){
            this.head = this.head.next;//让head往后移动
        }
        //找前驱
        ListNode cur = FindPrev(key);
        //判断返回值是否为空
        if (cur == null){
            System.out.println("没有删除的数据");
            return ;
        }
        //3.删除
        ListNode tmp  = cur.next;
        cur.next = tmp.next;

    }

    public ListNode FindPrev(int key ){
        ListNode cur = this.head;

        while ( cur.next != null ){
            if (cur.next.val == key){
                return cur;
            }
            cur = cur.next;
        }
        return null;
    }
    //删除所有的key
    @Override
    public void removeAllkey(int key) {
        if (this.head == null ){
            return;
        }

        //两个节点一个指向头另一个遍历链表
        ListNode prev = head;
        ListNode cur = head.next;
        //定义两个节点指针，指向头另一个指向头的下一
        while(cur != null){
            //2.如果有某个节点的val值等于vag，那么让这个节点的上一个节点next存放该节点的next
            // （相当于就是跳过了那个相等的节点，跳过了相当于就是删除了）
            if (cur.val == key ){
                prev.next = cur.next;
                cur = cur.next;
            }else{

                prev = cur ;//让prev指向cur节点
                cur = cur.next;

            }
        }

        //删除头部问题
        //最后解决头部问题，对链表的头的val与val进行比较，如果同则让头指向下一个节点即可。
        if (this.head.val == key){
            this.head = this.head.next;//让head往后移动

        }

    }

    @Override
    public int size() {
        ListNode cur = this.head;
        int count = 0;
        while(cur != null){
            count++;
            cur = cur.next;
        }
        System.out.println("11");
        return count;
    }

    @Override
    public void clear() {
        if (this.head == null ){
            return;
        }

        ListNode cur = head;


        while(cur != null){
            //cur.val = Integer.parseInt(null);
            ListNode cur2 = cur.next;
            //cur.val = 0;
            cur.next = null;
            cur = cur2;

        }
        //头结点没处理
        head = null;

    }

    @Override
    public void display() {
        ListNode cur = this.head;

        while(cur != null){
            System.out.print(cur.val + "  ");
            cur = cur.next;
        }
        System.out.println();


    }


    //翻转链表，就是让第二个节点开始，依次进行头插
    public  ListNode overturn(){
        if (head ==null){
            return null;
        }

        //如果是一个结点直接
        if (head.next ==null){
            return head;
        }
        //改变链表内部结构
        ListNode cur = head.next;

        head.next = null;
        while(cur != null){
            //1.循环条件咋么设置，还是cur ！= null，因为最后一个是null
            // 头插能实现第一个，但是后面的2接上呢,让cur返回来就行了
            ListNode curNode = cur.next;
             cur.next = head;
             head = cur;
             cur = curNode;//当做指针来想就行了
        }
        return head;
    }


    //实现求链表的中间结点
    /*
    1.先求整个链表的长度
    2.再求长度/2找到这个中间节点
     */
    public ListNode middleNode(){

        ListNode cur = head;
        int len = size();
        for (int i = 0; i < len /2 ; i ++){
            cur = cur.next;
        }
        return cur;
    }


    //另一方法写
    //快慢，速度为2倍，同样的起点，终点一样，速度是2倍
    public ListNode middleNode02(){

        ListNode slow = head;
        ListNode fast = head;

        while(fast.next !=null ||fast  != null  )//fast两倍速度会提前一半到终点
        //奇数情况fast.next ==null，偶数情况fast  == null
            {
                slow = slow.next;//慢的走一步
                fast = fast.next.next;//2倍速度走两步

        }
        //走完说明slow在中间
        return slow;


    }


    //输入一个链表，输出该链表中倒数第k个结点。
    //1.fast 先走k-1步
    //2.然后slow和fast同时走
    //3.fast到最后slow的位置就是倒数k-1
    //需要注意的是要对k进行判断是否合法
    //还需要解决当链表为空或者fast指向了空的情况
    public ListNode findK(int k){
        if ( k<= 0 || head ==null ){
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;

        //怎么先让fast先走k-1步
        for (int i = 0 ; i < k-1 ; i ++){

            fast = fast.next; //如果k>size()那么fast就指向null了
            if (fast == null){//处理k太大问题
                return null;
            }
        }
        //同时走
        //奇偶情况是如何的呢，通过画图分析是一样的
        while (fast.next != null){

            fast = fast.next;
            slow = slow.next;

        }
        //走完了然后勒
        //走完就说明slow就是要求的位置
        System.out.println(slow.val);
        return slow;





        //第二种简单法子
       /* // write code here
        ListNode fast = head;
        int count = 0;
        while(fast != null){
            count++;
            fast = fast.next;
        }
        //限制一下k范围
        if(head == null || k < 0 || k >count){
            return null;
        }
        fast = head; //重置为头位置
        int len = count - k; //
        while(len > 0){
            fast = fast.next;
            len--;

        }
        return fast;*/

    }
}







