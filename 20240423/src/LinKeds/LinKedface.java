package LinKeds;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-23
 * Time: 16:11
 */
public interface LinKedface {
    //头插法
    void addFirst(int data);
    //尾插
    void addLast(int data);
    //任意位置插入，前提有效位置
    void addIndex(int i,int data) throws Unusual;
    //查找
    boolean contains(int key);
    //删除第一次出现的key节点，如果有两个相同的字节内容删除第一个
    void remove(int key);
    //删除所有值为key的节点
    void removeAllkey(int key);
    //获取链表长度
    int size();
    //清空链表
    void clear();
    //打印链表
    void display();


}
