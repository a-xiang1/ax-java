import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-23
 * Time: 11:08
 */

//数组的缺点，插入和删除时间复杂度大，扩容之后，可能浪费空间


public class CardTest {
    public static void dayin(List<Card> cardList){
        for (int i = 0 ; i < cardList.size() ; i++){
            if (i % 13  == 0  && i != 0){
                System.out.println();
            }
            System.out.print(cardList.get(i) + "  " );
        }
    }

    public static void main(String[] args) {
        CardDemo carddemo = new CardDemo();
        List<Card> cardList = carddemo.buyCard();
        System.out.println("牌如下： ");
        //System.out.println(cardList);

        dayin(cardList);

        //洗牌
        System.out.println();

        System.out.println("---------------");
        System.out.println("洗牌如下");
        carddemo.shuffle(cardList);
        dayin(cardList);
        System.out.println();
        System.out.println("------------");
        carddemo.draw(cardList);


    }
}
