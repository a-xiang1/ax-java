package TEST;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-10
 * Time: 17:03
 */

//事件演示
public class Test02 implements ActionListener {
    //创建按钮对象
      JButton jbt = new JButton("点左键点空格点");
    //创建按钮对象
     JButton jbt2 = new JButton("点点");




        //double a = 0;
        /*int b =  0;
        Scanner scanner = new Scanner(System.in);
        double a  =  scanner.nextDouble();
        b=((int)a) % 10;
        System.out.println(b);*/
public void init(){
    //测试动作时间
    //创建窗口对象
    JFrame jFrame = new JFrame();
    //设置界面的宽高
    jFrame.setSize(600,600);
    jFrame.setTitle("事件演示");//标题
    jFrame.setAlwaysOnTop(true);//设置窗口界面置顶
    jFrame.setLocationRelativeTo(null);//设置界面剧中
    jFrame.setDefaultCloseOperation(3);//关闭模式
    jFrame.setLayout(null);// 取消默认放置按钮啥的

    //写事件

    //设置位置和宽高
    jbt.setBounds(300,300,100,50);
    //给按钮增加事件
    jbt.addActionListener(this);

    //设置位置和宽高
    jbt2.setBounds(100,100,100,50);
    //给按钮增加事件
    jbt2.addActionListener(this);

    //给按钮添加动作监听
    //jbt：组件对象，表示你要给哪个组件添加事件
    //addActionListener:表示给组件添加哪个事件监听（动作包括左键和空格）
    //参数表示事件触发要执行的代码，可以自写一个类但需要对接addActionListener接口
    //也可以直接使用匿名类
    //jbt.addActionListener(new MyActionListener());
    //只使用一次就不需要写类来实现了，直接匿名内部类
   /* jbt.addActionListener(new ActionListener() {//匿名内部类对象
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("点击成功");
        }
    });*/



    //把按钮添加到界面中
    // jFrame.getContentPane().add(jbt);//先获取隐藏布局
   jFrame.getContentPane().add(jbt);
    jFrame.getContentPane().add(jbt2);


    jFrame.setVisible(true);//窗口可见
}




    @Override
    public  void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source == jbt) {
            jbt.setSize(200 * 2, 200 * 2);//点鸡后变大

        } else if (source == jbt2) {
            Random r = new Random();
            jbt2.setLocation(r.nextInt(500), r.nextInt(500));
        }

    }
}
