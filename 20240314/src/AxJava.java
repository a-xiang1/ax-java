/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-14
 * Time: 9:42
 */
public class AxJava {
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");
        //数组环节
        //数组的定义和初始化

        int [] arr3 = new int [] {1,2,3,4,5,6}; //这两中定义是一样的。
        int [] arr1 = {1,2,3,4};//在java，[里面是不能加数字的，自动会识别的。
        int [] arr2 = new int [5];//[]如果里面写了数字，那么就不能{}初始化了。，默认都是0

        //理解数组的意义
        int [] arr = {1,2,3,4};
        System.out.println(arr2);//输出的是数组的首地址，所以在这里跟c语言不同，数组是地址。
        //可以输出数组的长度
        System.out.println(arr.length);

        //数组使用时，常见的错误
        //数组越界
        /*System.out.println(arr[5]);//这个就越界了*/
        //空指针异常
        /*arr = null;
        System.out.println(arr.length);*/



    }
}
