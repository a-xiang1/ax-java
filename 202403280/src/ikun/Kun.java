package ikun;

import ax.AX;
import jdk.swing.interop.SwingInterOpUtils;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-28
 * Time: 10:01
 */
//protected可以实现不同包不同子类引用，就是要继承才行
//满足两个条件，必须是继承，然后是子类，加上要用super才能引用
public class Kun extends AX {
    public void fun(){

        System.out.println(x);
    }
    public static void main(String[] args) {
        //AX ac = new AX(5);
        Kun kk = new Kun();
        kk.fun();



    }
}
