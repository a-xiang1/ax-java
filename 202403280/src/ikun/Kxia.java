package ikun;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-28
 * Time: 10:32
 */
final  public class Kxia {  //加上final就不能继承了
    final int a = 10;
    //a= 15;//有了final只能初始化一次，所以第二次赋值是错误的。
    final void ar(){
        System.out.println("sds");//给方法加上final，能继承但是不能被子类重写。
    }
}
