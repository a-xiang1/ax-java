package kunk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//第三种
public class A3 {
	public static void main(String[] args) {
	
		File fin = new File("D:\\坤\\坤.txt");
		byte[] bytes = new byte[1024];
		try {
			InputStream fins = new FileInputStream(fin);//创建一个字节输入流对象
			//InputStream fins1 = new FileInputStream("D:\\坤\\坤.txt");//第二种创建
			OutputStream fouts = new FileOutputStream("D:\\坤\\坤坤.txt",true);//
			
			while(fins.read(bytes, 100, 500)>0) {//读入，返回值为-1时候结束
				fouts.write(bytes, 0, 500);//写入
			}
			
			fouts.flush();//刷新流
			//关闭流
			fouts.close();
			
			fins.close();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
