import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-26
 * Time: 16:38
 */
public class Main {
    public static void main(String[] args) {

                // TODO Auto-generated method stub
                //InputStream fins = null;
                FileInputStream fins = null;

                OutputStream fouts = null;
                try {
                    //InputStream fins = new FileInputStream("C:\\java\\javam\\ax-java\\20240426\\kun.txt");
                    fins = new FileInputStream("C:\\java\\javam\\ax-java\\20240426\\kun.txt");
                    //OutputStream fouts = new FileOutputStream("C:\\java\\javam\\ax-java\\20240426\\kunk.txt",true);//加上
                    fouts = new FileOutputStream("C:\\java\\javam\\ax-java\\20240426\\kunk.txt",true);

                    int count = fins.available();//available()返回当前剩余的字节数
                    byte[] a = new  byte[10];
                    while(fins.read(a) > 0) {//读
                        fouts.write(a);//写
                    }

                    fouts.flush();//刷新流


                }catch(FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException  e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }finally {

                    if (fouts != null){
                        //关闭流
                        try {
                            fouts.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (fins != null){
                        try {
                            fins.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }



                }
            }


    }

