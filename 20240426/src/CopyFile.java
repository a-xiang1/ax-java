import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-26
 * Time: 17:01
 */

//将文件的复制和剪切一些操作封装起来
public class CopyFile {
    private String beginFile;//传的是读的文件目录位置
    private String endFile;//传的是写的文件目录位置
    private File begin;
    private File end;

   //这个有参构造不太懂参数表示啥
    public CopyFile(String beginFile, String endFile) {
        super();
        this.beginFile = beginFile;
        this.endFile = endFile;

    }
    //无参构造方法
    public CopyFile() {
        super();
    }



    //剪切
    public void cutFile(){
        copyFiles();
        //复制然后把读的文件删除即可
        begin.delete();//File类的才可以删除

    }

    //复制文件方法
    //参数传的是读入的文件、写的文件目录路径
    public void copyFiles(String begins,String ends){
        setBeginFile(begins);//传进修改那个方法即可
        setEndFile(ends);
        //然后再进行复制
        copyFiles();

    }



    //复制文件方法
    //无参的方法
    public  void copyFiles(){

        InputStream fis = null;
        OutputStream fos = null;
        try {

            //创建字节输入和输出字符流对象
           /* fis = new FileInputStream("C:\\stun_v2.dat");
            fos = new FileOutputStream("C:\\java\\javam\\ax-java\\20240426\\stun_v2.dat",true);*/

            fis = new FileInputStream(beginFile);//beinFile做作为一个字符串类型就是用来存放文件的目录，构造方法的时候会进行初始化
            fos = new FileOutputStream(endFile,true);



            //进行读和写入
            byte[] bytes = new byte[1024 * 1024]; //就是一个mb的大小，给一个字节数组存放读入的数据
            int readCount = 0;//记录每次读的字节个数
            while((readCount = fis.read(bytes)) != -1){//读，读的内容放入到字节数组里面
                //写入
                fos.write(bytes,0,readCount);//从0开始，到readCount

            }

       fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            //关闭流，我习惯先关闭输出流
            if (fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }


    }




    public String getBeginFile() {
        return beginFile;
    }

    public void setBeginFile(String beginFile) {//修改
        this.beginFile = beginFile;
        this.begin = new File(beginFile);//让其值赋给文件类型的begin
    }

    public String getEndFile() {
        return endFile;
    }

    public void setEndFile(String endFile) {
        this.endFile = endFile;
        this.end = new File(endFile);
    }
}
