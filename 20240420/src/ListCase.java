import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-20
 * Time: 15:46
 */
public class ListCase {
    public static void main(String[] args) {
        //实现求某个正整数的所有素数
        /*
        1.创建两个列表
        2.一个存放待查数据，另一个存储已经确认的素数
        3.将待查列表的第一个数字放到素数列表，
        4.然后在待查列表进行循环一次去除那些是放入素数列表的那个数的倍数关系的数据
         */
        LinkedList<Integer> list = new LinkedList<>();//待查列表
        LinkedList<Integer> list2 = new LinkedList<>();//素数列表
        Scanner scanner = new Scanner(System.in);
        int tep = scanner.nextInt();
        for (int i = 2; i < tep ; i ++){//素数从2开始0和1不是
            list.add(i);
        }

        int first = 0;
        while(list.size() > 0){//待查列表为空时候结束
            first = list.removeFirst(); //removeFirst()返回列表的第一个元素，然后再列表里删除
            list2.add(first);//将素数放到素数列表
            //然后进行清除first倍数的数据，这也是清除素数的关键
            //比如2,2倍数关系的数4,6,8,10这些都不是素数
            for (int i = 0; i < list.size();){
                int num = list.get(i);//获取每一位元素进行判断处理
                if (num % first ==0){//==0说明是倍数关系，就要清除
                    list.remove(i);
                }else{
                    i ++;
                }
            }

        }
        for (Integer t : list2){
            System.out.print(" " + t + " ");
        }

    }
}
