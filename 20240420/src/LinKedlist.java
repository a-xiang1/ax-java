import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-20
 * Time: 15:18
 */
public class LinKedlist {
    public static void main(String[] args) {
        System.out.println("四战");
        LinkedList<Integer> list = new LinkedList<>();
        //add将元素加到列表的末尾,也可以指定位置插入元素
        //基于链表
        list.add(15);
        list.add(12);
        list.add(11);
        list.add(13);
        list.add(1,20);//在1下标插入20
        //插入尾部
        list.addFirst(20);
        list.addLast(520);
        //返回列表的头部元素
        System.out.println(list.getFirst());

        //迭代器遍历
        Iterator iter = list.iterator();
        while(iter.hasNext()){
            System.out.println(iter.next());
        }

        //查一个元素是否存存在，若存在返回下标不存在返回-1
        System.out.println(list.indexOf(520));
        System.out.println("---*");
        System.out.println(list.offerFirst(520));
        for (Integer t : list){
            System.out.print("  " +t + "  ");
        }
        System.out.println();
        System.out.println(list.pollFirst());//返回头部元素，会从表中删除
        System.out.println(list.peekFirst());//返回头部元素，不会删除

        System.out.println();
        for (Integer t : list){
            System.out.print("  " +t + "  ");
        }

        //栈顶元素出栈,出栈出的是头部
        System.out.println();
        System.out.println(list.pop());
        //入栈,入的也是头部
        list.push(50000);
        System.out.println("*********");
         Iterator iterator = list.iterator();
         while(iterator.hasNext()){
             System.out.println(iterator.next());
         }


        //普通for循环遍历
        /*for (int i = 0 ; i < list.size(); i ++){
            System.out.println(list.get(i));
        }*/








    }
}
