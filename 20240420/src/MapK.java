import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-20
 * Time: 18:26
 */
public class MapK {
    //Map的遍历
    public static void main(String[] args) {
        //第一种，获取所有的key，通过遍历key来遍历value
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"韦坤");
        map.put(3,"爱");
        map.put(6,"打");
        map.put(10,"篮球");
        Set<Integer> sets = map.keySet();
        //遍历Map
        //获取所有的key，所有的key是一个Set集合。
        Iterator<Integer> iter = sets.iterator();
        /*while(iter.hasNext()){
            //取出一个key
            Integer key = iter.next();
            //通过key获取value
            String value = map.get(key);
            System.out.println( key +" = " + value);
        }*/
        //第二种直接用entrySet（），能返回键值对所有
        //以上方法就是把Map集合直接全部转换成Set集合
        //此时Set集合中的元素类型是Map.Entry
        Set<Map.Entry<Integer,String>> set2 = map.entrySet();
        //迭代器
        Iterator<Map.Entry<Integer,String>> it = set2.iterator();
        while(it.hasNext()){
            Map.Entry<Integer,String> node = it.next();
           /* Integer key = node.getKey();
            String value = node.getValue();*/
            System.out.println(node.getKey() +" = " + node.getValue());

        }
        System.out.println("--------------");
        for(Map.Entry<Integer,String> t : set2){
            System.out.println(t.getKey() + "---" + t.getValue());
        }





    }
}
