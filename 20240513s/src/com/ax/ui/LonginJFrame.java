package com.ax.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-14
 * Time: 17:48
 */
public class LonginJFrame extends JFrame implements MouseListener {

    Yzm yzmy = new Yzm();
    String path = "image\\animal1\\";

//用集合来设置一下密码和用户名，注册的话要用到数据库，目前写不了
    static ArrayList<Uset> list = new ArrayList<>();
    //泛型弄一个对象类型
    static {//这个静态内部类什么意思,应该就是能够直接调用
        list.add(new Uset("zhangsan","520"));
        list.add(new Uset("ax","1314"));

    }

    JButton login = new JButton();//点击按钮放到成员，方便判断
    JButton register = new JButton();


    //方便获取用户名和密码、验证码放在成员
    JTextField username = new JFormattedTextField();
    JPasswordField password = new JPasswordField();//密码框隐藏***
    JTextField code  = new JTextField();

    public LonginJFrame() {
        initJFrame();

        //对界面进行美化
        initView();

        this.setVisible(true);
    }

    private void initView() {

        //清空一下
        //清空原本已经出现的图片
        this.getContentPane().removeAll();

        new Yzm().Main();
        //添加用户名
        JLabel usernameText = new JLabel(new ImageIcon(path+"yhm.png"));
        usernameText.setBounds(95,90,70,38);
        this.getContentPane().add(usernameText);

        //2.添加用户名文字文本输入框

        username.setBounds(180,90,200,30);
        this.getContentPane().add(username);

        //3.添加密码
        JLabel passwordText = new JLabel(new ImageIcon(path+"mima.png"));
       passwordText.setBounds(95,140,70,38);
        this.getContentPane().add(passwordText);

        //4.添加密码文本输入框

        password.setBounds(180,140,200,30);
        this.getContentPane().add(password);

        //5.添加验证码
        JLabel codeText = new JLabel(new ImageIcon(path+"yzm.png"));
        codeText.setBounds(95,195,70,38);
        this.getContentPane().add(codeText);

        //6.添加验证码文本输入框
        code.setBounds(180,195,80,30);
        this.getContentPane().add(code);

        //7.添加随机的验证码，要写一个生成验证码的类，暂时不会
        //随机的验证码也是个图片
        JLabel codes = new JLabel(new ImageIcon(path+"666.png"));
        codes.setBounds(220,160,200,100);
        this.getContentPane().add(codes);

        //8.添加登录按钮
        /*JLabel login = new JLabel(new ImageIcon("image\\animal1\\dl.png"));
        login.setBounds(95,250,127,52);
        this.getContentPane().add(login);*/

        //用按钮控件来,按键才有鼠标或者键盘的事件应该是

        login.setBounds(95,250,127,52);
        login.setIcon(new ImageIcon(path+"dl.png"));
        //去除按钮的边框
        login.setBorderPainted(false);
        //去除按钮的背景
        login.setContentAreaFilled(false);
        this.getContentPane().add(login);
        //给登录按钮设置鼠标事件
        this.getContentPane().add(login);
        login.addMouseListener(this);



       //8.添加注册按钮
        //JButton register = new JButton();
        register.setBounds(280,250,135,45);
        register.setIcon(new ImageIcon(path+"zc.png"));
        //去除按钮的边框
        register.setBorderPainted(false);
        //去除按钮的背景
        register.setContentAreaFilled(false);
        this.getContentPane().add(register);
        //给注册按钮设置鼠标事件
        register.addMouseListener(this);


        //9.添加背景
        JLabel  background = new JLabel(new ImageIcon(path+"bj04.png"));
        background.setBounds(0,0,509,520);
        this.getContentPane().add(background);

    }

    private void initJFrame() {
        //设置宽高
        this.setSize(509,480);
        //设置标题
        this.setTitle("登录窗口");
        this.setDefaultCloseOperation(3);//关闭模式
        this.setLocationRelativeTo(null);//居中
        this.setAlwaysOnTop(true);//置顶
        this.setLayout(null);//取消默认布局


    }

    //写一个点击登录后判定结果的方法
    public void showJDialog(String content){
        //创建弹框对象
        JDialog jDialog = new JDialog();
        //弹框大小
        jDialog.setSize(200,150);
        //弹框置顶
        jDialog.setAlwaysOnTop(true);
        //弹框居中
        jDialog.setLocationRelativeTo(null);
        //弹框不关无法操作
        jDialog.setModal(true);


        //this.setLayout(new FlowLayout());//设置窗口布局
        //创建Jlabel对象管理文字添加到弹框中去
        JLabel warning = new JLabel(content);
        warning.setBounds(20,10,200,150);
        jDialog.getContentPane().add(warning);

        //弹框显示
        jDialog.setVisible(true);

    }

    //单击
    @Override
    public void mouseClicked(MouseEvent e) {

    }
    //按下不松
    //按下不松切换登录按钮的背景图片
    @Override
    public void mousePressed(MouseEvent e) {
        Object ob = e.getSource();
        if (ob == login){
            System.out.println("登录");
            //怎么实现按下不松时候修改背景颜色
            //login.setIcon(new ImageIcon(""));
            login.setIcon(new ImageIcon(path+"dl.png"));
        }

    }
    //松开
    //先获取用户的用户名
    //然后判断啥的
    @Override
    public void mouseReleased(MouseEvent e) {
        Object ob = e.getSource();
        if (ob == register){
            System.out.println("注册");

        }else if (ob == login){
            System.out.println("登录");
            //获取用户输入的用户名，密码，验证码
            //获取用户名
            //从输入的文本框里面的数据进行获取
            String name =username.getText();//可是我不知道输入框里面是啥数据类型
            System.out.println("用户名 "+name);
            //获取密码
            //char[] pass =password.getPassword();
            String s = new String(password.getPassword());
            System.out.println("密码 "+s);

            //获取验证码
            String yzm01 =code.getText();
            System.out.println("输入的验证码 "+yzm01);

            //开始比较
            //先比较验证码
            //我想要他不区分大小写equalsIgnoreCase用这个方法
            //错误然后怎么刷新验证码呢
            //能刷新验证码，但是他图片不跟着变化，我也没办法
            System.out.println("当前正确验证码：" + yzmy.yzm);
            if (yzmy.yzm.equalsIgnoreCase(yzm01)){
                System.out.println("验证码正确继续判断用户名和密码");
            }else{//错误跳出弹框返回
                showJDialog("验证码输入错误请重新输入");
                //更新验证码
                //先把之前的情况
                //yzmy.yzm = "";
                //刷新那个验证码图片
                //initView();
                return;
            }

            //判断用户名和密码是否为空，有一个空就不对
            //用户名和密码为空怎么判断，他并不是null

            if (name.length() == 0 && s.length() == 0){
                //为空跳弹框
                showJDialog("密码或者用户名不能为空！");
                return;

            }
            //判断用户名和密码是否为空

            if ((name.equals(list.get(0).name) && s.equals(list.get(0).password)) ||
                    (name.equals(list.get(1).name) && s.equals(list.get(1).password))){
                //判断完成，正确则跳转拼图界面
                System.out.println("登录成功，请尽情玩耍吧");
                showJDialog("登录成功，请尽情玩耍吧");
                new GameJFrame();
            }else{//错误弹出错误框

                showJDialog("用户名或密码错误请重新输入！");
            }

        }

    }
//划入
    @Override
    public void mouseEntered(MouseEvent e) {

    }
//划出
    @Override
    public void mouseExited(MouseEvent e) {

    }
}
