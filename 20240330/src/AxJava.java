/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-30
 * Time: 10:04
 */
//很多类放到一个文件夹里面
class Animal{  //动物
    public String name;
    public int age;

    public void eat(String name){
        this.name = name;
        System.out.println(name + "在吃饭");
    }
    //父类设置构造方法，但是要在子类里面进行初始化不然报错

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Animal() {

    }
    //创建一个重写方法
    public void show(){
        System.out.println("父类吃");
    }
}

class Dog extends Animal{
    public void bark(String name){
        this.name = name;
        System.out.println(name+"汪汪");
    }
    public Dog(String name,int age){
        super(name,age);
    }
    Dog(){
        System.out.println("Dog");
    };

    public void show(){
        System.out.println("狗吃");
    }

   /* public static void main(String[] args) {
        new Animal();
        new Dog();
    }*/

}


//再来一个子类
class Cat extends Animal{

    @Override
    public void show() {
        System.out.println("猫吃");
    }
}

public class AxJava {

    //方法的参数，传参的时候向上转型
    public  static void func(Animal animal){
        System.out.println("韦坤");
    }

    //返回值  向上转型
    public  static Animal func2(){
        Dog dog = new Dog();
        return dog;
    }

    public static void duotai(Animal animal){
        animal.show();

    }
    public static void main(String[] args) {
        Dog dog = new Dog("王王",18);
        dog.bark("阿黄");
        dog.eat("小花");
        func(dog);
        Animal cat = new Cat();
        Animal animal  = new Animal();

        new Animal();
        new Dog();
        Animal dw = new Dog();//上下转型，直接赋值
        //dw.show();//经过上下转型，那么父类就可以引用到子类了，不过前提要满足方法重写、这叫动态绑定


        //演示多态
        duotai(dog);
        duotai(cat);



    }
}
