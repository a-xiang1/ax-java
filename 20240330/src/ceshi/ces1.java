package ceshi;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-30
 * Time: 10:54
 */


//继承关系
//父类和子类都包含静态代码块 、实例代码块、构造方法
/*
    1.父类的静态
    2.子类的静态
    3.父类的实例
    4.父类的构造
    5.子类的实例
    6.子类的构造*/
class X{
    Y y = new Y();
    public X(){
        System.out.println("X");
    }

}
class Y{
    public Y(){
        System.out.println("Y");
    }
}

public class ces1 extends X{
    Y y = new Y();
    public ces1(){
        System.out.println("Z");
    }

    public static void main(String[] args) {
            new ces1();
    }
}
