import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-24
 * Time: 17:58
 */
public class IOEStream {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("C:\\java\\javam\\ax-java\\20240424\\韦坤.txt");
            //读开始
            //available用法就是求剩余没有被读的字节数，用处就是求剩余数，然后给字节数组作为范围，这样就可以一次读完
            /*System.out.println("总字节数： " + fis.available());
            byte[] bytes = new byte[fis.available()];
            int readCount = fis.read(bytes);//这种不太适合大文件，数组不能太大
            System.out.println(new String(bytes,0,readCount));*/
            //skip跳过n个字节
            fis.skip(1);
            System.out.println(fis.read());



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if (fis != null){
                try {
                    fis.close();//关闭流
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
