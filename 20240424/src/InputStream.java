import javax.sound.midi.MidiFileFormat;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-24
 * Time: 17:25
 */
public class InputStream {
    public static void main(String[] args) {
        //改用字节数组输读入
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("C:\\java\\javam\\ax-java\\20240424\\韦坤.txt");
            //数组来读
            //如果要读很大的数据，并且有中文的情况下，数组范围设置很大，才不会乱码
            byte[] bytes = new byte[100];//准备一个长度为4的byte数组一次读取4个字节

            //r这里read返回的是读取到的字节数量（不是字节的本身）
            /*int readCount  = fis.read(bytes); //这里需要加个可能会出现的异常，捕获一下
            System.out.println(new String(bytes,0,readCount));
            System.out.println(readCount);

            readCount  = fis.read(bytes); //再次调用就是继续往后读看看剩几个，直到返回-1时候就没了
            System.out.println(readCount);
            //字节数组转换成字符串，但是要做到读到几个就转几个。
            System.out.println(new String(bytes,0,readCount));


            readCount  = fis.read(bytes);//此时返回-1说明已经读完了
            System.out.println(readCount);*/

            //改造成循环
            int readCount  = 0;
            while(((readCount = fis.read(bytes)) != -1)){
                System.out.print(new String(bytes,0,readCount));
            }












        } catch (FileNotFoundException e) {
            e.printStackTrace();

            //关闭是必须
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if (fis != null){
                try{
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
