import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-24
 * Time: 16:06
 */
public class IOStream {
    public static void main(String[] args) throws IOException {
        System.out.println("韦坤法王");

        //字节流
        //创建文件字节输入流对象
        //这个创建字节流可能会异常，需要声明
        //文件的路径C:\java\javam\ax-java\20240424\韦坤.txt，他会自动变成双斜杠，单斜杠是表示转义，\\表示绝对路径
        //C:/java/javam/ax-java/20240424/韦坤.txt这样写也行属于相对路径
         //throws FileNotFoundException
//        FileInputStream fis1 = new FileInputStream("C:\\java\\javam\\ax-java\\20240424\\韦坤.txt");
//        fis1.close();//关闭流，使用那就必须关闭

        //read一次一个字节的交换，效率低，用数组来处理更快
        FileInputStream fis = null;
        try {
             fis = new FileInputStream("C:/java/javam/ax-java/20240424/韦坤.txt");
             //在这里读
            /*for (int i = 0 ; i < 3 ; i++){
                int readData = fis.read();
                System.out.print("读字节 ： ");
                System.out.println(readData);

            }*/

           /* int readData = 1;
            while(readData != -1){ //当读取返回-1的时候那么就是已经读完
                readData = fis.read();
                System.out.print(readData + "  ");
            }*/


          /*  while(true){ //当读取返回-1的时候那么就是已经读完
                int readData = fis.read();
                if (readData == -1){
                    break;
                }
                System.out.print(readData + "  ");

            }
            */
            //改造while
            int readData = 0;
            while (((readData = fis.read() )!= -1)){
                System.out.print(readData + "  ");
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally{
            //在finally中确保一定要关闭流
            if (fis != null){//避免空指针异常
                try {
                    fis.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }



    }
}
