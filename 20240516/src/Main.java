import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-16
 * Time: 10:39
 */
public class Main {
    public static void main(String[] args) {
        new Solution().isValid("({})");
    }

}


//解决括号匹配问题
class Solution {
    public boolean isValid(String s) {

        Stack<Character> stack = new Stack<>();
        //1.遍历字符串
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i); //取栈元素
            //判断是不是左括号
            if((ch == '{') || (ch == '(') || (ch == '[')){
                //是左括号就放到栈中
                stack.push(ch);
            }else{//不是左就是右
                //右话分几种情况，看看栈是不是空，如果是则不匹配
                if(stack.empty()){
                    return false;
                }
                //如果不是空说明栈中有左括号，然后拿出来进行与右括号看看是否匹配
                //获取一下左括号
                char ch2 = stack.peek();
                if ((ch2 == '{' && ch == '}') || (ch2 == '(' && ch == ')') || (ch2 == '[' && ch == ']')){
                   //如果篇匹配就出栈
                    stack.pop();
                }else{
                    return false;
                }

            }

        }
        //走完一遍判断是否栈跟字符串都为空，如果有一方提前为空那么就是false
        if (!stack.empty()){
            return false;
        }
        return true;

    }
}
