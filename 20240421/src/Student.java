import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-21
 * Time: 10:17
 */
public class Student {
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //重写equals


  /*  @Override
    public boolean equals(Object object) {
        //object instanceof Student就是用来判断object的类型是不是与Student有继承关系的，比如Integer就不是，String可以
       if (object ==null || !(object instanceof Student)){
           return false;
       }
       if (object == this){//这个是同一个对象同一个内存地址
           return true;
       }
       Student s = (Student) object;//将object类转换成Student类然后才进行比较
        return this.name.equals(s.name);
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

