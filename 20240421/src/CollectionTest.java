import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-21
 * Time: 16:48
 */
public class CollectionTest {
    public static void main(String[] args) {
          //对Set集合排序
        Set<String> set = new HashSet<>();
        set.add("a");
        set.add("d");
        set.add("b");
        set.add("p");

        //需要将set集合转成list集合
        List<String> list = new ArrayList<>(set);//ArrayList构造方法有参和无参两种
        Collections.sort(list);
        /*for (String S : list){
            System.out.println(S);
        }*/

        //list集合掌握
        // 创建对象
        //添加元素、取出元素
        //遍历
        //ArrayList<String > list1 = new ArrayList<>();
        LinkedList<String > list1 = new LinkedList<>();

        //list1.add("125");
        list1.add("20");
        list1.add("264");
        list1.add("147");


        String s = list1.get(0);
        System.out.println(s);

        //List<String> list2 = new ArrayList<>(list1);
        Collections.sort(list1);
        //便利
        for (String s1 : list1 ){
            System.out.print(s1 + "  ");
        }

        //Properties
        //创建对象
        Properties pro = new Properties();
        //存
        pro.setProperty("add","est");
        pro.setProperty("ad","tst");
        pro.setProperty("a","tet");
        //取
        System.out.println();
        System.out.println(pro.getProperty("add"));
        System.out.println(pro.getProperty("a"));




    }
}
