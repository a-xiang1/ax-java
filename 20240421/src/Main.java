import java.util.Comparator;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-21
 * Time: 10:17
 */
//
    /*
    1.拿put举例，什么时候equals不会调用
    k.hashcode（）方法返回哈希值，哈希值经过哈希算法转换成数组下标
    数组下标位置上如果是null，equals不需要执行
    2.如果一个类的equals方法重写了，那么hashcode方法必须重写
    并且equals方法返回是true那么hashcode返回值一样
    equals返回true说明两个对象相同，在同一个单向链表上比较，对于同一个单向链表的节点来说，他们的哈希值是相同的
    所以hashcode返回值相同。
    3.equals方法和hashcode方法需要同时重写
    4.哈希值01和02相同，一定放在同一单向链表
    5.hashcode集合key允许null，但是null也只能是一个，key是无重复的
     */
public class Main {
    public static void main(String[] args) {
        Student student = new Student("zhangsan");
        Student student2 = new Student("zhangsan");
        //student.equals(student2);//没重写equals是flase,因为equals默认是比较的是对象的地址，
        // 这里比较的是两个对象，因此不一样

        //重写equals方法
        System.out.println(student.equals(student2));
        System.out.println(student.hashCode());//重写hashcode后返回值一样
        System.out.println(student2.hashCode());

        //能实现按照字典大小排序
        //TreeSet<String> ts = new TreeSet<>();
        //TreeMap<String,String> ts2 = new TreeMap<>();
        /*ts.add("cai");
        ts.add("xu");
        ts.add("kun");
        ts.add("kunk");
        for (String s : ts){
            System.out.println(s);
        }*/
        /*ts2.put("wang","s");
        ts2.put("zhang","s");
        ts2.put("kk","s");
        ts2.put("cai","s");
        Set<Map.Entry<String,String>> list = ts2.entrySet();
        for (Map.Entry<String,String> s : list){
            System.out.println(s);
        }*/


        //能否对自定义的类型进行排序
        //不可以，因为自定义的类型没有实现comparbl接口，如果要实现就需要重写comparbl接口
        TreeSet<Tes> tes = new TreeSet<>();
        Tes t1 = new Tes(42,"zhangsan");
        Tes t2 = new Tes(52,"lisi");
        Tes t3 = new Tes(32,"shenjin");
        Tes t4 = new Tes(12,"老六");
        Tes t5 = new Tes(32,"赵五");
        tes.add(t1);
        tes.add(t2);
        tes.add(t3);
        tes.add(t4);
        tes.add(t5);
        for (Tes t : tes){
            System.out.println(t);
        }

        TreeSet<Wu> wu = new TreeSet<>(new Comparator<Wu>() {//Comparator可以直接new使用，可以写多个
            @Override
            public int compare(Wu o1, Wu o2) {
                return o1.age - o2.age;
            }
        });
        wu.add((new Wu(100)));
        wu.add((new Wu(200)));
        wu.add((new Wu(180)));
        for (Wu t : wu){
            System.out.println(t);
        }


    }
}

class Wu{
    public int age;

    public Wu(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Wu{" +
                "age=" + age +
                '}';
    }

}
class Tes implements Comparable<Tes>{
    private int  age;
    private  String name;


    public Tes(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tes{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
    //重写比较接口
    //实现先比较年龄排序，如果年龄相同用姓名比较
    @Override
    public int compareTo(Tes o) {
        if (this.age == o.age){
            return this.name.compareTo(o.name); //这个compareTo是String重写过的方法可以比较字符串排序
        }else{
            return o.age - this.age; //降序
        }

    }
}
