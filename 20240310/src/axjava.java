import jdk.jshell.SourceCodeAnalysis;

import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-10
 * Time: 9:49
 */
public class axjava {
    public static void main(String[] args) {
        //\"转为“，\\转为\。
        System.out.println("\\\"hello\\\"");
        /*short a =128;
        byte b =(byte) a;
        System.out.println(b);
        System.out.println(a);*/
       /* int c = 3;
        byte  d = c;*/
        /*double x = 2.0;
        int y = 4;
        x/= ++y;
        System.out.println(x);*/

       /* int a = 10;
        System.out.println(a);*/

       /* int i = 1;
        while(i <= 10)
        {
            System.out.print(i + " ");//拼接
            i ++;

        }*/

        //分别计算100内所有奇数、偶数的和
        /*int i = 1;
        int sumodd = 0;
        int sumeven = 0;
        while(i <= 100)
        {
            //首先判断是奇数
            //然后进行累加求和。
            if(i % 2 == 0)//偶数对二求余必为0
            {
                sumeven += i;
            }
            else
            {
                sumodd += i;
            }
            i ++;//每轮i都要进行++
        }
        //求和完成，打印数据
        System.out.println("偶数和 = "+ sumeven);
        System.out.println("奇数和 = "+ sumodd);*/

        //求5的阶乘之和
        /*int i = 1;
        int ret = 1;
        while(i <= 5)
        {
            ret *= i;
            i ++;
        }
        System.out.println(ret);*/

        //那么求1+2+3+4+...的阶乘之和
        //就是在基础之上在嵌套一个循环就可以了
        /*int j = 1;
        int sum = 0;
        while(j <= 5)
        {
            int i = 1;//这里有个细节，i和ret必须每次循环都要重置为1.
            int ret = 1;
            while(i <= j)
            {
                ret *= i;
                i ++;
            }
            sum += ret;
            j ++;
        }
        System.out.println(sum);*/

        //区分break和continue
        /*int j = 1;
        while(j <= 5)
        {
            if(j == 2)
            {
                break;//当j=2时候，
            }
            System.out.println(j); //这里只会输出1就结束了。
        }
        j = 1;
        while(j <= 5)
        {
            if(j == 2)
            {
                continue;//会进入死循环，永远停留在j=2
            }
            System.out.println(j);
        }*/

        /*int k = 1000;
        int i = 0;
        while(k > 1)
        {
            System.out.print(k + " ");
            k = k/ 2;
            i ++;

        }
        System.out.println();
        System.out.println(i);*/

        //找到1-100之间既能被3整除也能被5整除的所有数字；要求必须使用continue或者break
        //int i = 1;
        //int j = 0;
       // while(i <= 100)
       // {
            //既能被3整除也能被5整除的所有数字
            //还是用到if以及运算符 && 。

            //法一
            /*if(i % 3 != 0)
            {
                i++;
                continue;
            }
            if(i % 5 != 0)
            {
                i++;
                continue;
            }*/
            //法二
            /*if(i % 3 != 0  && i % 5 != 0 )
            {
                i++;
                continue;
            }
           if(i % 3 == 0  && i % 5 == 0 )
            {
                System.out.println(i);
                j ++;
            }
            i ++;*/

            //法三，最简，就是判断对15求余即可。
          /*  if(i % 15 != 0)
            {
                i ++;
                continue;
            }
            System.out.println(i);
            i ++;*/

       // }

        //java输入玩法
        Scanner scan = new Scanner(System.in);//这个就是相当于写好引用，头文件自动生成，叫做导入包
        //接下来就可以使用输入了
       /* System.out.println("请输入你的id");
        String name = scan.next();//next只读有效的字符，遇到空格、tab、enter键，那么就会停止结束。
        System.out.println(name);*/

        //输入字符串
        /*ystem.out.println("请输入你的id");
        String name = scan.nextLine();//nextLine 输入字符串，遇到enter才结束
        System.out.println(name);*/

        //输入整型
        /*System.out.println("请输入你的年龄");
        int ax = scan.nextInt();//只会录入空格前的，遇到空格、tab、enter键就停止了
        System.out.println(ax);

        //也可以进行循环输入
        //循环结束用ctrl+d
        while(scan.hasNextInt())//就是如果有数字的输入就不会停止
            //想停止两方法，ctrl+d，还有输入其他字符，不是数字的内容。
        {
            int age = scan.nextInt();
            System.out.println(age);
        }
        scan.close();//这个就是类似于文件操作的关闭，有打开那就有关闭。*/

        //java的猜数字实现
        //首先学习java中如何生成随机数
        //生成随机数要用到Random
        Random random = new Random();//对应的会产生一个对应的包。
        //Scanner scan = new Scanner(System.in);

        /*int randNum = random.nextInt(100); //在里面可以输入随机值的范围大小.这里100，表示0~100，但不包含100
        System.out.println("随机值 ： " + randNum);
        while(true)
        {
            System.out.println("请输入你猜的数字：");
            int x = scan.nextInt();
            if( x < randNum){

                System.out.println("猜小咯 ");
            }
            else if( x > randNum){

                System.out.println("猜大咯 ");
            }
            else{
                System.out.println("恭喜,猜对咯");
                break;
            }

        }*/

        //判断一个数是不是素数
        //有两方法
        //其一，就是通过循环一个数一个数的对x求余，当余数等于0，那么就不是素数，只有大于开根号数时候才是
        //比如16 ，可以写成1*16 2*8 4*4 ，其中2小于16开根号，那么就不是素数
        /*int num = scan.nextInt();
        // i = 2; //从二开始，因为每个数都能被1整除，所以1毫无意义
        for(i = 2 ;i <= Math.sqrt(num) ; i++ )
        {
            if(num % i == 0 ){
                break;//不是素数,1和2不是素数
            }
        }
        if( i > Math.sqrt(num)){ //如果i大于Math.sqrt(num)，那么就是素数
            System.out.println(num + "是素数");
        }*/


       //求两个数的最大公约数
        //就是用转转相除法比如 18 和 24 最大公约数就是6
        //过程，24 % 18 = 6,然后下一步就是进行判断，如果18 % 6 = 0,那么6就是最大公约数

        /*int a = scan.nextInt();//默认a大于b吧
        int b = scan.nextInt();
        int c = 0;
        while(true)
        {
            c = a % b;
            if( b % c == 0 )
            {
                System.out.println(c);
                break;
            }
        }*/

        /*int a = scan.nextInt();
        int b = scan.nextInt();
        int c = a % b;
        while( c != 0)
        {
            a = b;//这里是防止a<b吧
            b = c;
            c = a % b;

        }
        System.out.println(b);*/

        //求水仙花数
        //栗子 i= 153 = 1^3+5^3+3^3
        //先求这个数是几位数
        //然后再次循环求出这个数的每一位数取出来
        /*for(int i = 1 ; i <= 999999 ;i ++)
        {
            int count = 0;
            int tmp = i;
            while(tmp != 0){
                count ++;
                //一次除去10，每次去掉一位数
                tmp = tmp / 10;
            }
            //计算完再进行重新赋值
            tmp = i ;
            int sum = 0;//用于求和
            while(tmp != 0){

                sum+=Math.pow(tmp % 10,count);//这个函数就是传两个参数，然后进行相乘,(2,3)=2*3
                tmp /= 10;//每次去掉最后一位
            }
            if(sum == i)
            {
                System.out.println(i);
            }


        }*/

        //求二进制的个数
        int ax =scan.nextInt();
        int count = 0;//用来记录二进制中1的个数
        //用求二进制的方法来求1的个数；对2取余，余1的话就说明二进制中有个1，然后除以二，在取余，直到除2之后为0
        while (ax > 0)
            {
                if (ax%2==1)
                {
                    count++;
                    ax /= 2;
                }
                else
                {
                    ax /= 2;
                }
            }

        System.out.println(count);


    }
}
