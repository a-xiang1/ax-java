import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-12
 * Time: 14:53
 */
public class axjava12 {
    Scanner in = new Scanner(System.in);

    /*public static void main(String[] args) {
        //给二维数组填入

        Scanner scan = new Scanner(System.in);
        while (scan.hasNextInt()) { // 注意 while 处理多个 case
                int a = scan.nextInt();
            for (int i = 0; i < a; i++) {

                for (int j = 0; j < a; j++) {

                    if (i == j || i + j == a - 1) {
                        System.out.print("*");
                        //printf("%c", b[i][j]);
                    } else {
                        System.out.print(" ");
                    }
                   //System.out.println();

                }
                System.out.println();
            }

        }
    }*/
    /*public static void main11(String[] args) {

        *//*首先定义一个记录数字9出现次数的变量number，
        再通过对数字取余判断个位上数字9出现的次数，
        对数字除10判断十位上数字9出现的次数，
        最后将这两部分相加可得出1~100内数字9出现的次数的结果*//*
            int number=0;
            for(int i=1;i<=100;i++) {
                int t = i;
                if (t % 10 == 9) number++;
                if (t / 10 == 9) number++;
            }
        System.out.println(number);


    }
*/


    //计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值    。
    public static void fun(){
        double sum = 0.0;
        //用来交换正负
        int flag = 1;
        for(int i = 1 ; i <= 100 ; i++){
            sum += (1.0 / i) * flag;
            flag = -flag;

        }
        System.out.println(sum);
    }
    //模拟密码登录，只有三次机会
    public static void login (){//login进入系统
        Scanner in = new Scanner(System.in);
        String password = "123456";//设置密码
        int count = 3 ;//设置次数
        while( count != 0){
            System.out.print("请输入密码： ");
            String shuru = in.nextLine();
            if(shuru.equals(password)){//java的字符串比较用equals
                System.out.println("登录成功");
                break;
            }
            else{
                count--;
                System.out.println("密码错误，你还有"+count+"次机会！");
            }
        }

    }
    //输出一个整数的每一位，如：123的每一位是3，2，1
    //就是逆序
    public static void fun2(){
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        while(x != 0){

            int tmp = x % 10;
            System.out.println(tmp);
            x = x / 10;

        }

    }

    //创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。
     //要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算
    public static int max2(){
        int max = 0;
        int a = 10 , b = 20 ;
        if(a > b)
        {
            max = a;

        }else{

                max = b;
        }
        return max;

    }
    public static void max3(){
        int c = 15;
        if(c >= max2()){
            System.out.println(c);
        }
        else {
            System.out.println(max2());

        }

    }


    //方法重载
    //能调用多个相同名的方法
    public static int add(int x){
        System.out.println(x);
        return x;
    }
    public static double add(double x){
        System.out.println(x);
        return x;
    }

    public static void function(){
        /*int x = add();*/
        double a = add(1.0);
    }

    public static void main(String[] args) {

        //fun();
        /*login();*/
        //fun2();
       // max3();

        //方法重载
        //方法名相同；参数列表是不同的（个数，顺序，类型）
        //返回值同不同都无所谓
        function();



    }
}
