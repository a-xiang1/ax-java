module com.example.axjava {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.axjava to javafx.fxml;
    exports com.example.axjava;
}