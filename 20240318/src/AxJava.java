/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-18
 * Time: 9:50
 */
//类
//成员变量 {1.普通成员变量 ；2.静态成员变量}
//成员方法 {1.普通成员方法 ；2.静态成员方法}

    //类描述一类东西的

class PetDog {
    public String name;//名字
    public String color;//颜色
    // 狗的属性
    public void barks() {
        System.out.println(name + ": 旺旺旺~~~");
    }
    // 狗的行为
    public void wag() {
        System.out.println(name + ": 摇尾巴~~~");
    }

}

/*class AX{//类用来描述
    //属性 成员变量
    //public 是访问限定符
    public static int age;
    public String bad;

    //行为  成员方法
    public void wasches(){

    }

}*/

    //类的引用
class Date {
        public int year;
        public int month;
        public int day;

        public  void setDate(int y ,int m ,int d){//使用this的好处
            this.year = y;
            this.month = m;
            this.day = d;
        }
        public void print(){
            System.out.println(year + ": " + month + ": " + day + ": ");
        }

}
public class AxJava {
        /*public int year;
        public int month;
        public int day;

        public  void setDate(int y ,int m ,int d){//使用this的好处
            this.year = y;
            this.month = m;
            this.day = d;
        }*/

    public static void main(String[] args) {
            //定义类，实例化
       /* PetDog petDong1 = new PetDog();
        PetDog petDong2 = new PetDog(); //然后petDog2就是指向petDog类的类型
*/

        //还能引用类里面的成员
        /*petDong1.name = "ax";
        petDong1.color = "红";
        petDong2.name = "韦坤";
        petDong2.color = "黄";
        System.out.println(petDong1.name);
        System.out.println(petDong1.color);
        //调用类的方法
        petDong1.wag();*/
        /*AxJava ax = new AxJava();
        ax.setDate(199,11,11);*/

        Date ax = new Date(); //调用类之前先声明
        ax.setDate(1999,11,11);
        ax.print();








    }
}
