import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-02
 * Time: 16:11
 */
public class Test {
    public static void main(String[] args)  {
        //缓冲流
        //他不需要自定一个char数组或者byte数组来存数据，他自带缓存
        //缓冲流读，能一次读1行

        /*try {
            FileReader reader = new FileReader("C:\\java\\javam\\ax-java\\20240502\\kun.txt");
            //当一个流的构造方法中需要一个流的时候。这个流叫做：节点流
            //外部负责包装的这个流叫做包装流，还有一个名叫 处理流
            //像当前程序FileReader是一个节点流，BufferedReader是包装流。
            BufferedReader br = new BufferedReader(reader);//能放reader，因为FileReader是他的子类

            //读
            *//*String readers = br.readLine();//读一行
            System.out.println(readers);
            String readers2 = br.readLine();//读一行
            System.out.println(readers2);*//*
            String  s = null;
            while((s = br.readLine()) != null){
                System.out.println(s);


            }
            br.close();//关闭包装流即可，里面的节点流会自动关闭


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        //转换流，转换流就是把字节流转换成字符流再传给缓冲流
        //为啥要转，因为缓冲流传的节点流只能是字符流
        //字节流
        //FileInputStream in = null;
        try {
            /*in = new FileInputStream("C:\\java\\javam\\ax-java\\20240502\\kun.txt");

            //转换成字符流,Stream是字节，然后Reader结尾变成了字符
            //在这里字节流是节点流
            InputStreamReader reader = new InputStreamReader(in);
            //缓冲流只能放字符流
            //在这里字符流是节点流
            BufferedReader br = new BufferedReader(reader);*/

            //合并
           /* BufferedReader br = new BufferedReader(new InputStreamReader (new FileInputStream("C:\\java\\javam\\ax-java\\20240502\\kun.txt")));
            String s = null;
            while((s = br.readLine()) != null){
                System.out.println(s);
            }
            br.close();*/

            //缓冲输出流
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("C:\\java\\javam\\ax-java\\20240502\\kun.txt",true)));
            out.close();

            //写
            out.write("sdasdsds");
            out.write("sda******");

            //输出流要刷新和关闭
            //out.flush();
            //out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
