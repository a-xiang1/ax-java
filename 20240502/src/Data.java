import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-02
 * Time: 17:29
 */
public class Data {
    public static void main(String[] args)throws  Exception {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream("C:\\java\\javam\\ax-java\\20240502\\kunk.txt"));
        //写入数据
        dos.writeChar('a');
        dos.writeBoolean(true);
        dos.writeChar('b');

        //读
        //只能用DataInputStream来读
        DataInputStream ins = new DataInputStream(new FileInputStream("C:\\java\\javam\\ax-java\\20240502\\kunk.txt"));
        System.out.println(ins.readChar());
        System.out.println(ins.readBoolean());
        System.out.println(ins.readChar() );
    }
}
