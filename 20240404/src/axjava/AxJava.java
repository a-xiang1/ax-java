package axjava;

//import jdk.incubator.vector.VectorOperators;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 10:28
 */

interface Shape{

    public void drow();//不能有方法体
    //除了default和static修饰的方法
    default void fun(){
        System.out.println("韦坤");
    }
    static void fun2(){
        System.out.println("韦坤2");
    }
}
class Fa implements Shape{//对接接口
    //对接好接口，必须全部重写接口的方法
    @Override
    public void drow() {
        System.out.println("韦坤爱打篮球");
    }

}
class Son implements Shape{
    @Override
    public void drow() {
        System.out.println("韦坤爱唱跳");
    }
}


public class AxJava {
    public static void Test(Shape shape){//static为啥一定要才不会报错，因为吧Test在main里面，是属于static的，所以这里也要加上static
        shape.drow();
    }
    public static void main(String[] args) {
        //接口不能实例化，那么就用向上转型来使用
        Shape shape1 = new Fa();
        Shape shape2 = new Son();
        Test(shape1);
        Test(shape2);
        shape1.drow();
        shape2.drow();
    }
}
