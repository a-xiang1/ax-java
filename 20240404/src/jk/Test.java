package jk;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 12:25
 */
public class Test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        //打开电脑
        computer.powerOn();
        Mouse mouse = new Mouse(); //实例化鼠标对象
        KeyBoard keyboard = new KeyBoard(); //实例化键盘对象
        //电脑开始运行
        computer.useDevice(mouse);//鼠标点
        System.out.println("----------------");
        computer.useDevice(keyboard);//键盘敲

        //关闭电脑
        computer.powerOff();

        System.out.println(USB.a);//说明接口的变量成员是默认静态的。
        //USB.a = 10; //无法修改，说明a被final修饰

    }
}
