package jk;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 12:06
 */
public class Computer {
    public void powerOn(){
        System.out.println("打开笔记本电脑");
    }
    public void powerOff(){
        System.out.println("关闭笔记本电脑");
    }
    //笔记本跑起来
    public void useDevice(USB usb){
        usb.open();//打开usb接口

        //看传进来的参数是鼠标还是键盘，对应的进行操作实现
        if (usb instanceof Mouse)//进行向下转型，去调用鼠标
        {
            Mouse mouse = (Mouse) usb; //强制转换类型
            mouse.click();//点鼠标
        }
        else if (usb instanceof KeyBoard)//进行向下转型，去调用键盘
        {
            KeyBoard keyboard = (KeyBoard) usb;
            keyboard.inPut();
        }
        usb.close();//关闭usb
    }
}
