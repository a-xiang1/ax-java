/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-05
 * Time: 11:52
 */


interface IRunning {
    void run();
}

interface ISwimming {
    void swim();
}

// 两栖的动物, 既能跑, 也能游
interface IAmphibious extends IRunning, ISwimming {
    @Override
    void run();

    @Override
    void swim();
}

public class Jkou {
    public static void main(String[] args) {
        System.out.println("11");
    }
}
