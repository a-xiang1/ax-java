import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 19:45
 */

//实现一个类的两个数据进行排序
//要想实现，那么就要对接一个能比较类的接口
    //如果要想用名字排序这个时候就得解决
    //用到比较器
class  C {
    public String name;
    public int age;

    public C(String name, int age) {
        this.name = name;
        this.age = age;
    }
    //要重写接口Comparable的方法才可以使用
    public int compareTo(C c5){
        if (this.age > c5.age){
            return 1;
        }else if(this.age == c5.age){
            return 0;
        }
        else{
            return -1;
        }
    }
}

class AgeComparator implements Comparator<C> {

    @Override
    public int compare(C o1, C o2) {
        return o1.age - o2.age;
    }
}

class NameCompartor implements Comparator<C>{

    @Override
    public int compare(C o1, C o2) {
        return o1.name.compareTo(o2.name);
    }
}
public class Test3 {
    public static void main(String[] args) {
        C  c = new C("xi",5);
        C c2 = new C("li",10);
        //System.out.println(b.compareTo(b2));

        AgeComparator age2 = new AgeComparator();
        System.out.println(age2.compare(c, c2));

        NameCompartor name2 = new NameCompartor();
        System.out.println(name2.compare(c,c2));


    }


}



