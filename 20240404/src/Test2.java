import java.util.Arrays;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 19:16
 */

//实现一个类的两个数据进行排序
//要想实现，那么就要对接一个能比较类的接口
class  B implements Comparable<B>{
    public String name;
    public int age;

    public B(String name, int age) {
        this.name = name;
        this.age = age;
    }

//这是干嘛的，就是把name和age这些成员转换为字符串
    @Override
    public String toString() {
        return "B{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    //要重写接口Comparable的方法才可以使用
    public int compareTo(B b5){
        System.out.println("1");
        return b5.age-this.age;//降序
//        if (this.age > b5.age){
//            return 1;
//        }else if(this.age == b5.age){
//            return 0;
//        }
//        else{
//            return -1;
//        }


    }

}

class AgeComparator2 implements Comparator<B> {


    @Override
    public int compare(B o1, B o2) {
        return o1.age -o2.age;
    }
}

class NameCompartor2 implements Comparator<B>{


    @Override
    public int compare(B o1, B o2) {
        return o1.name.compareTo(o2.name);
    }
}

public class Test2 {
    public static void main(String[] args) {
        /*B b = new B("小王",5);
        B b2 = new B("小6",10);*/

        int[] a = new int[]{1,2,3};
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));


        //对对象类进行排序
        B[] b = new B[3];
        b[0] = new B("aiaowang",15);
        b[1] = new B("ciaowang",16);
        b[2] = new B("biaowang",11);
        System.out.println("排序前" + Arrays.toString(b));
        //Arrays.sort 排序前需要知道要根据什么排序
        Arrays.sort(b);//排序，这里需要借助compareTo接口才能实现


//        AgeComparator2 a2 = new AgeComparator2();
//        //可以设置按年龄或者名字排序
//        Arrays.sort(b,a2);//指定按年龄排序

        //按名字比较
        /*NameCompartor2 n2 = new NameCompartor2();
        Arrays.sort(b,n2);*/
        System.out.println("排序后" + Arrays.toString(b));



        //System.out.println(b.compareTo(b2));
    }


}
