package jieko;

import jdk.swing.interop.SwingInterOpUtils;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 17:07
 */

public class Animal {
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

class Duck extends Animal implements Run,Swim,Fly{//alt+回车，即可得到重写接口方法


    public Duck(String name,int age) {
        super(name,age);
    }

    @Override
    public void fly() {
        System.out.println(this.name+"鸭子飞飞");
    }

    @Override
    public void swim() {
        System.out.println(this.name+"鸭子游泳");
    }

    @Override
    public void run() {
        System.out.println(this.name+"鸭子跑路");
    }
}

class Dog extends Animal implements Run{

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void run() {
        System.out.println(this.name+"狗跑路");
    }
}

class Fish extends Animal implements Swim{

    public Fish(String name, int age) {
        super(name, age);
    }

    @Override
    public void swim() {
        System.out.println(this.name+"鱼游泳");
    }
}
