package jieko;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 17:19
 */
public class AxTest {
    /*public static void fun(Animal animal){
        System.out.println(animal.name);

    }*/
    public static void running(Run run){
        run.run();

    }
    public static void swimming(Swim swim){
        swim.swim();
    }
    public static void flying(Fly fly){
        fly.fly();
    }
    public static void main(String[] args) {
        //接口解决多继承问题
        //有接口就你使用了
        running(new Dog("二哈",1));
        running(new Duck("二丫",1));
        swimming(new Fish("小红",2));
        swimming(new Duck("三丫",2));
        flying(new Duck("四丫",3));
        //running(new Fish("二哈",1));//这个就会报错，因为没有跑的接口

    }
}
