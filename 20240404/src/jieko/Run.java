package jieko;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-04
 * Time: 17:08
 */
public interface Run {
    void run();
}
interface Swim{
    void swim();
}
interface Fly{
    void fly();
}
