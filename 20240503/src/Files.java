import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-03
 * Time: 16:38
 */
public class Files {
    public static void main(String[] args) throws IOException {
        //file类
        //1.file是文件和目录路径名的抽象表示形式。
        //c:long 这是一个file对象
        //2.须掌握File类常用的方法

        File f1 = new File("C:\\java\\javam\\ax-java\\20240503\\file\\a\\d\\c\\q");
        //判断是否存在该文件
        System.out.println(f1.exists());

        //如果不存在那么进行创建文件
        /*if (!f1.exists()){
            f1.createNewFile();//创建新的文件
        }*/
        //System.out.println(f1.exists());

        /*//如果不存在那么进行创建文件夹
        if (!f1.exists()){
            f1.mkdir();//创建一个新的子目录
        }
        System.out.println(f1.exists());*/

        //如果不存在那么进行创建多个文件子目录
        /*if (!f1.exists()){
            f1.mkdirs();//创建一个新的子目录
        }
        System.out.println(f1.exists());
*/

        File f2 = new File("C:\\java\\javam\\ax-java\\20240503\\kun.txt");

        //获取文件的父路径
        String paren = f2.getParent();
        System.out.println(paren);

        //另一种获取父路径
        File parenFile = f2.getParentFile();
        System.out.println(parenFile.getAbsolutePath());

        //获取路径名的绝对路径名字符串
        File f3 = new File("kun.txt");//文件名
        System.out.println(f3.getAbsolutePath());

        File f4 = new File("C:\\java\\javam\\ax-java\\20240503\\kun.txt");

        //获取文件名
        System.out.println("文件名" + f4.getName());

        //判断是否是一个目录
        System.out.println(f4.isDirectory());
        //判断是否是一个文件
        System.out.println(f4.isFile());

        //获取文件最后一次修改时间

        long haomiao = f4.lastModified();//返回的是毫秒长整型
        //将毫秒数转换成日期
        Date time = new Date(haomiao);
        //转换日期输出格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
        String strTime = sdf.format(time);
        System.out.println(strTime);

        //获取文件大小
        System.out.println(f4.length());//大小单位为字节

        //listFile返回目录下的所有子目录或者文件绝对路径
        File f5 = new File("C:\\java\\javam\\ax-java\\20240503");
        File[] files = f5.listFiles();

        for (File file : files){
            //System.out.println(file.getName());
            System.out.println(file.getAbsolutePath());
        }




    }
}
