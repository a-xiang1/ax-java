import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-03
 * Time: 20:55
 */
public class Copys {
    //拷贝目录
    public static void main(String[] args) {
        //拷贝源
        File srcFile = new File("C:\\Users\\11446\\Desktop\\编程");
        //拷贝目标
        File destFile = new File("C:\\java");

        //调用拷贝方法拷贝
        copyDir(srcFile,destFile);
    }

    //拷贝方法
    private  static void copyDir(File srcFile,File destFile){
        //递归结束条件就是当遇到是文件的时候就不用递归了,目录的话则是继续递归，就是继续打开目录
        if (srcFile.isFile()){
            //创建好目录，读和写文件
            //一边读一边写
            FileInputStream in = null;
            FileOutputStream out = null;

            try {
                //创建字节输入流对象
                in = new FileInputStream(srcFile);
                String path = (destFile.getAbsolutePath().endsWith("\\")  ? destFile.getAbsolutePath() : destFile.getAbsolutePath()+ "\\" )+ srcFile.getAbsolutePath().substring(3);
                out = new FileOutputStream(path);

                byte[] bytes = new byte[1024 * 1024];
                int readCount = 0;
                while((readCount = in.read(bytes) ) != -1){
                    out.write(bytes,0,readCount);
                }

                out.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (out != null){
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return;
        }


        //先获取源下面的全部子目录
        File[] files = srcFile.listFiles();
        //System.out.println(files.length);

        for (File file : files){
            //里面有目录或者文件，不过他们都是File类型

            //获取所有文件的（包括目录和文件）的绝对路径
            //System.out.println(file.getAbsolutePath());

            //判断一下，如果是目录那就创建目录
            if (file.isDirectory()){
                //新建对应目录
                //System.out.println(file.getAbsolutePath());
                //需要知道源目录和目标目录
                //源目录已经知道
                String srcdir = file.getAbsolutePath();//源目录
                //C:\Users\11446\Desktop\编程，把c:\ 这三个截断取后面的给目标目录
                //C:\java
                //目标目录就是在目标目录的基础上加上原目录的目录路径

                //截取后的原目录
                //System.out.println(srcdir.substring(3));//用到String里面的字符截取

                //拼接目标目录
                //String destdir = destFile.getAbsolutePath() + srcdir.substring(3);
                //解决没有\
                String destdir = (destFile.getAbsolutePath().endsWith("\\")  ? destFile.getAbsolutePath() : destFile.getAbsolutePath()+ "\\" )+ srcdir.substring(3);

                System.out.println(destdir);

                //在目标目录里面开始创建好目录
                File newFile = new File(destdir);
                //如果不存在那么就创建
                if (!newFile.exists()){
                    newFile.mkdirs();
                }

                //创建好目录，读和写文件

            }


            //递归调用
            copyDir(file,destFile);

            //在哪创建目录
        }

    }

}
