import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-03
 * Time: 15:56
 */
public class Main {
    public static void main(String[] args) throws Exception {
        //标准字节输出流，默认输出到控制台。
        //合起来写
        System.out.println("韦坤四战四级");

        //分开写
        PrintStream ps = System.out;//标准字节输出流
        ps.println("sdasda");//默认直接输出到控制台
        ps.println("sdas");
        //标准输出流不需要手动关闭流
        //可以改变标准输出流的输出方向

        //此时标准输出流不再指向控制台，指向“log文件"
        PrintStream print = new PrintStream(new FileOutputStream("C:\\java\\javam\\ax-java\\20240503\\kunk.txt",true));
        //修改输出方向将输出方向修改到log文件
        System.setOut(print);
        //然后输出
        System.out.println("hello word" );
        System.out.println("hello word" );
        System.out.println("hello word" );
        System.out.println("hello word" );






    }
}
