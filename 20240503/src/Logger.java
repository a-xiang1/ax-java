import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-03
 * Time: 16:13
 */
public class Logger {
    //使用标准输出做一个日志文件
    public static void log(String msg){//这是个静态方法，不用声明可以直接调用
        try {
            //输出方向指向日志文件
            PrintStream print = new PrintStream(new FileOutputStream("C:\\java\\javam\\ax-java\\20240503\\kun.txt",true));
            //该变输出方向
            System.setOut(print);
            //输出当前时间
            Date nowtime = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");//日期格式
            String strTime = sdf.format(nowtime); //获取日期时间

            System.out.println(strTime + ": " + msg);//将内容输出到文件里面

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
