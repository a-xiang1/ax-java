import java.util.Locale;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-13
 * Time: 10:50
 */




public class Mian {

    ////排除所有非字母数字字符
    public static boolean isNumOrCharacter(char ch){
        if (ch >= '0' && ch <= '9' || ch >='a' && ch <= 'z')
          // 0-9是数字,a-z,26个字符,不用考虑大写情况，因为会提前全部变成小写字母
        {
            return true;
        }
        return false;
    }
    //
    public static boolean isPalindrome(String s){

        s.toLowerCase();//变成小写字母
        //定义i和j
        int i = 0;
        int j = s.length()-1;
        //两个循环同时进行才可以
        while(i < j){

            ////i<j防止越界,判断是不是字母和数字，是的话++
            while (i < j && !isNumOrCharacter(s.charAt(i)))
            {
                i ++;
            }

            while (i < j && !isNumOrCharacter(s.charAt(j)))
            {
                j --;
            }
            //判断两个对应的字符是否相等，相等是回文
            //不相等返回flase，相等继续往前和往后
            if (s.charAt(i) == s.charAt(j)){
                i ++;
                j --;
            }else{
                return false;
            }


        }
        //走完一遍都符合说明是回文串
        return true;
    }


    public static  int find(String str) {
        char[] count = new char[26];//存放26个小写字母的位置
        for (int i = 0; i < str.length(); i++) {//遍历字符数组凡是出现一次就加1

            char ch = str.charAt(i);
            count[ch - 'a']++;//给对应的下标字符加1,减a相当于-97，小写最小97
        }

        //再次遍历字符串的每个字符，如果在数组里面是1那么返回当前的数组下标
        for (int i = 0; i < str.length(); i++) {//遍历字符数组凡是出现一次就加1

            char ch = str.charAt(i);
            if (count[ch - 'a'] == 1) {//找第一个是1的
                return i;//找到返回下标
            }
        }
        return -1;//找不到
    }




    public static void main(String[] args) {
        /*System.out.println("归队");
        String str = "abc";
        String s1 = str +"abc";
        System.out.println(s1);*/
        //这样写法并不是每次拼接直接在str里面添加，而是新new新的对象，然后拼接
        //然而如果要进行大量拼接就会要new大量对象，会很浪费时间
        //而使用StringBuffer则无需新new对象，可以直接加到StringBuffer类中
        /*StringBuffer str2 = new StringBuffer("abd");
        str2.append("add");//直接拼接add上去*/

        //找唯一的字符
        String str2 = "abcfabcdf";
        System.out.println(find(str2));

        //找字符串在的位置
        /*Scanner scanner = new Scanner(System.in);
        String str3 = scanner.nextLine();
        System.out.println(str2.indexOf(str3));*/
        //使用库方法求比如hello new 这样的空格后面的单词长度
        //先找空格下标，然后从空格下标后面开始数长度
        String str3 = "hello word";
        System.out.println(str3.indexOf(" "));
        System.out.println(str3.substring(str3.indexOf(" ")+1));//[)给定区间截取字符串
        String   strcount = str3.substring(str3.indexOf(" ")+1,str3.length() );
        System.out.println(strcount.length());


        //实现如果将所有的大写字符转换成小写字符，并移除所以非字母数字字符之后
        String str4 = "A man, a plan,a canal; Panama";
        String str5 = "aaa ccc aaa";
        System.out.println(isPalindrome(str5));


    }
}
