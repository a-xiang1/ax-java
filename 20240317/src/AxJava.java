import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-17
 * Time: 9:55
 */
//今日学习内容，类和对象
public class AxJava {


    //数组拷贝
    public static void Copy(){
        int[] arr3 = {5,6,1,4,5};
        //第一种直接遍历数组赋值拷贝
        /*int[] tem = new int[arr3.length];
        for (int i = 0 ;i < arr3.length ; i ++){
            tem[i] = arr3[i];
        }
        System.out.println(Arrays.toString(tem));*/

        //第二种使用Arrays使用
        /*int[] copy2 = Arrays.copyOf(arr3,arr3.length);//参数是要拷贝的数组，拷贝的长度
        System.out.println(Arrays.toString(copy2));*/
        /*int[] copy3 = Arrays.copyOfRange(arr3,0,3);//指定范围拷贝[0,3)
        System.out.println(Arrays.toString(copy3));*/
        int[] copy4 = new int[arr3.length];
        //还能指定下标位置，然后设置拷贝范围
        System.arraycopy(arr3,1,copy4,1,arr3.length-1);
        System.out.println(Arrays.toString(copy4));
    }
    //数组方法的集合
    public  static void arr_means(){
        /*int[] arr = {1,2,3,4,5};
        int[] arr2 = {1,2,3,4,51};*/
        //判断两数组是否相等
        //System.out.println(Arrays.equals(arr, arr2));//返回值是true 和flase
        //数组初始化
        /*Arrays.fill(arr2,6);//给数组全部填充，初始化
        System.out.println(Arrays.toString(arr2));*/

        //也可以给数组指定范围填充，初始化
       /* Arrays.fill(arr2,0,3,6);
        System.out.println(Arrays.toString(arr2));*/

        //实现数组的拷贝
        /*Copy();*/

        //进入二维数组
        //二维数组的定义，首先要明白，二维数组是特殊的一维数组
        int[][] arr = {{12,2,3},{2,2,3}} ;
        int[][] arr2 = new int[][]{{12,2,3},{2,2,3}};
        //int[][] arr3 = new int[2][3]{{12,2,3},{2,2,3}};//这样写是报错的，因为如果你赋值了，java会识别数组的大小的
        int[][] arr3 = new int[2][3];
        int[][] arr4 = new int[2][];//不规则二维数组，就是列是可以不同的

        //二维数是特殊的一维数组
       /* System.out.println(arr.length);
        System.out.println(arr[0].length);
        System.out.println(arr[1].length);*/

        //遍历二维数组
       /* for (int i = 0 ; i < arr.length; i ++){//arr.lenth代表着中国二位数组的大小行数
            for (int j = 0; j < arr[i].length ; j ++){//arr[i].lenth代表着数组每行有几列。
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }*/
        //用加强版for来打印二维数组
        /*for (int[] tmp : arr){
            for(int x : tmp){
                System.out.print(x + " ");
            }
            System.out.println();
        }*/
        //简单粗暴的打印二维数组
        /*System.out.println(Arrays.deepToString(arr));*/

        //处理一下不规则二维数组
        /*arr4[0] = new int[]{1,2};
        arr4[1] = new int[]{1,2,3};
        System.out.println(Arrays.deepToString(arr4));*/
       /* int arr6[] = {1, 2, 3};
        System.out.println(Arrays.toString(arr6));*/


    }
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");

        arr_means();
    }
}
