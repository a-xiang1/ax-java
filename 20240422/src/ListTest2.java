import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-22
 * Time: 18:41
 */

public class ListTest2 {
    public static List<List<Integer>> generate(int num) {

        List<List<Integer>> ret = new ArrayList<>();//创建一个二维数组
        List<Integer> list = new ArrayList<>();//创建一个一维数组，二维数组是由多个一维数组构成
        list.add(1);//给二维数组第0行第0列放入1
        ret.add(list);
        for (int i = 1; i < num; i++) {//要小于行数

            List<Integer> curRow = new ArrayList<>();
            curRow.add(1);//第二行第0列1

            List<Integer> prevRow = ret.get(i - 1);//获取上一行，接下来需要上一行的相加

            for (int j = 1; j < i; j++) {//因为每行的第0列和最后一列都是放1所以j从1开始
                int val = prevRow.get(j - 1) + prevRow.get(j);//上一行的前一位+后一位
                curRow.add(val);//然后存放到该行中
            }
            //存完，然后给最后一位补1
            curRow.add(1);//即可完成一行数据

            //然后存到二维数组里面，相当于第二行就完成了
            ret.add(curRow);


        }
        return ret;
    }
    public static void main(String[] args) {
        //list实现二维数组打印杨辉三角
        List<List<Integer>> list66 = generate(5);//5是五行意思
        System.out.println(list66);


    }
}
