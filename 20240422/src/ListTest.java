import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-22
 * Time: 9:45
 */
public class ListTest {
    public static void main(String[] args) {
        //ArrayList数组结论
        //1.第一次add方法会分配大小为10的内存
        //2.扩容的倍数是1.5倍

        //ArrayList构造方法的使用
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(1);
        list1.add(3);
        list1.add(3);
        list1.add(3);
        LinkedList<Number> list2 = new LinkedList<>(list1);//传一个属于Collection类型的
        System.out.println(list2);
        //remove删除
        /*list2.remove(new Integer(1));//给一个对象然后进行删除
        list2.remove(0);//给一个下标，删除指定下标的内容*/

        Iterator iter = list2.iterator();
        while(iter.hasNext()){
            System.out.println(iter.next());
        }
        System.out.println(list2);

        List<Number> list = list2.subList(1,3);//[)
        System.out.println(list);
        list.set(1,20);
        System.out.println(list);

        //ArrayList、LinkedList、TreeSet,都实现了Collection接口
        //因此可以通过接口来实例化对象
        //泛型要符合上界关系
        /*Collection<Integer> co1 = new ArrayList<>();
        Collection<Integer> co2 = new LinkedList<>();
        Collection<Integer> co3 = new TreeSet<>();*/


    }
}
