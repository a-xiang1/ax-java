package axbao;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-02
 * Time: 9:59
 */

//多态的优点
class Shape{
    public void draw(){
        System.out.println("0");
    }

    //实例成员
    public String ax ;
    public String ss ;
    public int ax2 ;

    public Shape(){

    }
    //构造方法块
    {
        this.ax = "sedd";
    }

}

class Shape1 extends Shape{
    public void draw(){
        System.out.println("1");
    }
}

class Shape2 extends Shape{
    public void draw(){
        System.out.println("2");
    }
}
class Shape3 extends Shape{
    public void draw(){
        System.out.println("3");
    }
}
class Shape4 extends Shape{
    public void draw(){
        System.out.println("4");
    }
}



public class AXjava {
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");
        Shape[] shapes = {new Shape1(),new Shape2(),new Shape3(),new Shape4()};
        for (Shape shape : shapes ){
            shape.draw();
        }

        {
            System.out.println("shabei韦坤");
        }

        //普通代码块，定义在方法中的代码块
       /* public class Main{
            public static void main(String[] args) {
                { //直接使用{}定义，普通方法块
                    int x = 10 ;
                    System.out.println("x1 = " +x);
                }
                int x = 100 ;
                System.out.println("x2 = " +x);
            }
        }*/



    }
}
