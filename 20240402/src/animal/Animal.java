package animal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-02
 * Time: 15:27
 */
class Animal {//作为父类
    String name;
    int age;
    public void eat(){
        System.out.println(name + "在吃饭");
    }
    public void sleep(){
        System.out.println(name + "在睡觉");
    }

    public void show(){
        System.out.println("父类重写");
    }
}

class  Dog extends Animal{//继承父类，拥有父类的属性

    //定义与父类相同名的成员变量
    String name; // 与父类中成员变量同名且类型相同
    char age; // 与父类中成员变量同名但类型不同
   public void men(){
        //对于同名的成员变量，直接访问的话，访问到的都是子类
        age = 'a'; // 等价于： this.age = 100  this是当前对象的引用
        name = "韦kun";

        //如果要访问父类的成员变量,则需要借助super
        //super是获取到子类对象中从基类继承下来的部分
        super.name = "kk";
        super.age = 100;

       // 父类和子类中构成重载的方法，直接可以通过参数列表区分清访问父类还是子类方法
       show();//这是重写的方法，默认会访问子类的方法
       super.show();//这样才可以访问到父类的方法
       show(10);//重载访问子类
    }

    // 与父类中show()构成重载，重载就是函数名一样，但是参数不同。
    public void show(int a){
        System.out.println(a);
        System.out.println(a + "子类");
    }
    //重写父类方法，后续会讲到重写，重写其实就是父类方法名和参数一模一样才行。
    public void show(){
        System.out.println("子类重写");
    }
    public void bark(){
        System.out.println(name + "汪");
    }

}

class  Cat extends Animal{//继承父类

    public void mew(){
        System.out.println(name + "喵");
    }
}

class TestA{
    public static void main(String[] args) {
        Dog dog = new Dog();
        // dog类中并没有定义任何成员变量，name和age属性肯定是从父类Animal中继承下来的
        dog.men();
        System.out.println(dog.name);
        System.out.println(dog.age);
        // dog访问的eat()和sleep()方法也是从Animal中继承下来的

        dog.eat();
        dog.sleep();
        dog.bark();


        /*Cat cat = new Cat();
        System.out.println(cat.name);
        System.out.println(cat.age);*/


    }
}
