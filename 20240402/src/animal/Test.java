package animal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-02
 * Time: 16:42
 */
class Base{
    int a ;
    public Base() {
        System.out.println("父类创建构造方法");
    }


}

class Son extends Base{
    int a;
    public Son() {
        super();//这个如果不写，也会默认有一个，但是当父类构造方法有参数时候需要传参
        System.out.println("子类创建构造方法");

    }

}

public class Test {
    public static void main(String[] args) {
        Son son = new Son();
    }
}
