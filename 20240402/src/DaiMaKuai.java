/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-02
 * Time: 17:09
 */



class Person {
    public String name;
    public int age;
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Person：构造方法执行");
    }
    {
        System.out.println("Person：实例代码块执行");
    }
    static {
        System.out.println("Person：静态代码块执行");
    }
}
class Student extends Person{
    public Student(String name,int age) {
        super(name,age);
        System.out.println("Student：构造方法执行");
    }
    {
        System.out.println("Student：实例代码块执行");
    }

    static {
        System.out.println("Student：静态代码块执行");
    }
}




public class DaiMaKuai {
    public static void main(String[] args) {
        Student student1 = new Student("张三",19);
        System.out.println("===========================");
        Student student2 = new Student("555",20);
        Person person1 = new Person("666",10);
        System.out.println("============================");
        Person person2 = new Person("777",20);
    }
}





