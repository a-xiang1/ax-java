package com.ax.ui;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-08
 * Time: 16:43
 */
/*
登录界面
获取用户输入的用户名、密码
生成一个验证码
获取用户输入的验证码
比较用户名、密码、验证码是否正确
 */
public class LohinJFrame extends JFrame {

    public LohinJFrame() {
        this.setSize(488,430);
        this.setAlwaysOnTop(true);//设置窗口界面置顶
        this.setLocationRelativeTo(null);//设置界面剧中
        this.setLayout(null);// 取消默认放置按钮啥的
        this.setVisible(true);
    }
}
