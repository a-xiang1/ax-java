/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-07
 * Time: 11:37
 */
public class StackTest {
    public static void main(String[] args) {
        Stack01 stack = new Stack01();
        stack.push(2);
        stack.push(5);
        stack.push(8);
        stack.push(9);
        stack.display();

        //出栈
        System.out.println(stack.pop());
        //看栈顶
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.peek());
    }
}
