/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-07
 * Time: 11:38
 */
//异常
public class Unusual  extends RuntimeException{
    public Unusual (String msg){
        super(msg);
    }
}
