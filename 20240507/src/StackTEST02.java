import javax.management.MBeanAttributeInfo;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-07
 * Time: 23:24
 */


/*
输入：tokens = ["2","1","+","3","*"]
输出：9
解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
 */
public class StackTEST02 {
    public static void main(String[] args) {
        stack0s();
    }
    private static boolean isOperation(String s){
        if (s.equals("+")|| s.equals("-") || s.equals("*") || s.equals("/")){
            return true;
        }
        return false;
    }
    public static int stack0s(){
        Stack<Integer> stack = new Stack<>();
        String[] tokens = new String[20];
        for (String  x : tokens){
            if (!isOperation(x)){
                stack.push(Integer.parseInt(x));//将字符串改为数字格式
            }else {
                int num2 = stack.pop();//先出来的作为/的右边
                int num1 = stack.pop();
                switch (x){
                    case "+":
                        stack.push(num1 + num2);
                        break;
                    case "-":
                        stack.push(num1 - num2);
                        break;
                    case "*":
                        stack.push(num1 * num2);
                        break;
                    case "/":
                        stack.push(num1 / num2);
                        break;
                }
            }
        }
        return stack.pop();
    }
}
