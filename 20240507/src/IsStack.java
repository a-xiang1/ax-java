/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-07
 * Time: 11:37
 */
public interface IsStack {
    //入栈
    void push(int x);
    //出栈
    int pop();

    //查栈顶元素不出栈
    int peek();

    //求长度
    int size();

    //判断是否为空
    boolean empty();
    //判断是否满了
    boolean full();
    void display();//打印



}
