import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-07
 * Time: 11:36
 */
public class Stack01 implements IsStack {

    //给个数组当做栈
     private int[] elem;
     private int usedsize;

     private  static final int DEEAULT = 10;

    public Stack01() {
        elem = new int[DEEAULT];
    }



    @Override
    public void push(int x) {
        //看是否满了
        if (full()){
            //满了进行扩容
            //拷贝然后扩容
        elem = Arrays.copyOf(elem,2*elem.length);
        }
        elem[usedsize] = x;
        usedsize++;

    }

    @Override
    public int pop() {
        //先看是不是空
        if (empty()){
            //抛异常
            throw new Unusual("栈空了");
        }
        int old = elem[usedsize-1];
        usedsize--;//指向下一个,算是覆盖

        return old;


    }

    @Override
    public int peek() {
        //先看是不是空
        if (empty()){
            //抛异常
            throw new Unusual("栈空了");
        }


        return elem[usedsize-1];


    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean empty() {
        if (usedsize == 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean full() {
        if (usedsize == elem.length){
            return true;
        }
        return false;
    }

    @Override
    public void display() {
        for (int i = 0 ; i < usedsize ; i++){
            System.out.print(elem[i] + " ");
        }
        System.out.println();
    }
}
