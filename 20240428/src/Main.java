/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-28
 * Time: 10:08
 */
public class Main {

    //实现链表的回文结构
    /*
    //目前只考虑奇数情况
    1.找到中间位置
    怎么找，用快慢指针来实现
    2.从中间位置开始，中间后面都要翻转，这样就能实现前后的进行比较
    3.从开头 -》后面  后面 -》 前面 ，最终会在中间集合，到了之后都符合说明是回文
     */

    /*
    public class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }*/
    public class PalindromeList {
        public boolean chkPalindrome(ListNode head) {


            if(head == null || head.netx == null){
                return true;
            }
            // write code here
            //中间位置
            ListNode fast = head;
            ListNode slow = head;
            while(fast != null && fast.next != null ){
                fast = fast.next.next;//走两步
                slow = slow.next;
            }
            //走完即可得到中间位置

            //从中间开始翻转
            //定义两个指向slow的后面，把slow当做头，然后进行翻转
            ListNode cur = slow.next;
            ListNode curNext = slow.next.next;

            while(cur != null){
                curNext = cur.next;//提前存好cur的下一个地址，不然丢失
                cur.next = slow;//slow后面指向存放slow地址
                slow = cur; //然后slow相当于头结点往后走
                cur = curNext;//然后继续下一个节点翻转

            }

            //走到这里翻转完成，此时slow指向最后一个节点位置
            //3.进行前后移动
            fast = head; //让fast回到开头
            //此时fast和slow一个在头一个在尾，进行移动即可。
            //可是怎么知道走到中间了呢
            while(fast.next != slow.next){ //因为后面被翻转了，所以中间的左右节点的next放的地址都是中间的地址。

                if(fast.val == slow.val){//进行值的对比，如果有一个不对就不是回文
                    fast = fast.next;
                    slow = slow.next;

                }
                else{
                    return false;
                }
            }
            return true;



        }
    }

    //编写代码，以给定值x为基准将链表分割成两部分，所有小于x的结点排在大于或等于x的结点之前
    //会改变链表的结构
    public ListNode partition(ListNode head, int x) {
        // write code here

        ListNode cur = head;

        ListNode bs = null;//创建好节点类型的用于保存分割后两个区间的节点位置地址
        ListNode be = null;

        ListNode as = null;
        ListNode ae = null;

        while(cur != null){
            //分割
            if(cur.val < x ){
                if(bs == null){//如果是第一个头尾都指向他
                    bs = cur;
                    be = cur;

                }else{
                    //如果是第二个以后进行尾插
                    be.next = cur;//尾插原理
                    be = be.next;
                }

            }else{
                if(as == null){
                    as = cur;
                    ae = cur;
                }else{
                    ae.next = cur;
                    ae = ae.next;
                }

            }
            cur = cur.next;
        }
        //有一种情况，如果链表是3333，x为3，那么前面区间是空的
        //此时be.next会空指针异常
        //因此要对第一区间进行判断
        if(be == null){
            return as;//如果第一区间是空那么没必要连接，返回第二区间的头即可。
        }

        //连接起来
        //用第一个区间的尾和第二个区间的头进行连接
        be.next = as;

        //还有一个情况，就是拼接起来后，第二区间的尾节点的next有可能不为空，异常
        if(ae != null ){//此时需要手动进行置空
            ae.next = null;
        }
        return bs;//返回第一区间的头

    }



    //给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回 null
    /*
    1.分别求长度
    2.求两个长度差值，让长的先走完差值步数
    3.然后同时走
    4.当他们相遇的时候那个相遇点就是相交节点
    */
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        //1.分别求长度
        ListNode head1 = headA;
        ListNode head2 = headB;

        int count1 = 0;
        int count2 = 0;
        while(head1 != null){
            count1++;
            head1 = head1.next;
        }
        while(head1 != null){
            count2++;
            head2 = head2.next;

        }
        //这里要注意上面求完长度，记得要重新指向头结点
        head1 = headA;
        head2 = headB;

        //这里要注意上面求完长度，记得要重新指向头结点
        //2.求差值
        int len = count1 - count2;
        //差值的正负情况也要分析一下
        //如果差值为负说明headB链表长,反之就是headA。

        //如果为负数第二个链表比较长，那么改变一下指向，
        //下面就是为了改变len，让len肯定是正数，让head1是指向最长的那个链表
        if(len < 0){
            head1 = headB;
            head2 = headA;
            len =  count2 - count1;
        }

        //让长的走差值步

        while(len != 0){
            head1 = head1.next;
            len--;
        }

        //同时走
        while(head1 != head2){//当两个相遇的时候就是结束的时候
            head1 = head1.next;
            head2 = head2.next;


        }
        if(head2 == null){
            return null;
        }

        //相遇点
        return head1;


    }


    /*
   给你一个链表的头节点 head ，判断链表中是否有环。

如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。
为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置
（索引从 0 开始）。注意：pos 不作为参数进行传递 。仅仅是为了标识链表的实际情况。

如果链表中存在环 ，则返回 true 。 否则，返回 false
   */
    //追击问题
    //就是利用快慢指针，一个走两步，一个走一步，如果有环，那么两者肯定会相遇,每走完一步就进行判断
    public boolean hasCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while(fast != null && fast.next != null){
            fast = fast.next.next; //走两步
            slow = slow.next;
            if(fast == slow){//每走完一步就进行判断
                return true;
            }
        }
        return false;



    }

}
