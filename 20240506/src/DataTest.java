import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-05-06
 * Time: 9:14
 */
public class DataTest {
    public static void main(String[] args) throws ParseException {
        //获取系统当前时间（精确到毫秒）
        //直接调用无参数构造方法
        Date nowTime = new Date();
        //输出格式 是国际的日期格式
        System.out.println(nowTime);

        //修改日期的输出格式,格式化日期
        /*
        yyyy 年
        MM 月
        dd 日
        EEE 星期
        HH 时
        mm 分
        ss 秒
        SSS 毫秒（三位数）
         */
        //格式可以随便写
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss SSS");//构造方法里面写日期格式
       String nowTimes =  sdf.format(nowTime);// SimpleDateFormat会把日期格式化成字符串
        System.out.println("北京时间：" + nowTimes);

        //日期字符串String怎么转换成Date类型
        //String - date
        String time03 = "2008-08-08 08:08:08 888";
        //这里的格式必须对应字符串的格式来不然报错
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
        Date date = sdf2.parse(time03);
        System.out.println(date);




        //获取自1970年1月1日 00到当前系统时间的总毫秒数
        //1秒 = 1000毫秒
        long nowTimes02 = System.currentTimeMillis();
        System.out.println(nowTimes02);

        //统计调用一个方法所需要耗费的时间
        long  begin = System.currentTimeMillis();
        print();
        long end = System.currentTimeMillis();
        System.out.println();
        System.out.println("总耗时：" + (end - begin) + "毫秒");

        //Date有参表示毫秒
        Date time = new Date(1);
        System.out.println(sdf.format(time));

        //获取昨天的时间
        Date time02 = new Date(System.currentTimeMillis() - 1000 * 60 * 60 *24);
        System.out.println(sdf.format(time02));
        //数字格式化
        //DecimalFormat df = new DecimalFormat("数组格式");
    /*
    #代表任意数字
    ，代表千分位
    .代表小数
    0代表补0
    */

        DecimalFormat df = new DecimalFormat("###,###.0000");//保留四位不够补0
        String s2 = df.format(1234.56);
        System.out.println(s2);


    }
    public static void print(){
        for (int i = 0; i < 100000000 ;i ++){
           // System.out.print(i + " ");
        }
    }





}
