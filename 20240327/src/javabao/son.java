package javabao;

import org.w3c.dom.ls.LSOutput;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-03-27
 * Time: 16:35
 */
//遵循优先原则，子类有的先用子类，没有的话去父类找
public class son  extends Fu{
    int a = 5;//变量、成员重写
    public void show(){
        System.out.println(a);
        System.out.println(super.a);
    }
    public void add(){//父类和子类方法重载，这时父类得分方法将会被隐藏

        System.out.println("韦坤战四级");
    }

    //那如何引用隐藏了的父类中的成员或者方法呢
    //这时候要用到super


    public son(int a) { //构造方法
        this.a = a;
    }
    public son() { //构造方法
       super(1);//通过构造方法给父类初始化

    }

}
