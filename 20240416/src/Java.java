/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-16
 * Time: 10:38
 */
public class Java {
    public static int getElement(int[] array, int index){
        if(null == array) {
            throw new NullPointerException("传递的数组为null");//throw就是用来将错误信息翻译一下告知调用者

        }
        if(index < 0 || index >= array.length){
            throw new ArrayIndexOutOfBoundsException("传递的数组下标越界");
        }

        return array[index];
    }
    public static void main(String[] args) {
        System.out.println("韦坤四战四级");
        int[] array = {1,2,3};
        int[] array2 = null;
        getElement(array2, 3);
    }
}
