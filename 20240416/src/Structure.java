/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-16
 * Time: 15:53
 */
//正式进入数据结构的学习
    //学好三要素
    //多写代码
    //多思考
    //多画图
public class Structure {
    public static void main(String[] args) {
        System.out.println("韦坤四战");

        //算法效率如何看
        //时间和空间复杂度
        //讨论最坏情况
        //装箱（包）和拆箱（包）
        Integer a =10 ;
        int i = 50;
        a = i;//装箱，基本数据类型给包装类型
        System.out.println(a);

        Integer b = 10;
        int c = b;//拆箱，包装类型给基本数据类型
        System.out.println(b);

        Integer a2 = 100;
        Integer a3 = 100;
        System.out.println(a2 == a3);//true

        Integer b2 = 128;
        Integer b3 = 128;
        System.out.println(b2 == b3);//flase
        //原因是装箱的时候有一个数组范围只有[128,127],在这范围才不会错。



    }
}
