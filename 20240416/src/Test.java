/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-16
 * Time: 18:47
 */
//泛型<T>里面填任何字母都行，只是泛型的标识符
class MyArray<T>{
    public Object[] array = new Object[10];

    public void setValue(int pos ,T val){//接收类型为T
        array[pos] = val;

    }
    public  T getValue(int pos){
        return (T)array[pos];//强转为T，返回pos下标数据
    }

}


public class Test {

    //泛型 是编译时期存在的 当程序运行起来 到jvm之后就没有泛型的概念
    public static void main(String[] args) {
        //所谓的泛型就是将类型进行了传递，然后可以进行调用
        MyArray<Integer> myArray = new MyArray<>(); //<>里面只能放包装类不能放基本数据类型
        //上面放的是Integer 所以只能传int/Integer类型的过去
        myArray.setValue(0,10);
        myArray.setValue(1,100);
        Integer a = myArray.getValue(0);
        int  b = myArray.getValue(1);
        System.out.println(a);
        System.out.println(b);

        MyArray<String> myArray2 = new MyArray<>();
        myArray2.setValue(0,"hello");
        myArray2.setValue(1,"word");
        String str = myArray2.getValue(0);
        String str2 = myArray2.getValue(1);
        System.out.println(str);
        System.out.println(str2);


    }
}
