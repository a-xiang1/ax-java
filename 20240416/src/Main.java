/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 11446
 * Date: 2024-04-16
 * Time: 11:49
 */
public class Main {


        private String userName = "admin";
        private String password = "123456";

        public static void loginInfo(String userName, String password) throws UserNameException, PasswordException {
            if (!userName.equals(userName)) {
                throw new UserNameException("用户名1错误！");//要用这个自定义类型得用throws声明才行
            }
            if (!password.equals(password)) {
                throw new PasswordException("用户名错误！");
            }
            System.out.println("登陆成功");
        }

        public static void main(String[] args) throws UserNameException, PasswordException {
            loginInfo("admin", "123456");
        }
    }

    /*自定义异常通常会继承自 Exception 或者 RuntimeException
    继承自 Exception 的异常默认是受查异常
     继承自 RuntimeException 的异常默认是非受查异常.*/
class UserNameException extends Exception {
    public UserNameException(String message) {
        super(message);
    }
}

class PasswordException extends Exception {
    public PasswordException(String message) {
        super(message);
    }
}
